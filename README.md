# Carbobrake

This research has been funded by the Walloon Region under the agreement no. 2010092-CARBOBRAKE in the context of the M-ERA.Net Join Call 2020. Funded by the European Union under the Grant Agreement no. 101102316. Views and opinions expressed are those of the author(s) only and do not necessarily reflect those of the European Union or the European Commission. Neither the European Union nor the granting authority can be held responsible for them.

## Directories

  1. [Polypropylene BJ380MO](./Polypropylene)
  2. [Forge Molding Compound STR120N131](./Forge Molding Compound STR120N131)
  3. [Data of paper submitted to IJSS](./ijss_2025_vevp_ScriptsForTestsImages)



## License
Creative Commons Attribution 4.0 International (CC BY 4.0) licence

