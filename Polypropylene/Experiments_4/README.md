
# Compression and Tension Uniaxial Tests

## Sub-Directories  (Relax: Its not a Russian Doll):

1. [Part1_23_70_cyclic](./Part1_23_70_cyclic)
 * 1-7) 23C_1-cycle1 to 23C_1-cycle7.txt - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, 7 load-unload cycles - Attempt 1
 * 8) 23C_1-summary.txt - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, Attempt 1 summarised, .txt
 * 9) 23C_1-summary.xlsx - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, Attempt 1 summarised, .xlsx
 * 10) 23C_1-summary_refined.xlsx - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, Attempt 1 summarised and refined
 * 11-18) 23C_2-cycle1 to 23C_2-cycle7.txt - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, 8 load-unload cycles - Attempt 2
 * 19) 23C_2-summary.txt - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, Attempt 2 summarised, .txt
 * 20) 23C_2-summary.xlsx - Uniaxial Tension Cyclic Loading at 23°C Quasi-steady, Attempt 2 summarised, .xlsx
 * 21) 70C_1-cycle1 to 70C_1-cycle6.txt - Uniaxial Tension Cyclic Loading at 70°C Quasi-steady, 6 load-unload cycles - Attempt 1
 * 22) 70C_1-summary.txt - Uniaxial Tension Cyclic Loading at 70°C Quasi-steady, Attempt 1 summarised, .txt
 * 23) 70C_1-summary.xlsx - Uniaxial Tension Cyclic Loading at 70°C Quasi-steady, Attempt 1 summarised, .xlsx

2. [Part2_m10_70_cyclic](./Part2_m10_70_cyclic)
 * 1-5) -10C_1-cycle1 to -10C_1-cycle5.txt - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, 5 load-unload cycles - Attempt 1
 * 6) -10C_1-summary.txt - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, Attempt 1 summarised, .txt
 * 7) -10C_1-summary.xlsx - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, Attempt 1 summarised, .xlsx
 * 8-13) -10C_2-cycle1 to -10C_2-cycle7.txt - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, 6 load-unload cycles - Attempt 2
 * 14) -10C_2-summary.txt - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, Attempt 2 summarised, .txt
 * 15) -10C_2-summary.xlsx - Uniaxial Tension Cyclic Loading at -10°C Quasi-steady, Attempt 2 summarised, .xlsx
 * 16-20) 70_2-cycle1 to 70C_2-cycle5.txt - Uniaxial Tension Cyclic Loading at 70°C Quasi-steady, 5 load-unload cycles - Attempt 2
 * 21) 70C_2-summary.txt - Uniaxial Tension Cyclic Loading at 70°C Quasi-steady, Attempt 2 summarised, .txt
 * 22) 70C_2-summary.xlsx - Uniaxial Tension Cyclic Loading at 700°C Quasi-steady, Attempt 2 summarised, .xlsx

3. [Part3_1_Thermal_Cycle_1TC_23cyclic](./Part3_1_Thermal Cycle_1TC_23cyclic)

Thermal cycle followed by mechanical cyclic loading -> To study a reduction in stiffness due to thermal cycling 

Thermal cycle is as follows: 23°C -> -10°C -> 70°C -> 23°C  (Temperature rate: 1-2 K/min )     
  * 1-9) 1TC_23C_1-cycle1 to 1TC_23C_1-cycle9.txt - Attempt 1
  * 10) 1TC_23C_1-summary.txt - Attempt 1 summarised, .txt
  * 11) 1TC_23C_1-summary.xlsx - Attempt 1 summarised, .xlsx
  * 12-20) 1TC_23C_2-cycle1 to 1TC_23C_2-cycle9.txt - Attempt 2
  * 21) 1TC_23C_2-summary.txt - Attempt 2 summarised, .txt
  * 22) 1TC_23C_2-summary.xlsx - Attempt 2 summarised, .xlsx
