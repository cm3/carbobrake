# Tension DMA and code for master curve, etc.

## Tests

1) FTS_TTS_1.csv - Tension DMA with frequency sweeps of 0.1 to 100 Hz in -20°C to 70°C with 10°C increments (Attempt 1).
2) FTS_TTS_2.csv - Tension DMA with frequency sweeps of 0.1 to 100 Hz in -20°C to 70°C with 10°C increments (Attempt 2).
3) Temperature tensile.xlsx - Uniaxial Tensile Tests at 23, 70, -10 °C Quasi-steady (Attempt 1).

## Additionally - 

1) [Code for making DMA, relaxation spectrum and shift factor](./processingCode)


