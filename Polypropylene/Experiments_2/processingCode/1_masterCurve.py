#!/usr/bin/env python
# coding: utf-8

"""
# CODE 1. This is the 1st CODE of the Package -> MasterCurve and Shift Generation -> Generates the CSV files

THIS CODE PRODUCES MasterCurve, and horizontal shift data

NOTE 1: Take care of the Nsweeps and Nfreq lists below -> Change them if available only oen set of DMA data (data_DMA1 only)
NOTE 2: The dataframes having different colHEADs, etc., must be changed depending on what headers are given in the EXPERIMENT DMA RESULTS.
NOTE 3: IN THE FUNCTION: trim_AND_extrapolate, tagged with "HERE IS THE PROBLEM",  the threshold to stop extrapolation is set at 0.14989 and inc = 0.15,
           These values correspond to the log10(freq_step) in between 2 succesive readings of DMA -> Change it depending on your data.

NOTE 4: Doesnt work, currently, for tanD based MasterCurves.  Current implementation is Storage Modulus E' based.

NOTE 5: COMMENT THE drop() lines for Frequencies if you wish to not omit low and high Freq data -> I did it because data was bad.
"""
# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import interpolate
import csv
from scipy.signal import butter,filtfilt


# In[2]:


# Load
data_DMA1 = pd.read_csv("../FTS_TTS_1.csv", sep=';')
data_DMA2 = pd.read_csv("../FTS_TTS_2.csv", sep=';')


# In[3]:


# Check if frequency values are same
if data_DMA1['Freq Hz'].equals(data_DMA2['Freq Hz']):
  print("Frequency Data is SAME in the two sets of DMA data.")
else:
  print("Frequency Data is NOT SAME in the two sets of DMA data.")


# In[4]:


# Check if tan Delta values are the same  (Ep/Edp and Kp/Kdp)
        # Here, K N/m is the stiffness, not the bulk modulus
if data_DMA1['Tan \u03B4 '].equals(data_DMA1['Tan \u03B4 E ']):
  print("Tandelta is SAME for tan\u03B4 and E in DMA1.")
else:
  print("Tandelta is NOT SAME for tan\u03B4 and E in DMA1.")
if data_DMA2['Tan \u03B4 '].equals(data_DMA2['Tan \u03B4 E ']):
  print("Tandelta is SAME for tan\u03B4 and E in DMA2.")
else:
  print("Tandelta is NOT SAME for tan\u03B4 and E in DMA2.")


# In[5]:


# Drop useless columns
data_DMA1 = data_DMA1[data_DMA1.columns[data_DMA1.columns.isin(['Freq Hz','T °C','E Pa','E\' Pa','E\" Pa','Tan \u03B4 '])]]
data_DMA2 = data_DMA2[data_DMA2.columns[data_DMA2.columns.isin(['Freq Hz','T °C','E Pa','E\' Pa','E\" Pa','Tan \u03B4 '])]]


# In[6]:


# Remove any other values (Here we drop all Freq < 0.6 and Freq > 20 because of erratic data)
data_DMA1.drop(data_DMA1.loc[data_DMA1["Freq Hz"] < 0.6].index, inplace = True)
data_DMA1.drop(data_DMA1.loc[data_DMA1["Freq Hz"] > 20].index, inplace = True)
# data_DMA1.reset_index()
data_DMA2.drop(data_DMA2.loc[data_DMA2["Freq Hz"] < 0.6].index, inplace = True)
data_DMA2.drop(data_DMA2.loc[data_DMA2["Freq Hz"] > 20].index, inplace = True)
# data_DMA2.reset_index()
# data_DMA1


# In[7]:


# Take the log of requisite data
data_DMA1.loc[:, 'log10 Freq Hz'] = np.log10(data_DMA1['Freq Hz'])
data_DMA1.loc[:, 'log10 E Pa'] = np.log10(data_DMA1['E Pa'])
data_DMA1.loc[:, 'log10 E\' Pa'] = np.log10(data_DMA1['E\' Pa'])
data_DMA1.loc[:, 'log10 E\" Pa'] = np.log10(data_DMA1['E\" Pa'])
data_DMA1.loc[:, 'log10 Tan \u03B4 '] = np.log10(data_DMA1['Tan \u03B4 '])
data_DMA2.loc[:, 'log10 Freq Hz'] = np.log10(data_DMA2['Freq Hz'])
data_DMA2.loc[:, 'log10 E Pa'] = np.log10(data_DMA2['E Pa'])
data_DMA2.loc[:, 'log10 E\' Pa'] = np.log10(data_DMA2['E\' Pa'])
data_DMA2.loc[:, 'log10 E\" Pa'] = np.log10(data_DMA2['E\" Pa'])
data_DMA2.loc[:, 'log10 Tan \u03B4 '] = np.log10(data_DMA2['Tan \u03B4 '])
# data_DMA1


# In[8]:


# Calculate the no.of sweeps
temp1 = data_DMA1.duplicated(subset='T °C', keep='first').sum() # counts all the data that is repeated
temp2 = data_DMA2.duplicated(subset='T °C', keep='first').sum()
Nsweeps = [len(data_DMA1)-temp1,len(data_DMA2)-temp2] # assuming the frequency sweeps are of equal length
print("Nsweeps =", Nsweeps)


# In[9]:


# Calculate the no.of freq steps
Nfreq = [int(len(data_DMA1)/Nsweeps[0]), int(len(data_DMA2)/Nsweeps[1])] #
print("Nfreq =", Nfreq)


# In[10]:


# Set Tref
Tref = 20.0


# In[11]:


# ------
# Horizontal Shift Code
# ------

# Returns the index value of left most point to be used in the interpolation
def getBracket(xPoint, x):
    # First find the two point bracket of xPoint
    y1 = 0
    for index in range(0, (x.size-1)):  # 0 to 19
        if ( (x[index] <= xPoint) and (xPoint < x[index+1]) ):
            y1 = index
            break

    if ( xPoint == x[x.size-1] ):  # last element, 20
        y1 = x.size - 2

    # Determine the third point that will be used in the Lagrange interp.
    if ( (y1+2) > (x.size-1) ):    # if y1+2 > 20
        y1 = y1-1

    return y1
# ------

def LagrangeInterp(i, xp, x, y):  # Note: the implementation in the next function actually gives xinterp
    p1 = y[i]
    p2 = y[i+1]
    p3 = y[i+2]
    x1 = x[i]
    x2 = x[i+1]
    x3 = x[i+2]

    p12 = p1*(xp - x2)/(x1-x2) + p2*(x1-xp)/(x1-x2)
    p23 = p2*(xp-x3)/(x2-x3) + p3*(x2-xp)/(x2-x3)
    yinterp = p12*(xp-x3)/(x1-x3) + p23*(x1-xp)/(x1-x3)

    return yinterp
# ------

def horizontalShift(LC,UC,Npoints,n,T,Tref,prev_shift):
    # LC - lower curve
    # UC - upper curve
    # Npoints - no.of interpolation points
    # n - no.of interpolation points for the horizontal distance
    # Tref - reference Temperature
    # prev_shift - the shift applied to previous set of data (initially 0.)

    # ------
    # Find the OverlapWindow, the OWlow is the first y value of the upper curve, OWhigh
    # is the last y value of the lower curve.
    # ------
    OWlow = UC[0,1]   # first value of UC
    OWhigh = LC[-1,1] # last value of LC

    # OWlow must be lower than OWhigh, or the interpolation is useless
    if (OWlow < OWhigh):
        # ------
        # Cut the vertical distance between Owlow and OWhigh into Npoints (so, Npoints-1 intervals).
        # Find the interpolated points on the x-axis
        # ------

        OWflag = True

        yPoints = np.linspace(OWlow, OWhigh, Npoints)

        UCxPoints = np.zeros((Npoints,1))
        LCxPoints = np.zeros((Npoints,1))

        for i in range(0, Npoints):
            # print("i",i)
            u1 = getBracket(yPoints[i], UC[:,1])
            l1 = getBracket(yPoints[i], LC[:,1])

            UCxPoints[i] = LagrangeInterp(u1, yPoints[i], UC[:,1], UC[:,0])
            LCxPoints[i] = LagrangeInterp(l1, yPoints[i], LC[:,1], LC[:,0])

        distances = np.abs(UCxPoints - LCxPoints)
        kStart = np.min(distances) - 0.1
        kLast  = np.max(distances) + 0.1
        step = (kLast - kStart)/n

        kIndex = 0
        k_array = np.arange(kStart, kLast, step)
        kSSE = np.zeros((k_array.size,2))

        # Iterate over trail shift values
        for k in k_array:
            # Check SSE for the k value and store it
            UCxShifted = UCxPoints + k
            E = LCxPoints - UCxShifted
            SSE = np.sum(np.square(E))
            kSSE[kIndex,0] = k
            kSSE[kIndex,1] = SSE
            kIndex = kIndex + 1

        # Choose the shift value that lead to the min SSE
        idx_min = np.where( kSSE[:, 1] == np.min(kSSE[:,1]) )
        kBest = kSSE[idx_min[0],0]
        print("Overlap Exists")
        print("kBest is ",kBest)
        print("Max shift is ",np.abs(LC[-1,0] - UC[0,0]))
        shiftby = kBest

    else: # OWlow > OWhigh , i.e., no overlap

        OWflag = False

        if (T>Tref):
            idx = 0
            yPoint = LC[-1,1]
            # xPoint = LagrangeInterp(idx, yPoint, UC[:,1], UC[:,0])
            f = np.poly1d(np.polyfit(UC[:,1], UC[:,0], 2))
            xPoint = f(yPoint)
            print("xPoint is ",xPoint)
            print("Last xCoord in LC is ", LC[-1,0])
            shiftby = abs(LC[-1,0] + np.abs(xPoint))

        elif (T<Tref):
            idx = np.size(UC,0)-3  # -2
            yPoint = UC[0,1]
            # xPoint = LagrangeInterp(idx, yPoint, LC[:,1], LC[:,0])
            f = np.poly1d(np.polyfit(LC[:,1], LC[:,0], 2))
            xPoint = f(yPoint)
            print("xPoint is ",xPoint)
            print("First xCoord in UC is ", UC[0,0])
            shiftby = abs(UC[0,0] - xPoint)

        shiftby_max = np.abs(LC[-1,0] - UC[0,0])
        print("No Overlap - Use Extrapolation")
        print("shiftby is ", shiftby)
        print("Max shift is ",shiftby_max)

    if (T>Tref):
        shiftedDataSnippet = np.zeros((LC.shape))
        shiftedDataSnippet[:,1] = LC[:,1]
        shiftedDataSnippet[:,0] = LC[:,0] - (shiftby+np.abs(prev_shift))
        shiftby *= -1
    elif (T<Tref):
        shiftedDataSnippet = np.zeros((UC.shape))
        shiftedDataSnippet[:,1] = UC[:,1]
        shiftedDataSnippet[:,0] = UC[:,0] + (np.abs(shiftby)+np.abs(prev_shift))
    return shiftedDataSnippet, OWflag, shiftby
# ------

def trim_AND_extrapolate(LC, UC, order, LC_dependent = None, UC_dependent = None, order_dependent = None):

    OWlow = UC[0,1]
    OWhigh = LC[-1,1]

    if (OWlow < OWhigh):
        OWflag = True

        # Trim the overlapped points for the master curve
        idx = np.array(np.where(LC[:,1] > OWlow))
        LC = np.delete(LC, idx, axis = 0)  # Remove upper elements of LC

        if (np.all(LC_dependent) != None):
            LC_dependent = np.delete(LC_dependent, idx, axis = 0)  # Remove upper elements of LC_dependent

    else:
        OWflag = False

        # Extrapolate
        temp_Arr1 = np.append(LC, UC, axis = 0)
        f = np.poly1d(np.polyfit(temp_Arr1[:,0], temp_Arr1[:,1], order)) # Interpolating Polynomial

        if (np.all(LC_dependent) != None):
            temp_Arr1_dependent = np.append(LC_dependent, UC_dependent, axis = 0)
            f_dependent = np.poly1d(np.polyfit(temp_Arr1_dependent[:,0], temp_Arr1_dependent[:,1], order_dependent))
                                                                # Interpolating Polynomial
        """
        # HERE IS THE PROBLEM
        # freq_step = abs(UC[0,0] - UC[1,0]) - 0.05 -> USE THIS FOR THE SECOND CONDITION and inc (little higher) -> MUST CALIBRATE LOOKING AT THE WHOLE DATA
        """
        while ( LC[-1,1] < UC[0,1] and (UC[0,0] - LC[-1,0]) > 0.14989 ):
            inc = 0.15   # This increment in log10 freq Hz is because of the constant difference in succesive freq steps
            xPoint = LC[-1,0] + inc
            yPoint = f(xPoint)
            temp_Arr2 = np.array([xPoint, yPoint]).reshape((1,2))
            LC = np.append(LC, temp_Arr2, axis = 0)

            if (np.all(LC_dependent) != None):
                yPoint_dependent = f_dependent(xPoint)
                temp_Arr2_dependent = np.array([xPoint, yPoint_dependent]).reshape((1,2))
                LC_dependent = np.append(LC_dependent, temp_Arr2_dependent, axis = 0)

        if (np.all(LC_dependent) == None):
            return LC, OWflag
        else:
            return LC, OWflag, LC_dependent
# ------


# In[12]:


# This function gives the rawshifted data and hshift data if no previously shifted data is given
# ... if shifted data is given it gives the rawshifted data directly
def rawShiftFunc(Tref, Nsweeps, Nfreq, unshiftedData, totalShiftGiven = None, vShiftFlag = False, reverseTrendFlag = False):

# If vShift is True
    if vShiftFlag == True:
        unshiftedData[:,1] += np.log10((Tref+273.15)/(unshiftedData[:,2]+273.15))

# reverseTrendFlag --> if calculating shift wrt tanD --> has to be only with vShiftFlag = False
    if (reverseTrendFlag == True) and (vShiftFlag == False):
            print("THIS ALGORITHM DOESN'T function well for tanD") # unshiftedData[:,1] *= -1
    elif (reverseTrendFlag == True) and (vShiftFlag == True):
            print("vertical shifter should not be applied to tanD data, change vShiftFlag = False if reverseTrendFlag = True.")
            return None

# Initialise the rawShiftedCurve
    rawShiftedCurve = np.zeros((unshiftedData.shape))

# Main Loop
    if (np.all(totalShiftGiven) == None): # Calculate shift and shiftedData

        # Initialise
        hShift = np.zeros((Nsweeps,2))
        prev_shift_Tl = 0. # Increments of prev_shift for T<Tref
        prev_shift_Tu = 0. # Increments of prev_shift for T>Tref
        totalShift = np.zeros((Nsweeps,2))

    # If T = Tref exists in the unshiftedData then copy it into masterCurve
        if Tref in unshiftedData[:, 2]:
            idxTref = np.where( unshiftedData[:,2] == Tref )  # This creates a Tuple
            rawShiftedCurve[idxTref] = unshiftedData[idxTref]
            idxTref = np.array(idxTref)
            sweepTref = int(idxTref[0,0]/Nfreq)
            hShift[sweepTref,:] = 0.,Tref
            totalShift[sweepTref,:] = 0.,Tref
            print("Tref exists in the Data")
            print("--------")
        else:
            print("Tref DOESNT EXIST in the Data, pick another Tref")
            print("--------")
            return None

    # For all T>Tref, go higher in the data (since Higher temperatures are at the top of the DMA matrix provided here)
        # Shift LC leftwards
        for i in range(sweepTref, 0, -1):     # Start at 5, stop at 1
            T = unshiftedData[ ( Nfreq*(i-1)), 2]
            print("T =", T)
            UC = unshiftedData[ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
            LC = unshiftedData[ (Nfreq*(i-1)):(Nfreq*i), [0,1] ]
            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC[:,1] *= -1
                UC[:,1] *= -1

            LC_shift, OWflag, hshift = horizontalShift( LC, UC, 21, 100, T, Tref, prev_shift_Tu)

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC_shift[:,1] *= -1

            prev_shift_Tu += hshift
            rawShiftedCurve[ (Nfreq*(i-1)):(Nfreq*i), [0,1]] = LC_shift
            rawShiftedCurve[ (Nfreq*(i-1)):(Nfreq*i), 2] = unshiftedData[ ( Nfreq*(i-1)):(Nfreq*i), 2]
            hShift[i-1,0] = hshift
            hShift[i-1,1] = T
            totalShift[i-1,0] = prev_shift_Tu
            totalShift[i-1,1] = T
            print("--------")

    # For all T<Tref, go higher in the data (since Higher temperatures are at the top of the DMA matrix provided here)
        # Shift LC leftwards
        for i in range(sweepTref, Nsweeps-1, +1):     # Start at 5, stop at 8
            T = unshiftedData[ ( Nfreq*(i+1)), 2]
            print("T =", T)
            LC = unshiftedData[ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
            UC = unshiftedData[ (Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ]

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC[:,1] *= -1
                UC[:,1] *= -1

            UC_shift, OWflag, hshift = horizontalShift( LC, UC, 21, 100, T, Tref, prev_shift_Tl)

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                UC_shift[:,1] *= -1

            prev_shift_Tl += np.abs(hshift) # CHANGE THIS TO abs(hshift) for tanD
            rawShiftedCurve[(Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ] = UC_shift
            rawShiftedCurve[(Nfreq*(i+1)):(Nfreq*(i+2)), 2] = unshiftedData[(Nfreq*(i+1)):(Nfreq*(i+2)), 2]
            hShift[i+1,0] = np.abs(hshift)  # CHANGE THIS TO abs(hshift) for tanD
            hShift[i+1,1] = T
            totalShift[i+1,0] = prev_shift_Tl
            totalShift[i+1,1] = T
            print("--------")

        return hShift, totalShift, rawShiftedCurve

    else: # The following code is if shift data is given.
        if Tref in unshiftedData[:, 2]:
            idxTref = np.where( unshiftedData[:,2] == Tref )  # This creates a Tuple
            rawShiftedCurve[idxTref] = unshiftedData[idxTref]

            idxTref = np.array(idxTref)
            sweepTref = int(idxTref[0,0]/Nfreq)

        for i in range(sweepTref, 0, -1):     # Start at 5, stop at 1

            UC = unshiftedData[ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
            LC = unshiftedData[ (Nfreq*(i-1)):(Nfreq*i), [0,1] ]

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC[:,1] *= -1
                UC[:,1] *= -1

            LC_shift = np.zeros((LC.shape))
            LC_shift[:,0] = LC[:,0] - np.abs(totalShiftGiven[i-1,0])
            LC_shift[:,1] = LC[:,1]

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC_shift[:,1] *= -1

            rawShiftedCurve[ (Nfreq*(i-1)):(Nfreq*i), [0,1]] = LC_shift
            rawShiftedCurve[ (Nfreq*(i-1)):(Nfreq*i), 2] = unshiftedData[ ( Nfreq*(i-1)):(Nfreq*i), 2]


        for i in range(sweepTref, Nsweeps-1, +1):     # Start at 5, stop at 8

            LC = unshiftedData[ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
            UC = unshiftedData[ (Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ]

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                LC[:,1] *= -1
                UC[:,1] *= -1

            UC_shift = np.zeros((UC.shape))
            UC_shift[:,0] = UC[:,0] + np.abs(totalShiftGiven[i+1,0])
            UC_shift[:,1] = UC[:,1]

            if (reverseTrendFlag == True) and (vShiftFlag == False):
                UC_shift[:,1] *= -1

            rawShiftedCurve[(Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ] = UC_shift
            rawShiftedCurve[(Nfreq*(i+1)):(Nfreq*(i+2)), 2] = unshiftedData[(Nfreq*(i+1)):(Nfreq*(i+2)), 2]

        return rawShiftedCurve
# -------


# In[13]:


## Initialise the unshifted and masterCurve data for freq_Ep
unshifted_F_Ep1 = data_DMA1[['log10 Freq Hz', 'log10 E\' Pa', 'T °C']].to_numpy()
unshifted_F_Edp1 = data_DMA1[['log10 Freq Hz', 'log10 E\" Pa', 'T °C']].to_numpy()
unshifted_F_tanD1 = data_DMA1[['log10 Freq Hz', 'log10 Tan \u03B4 ', 'T °C']].to_numpy()
unshifted_F_E1 = data_DMA1[['log10 Freq Hz', 'log10 E Pa', 'T °C']].to_numpy()
unshifted_F_Ep1 = data_DMA1[['log10 Freq Hz', 'log10 E\' Pa', 'T °C']].to_numpy()
unshifted_F_Ep2 = data_DMA2[['log10 Freq Hz', 'log10 E\' Pa', 'T °C']].to_numpy()
unshifted_F_Edp2 = data_DMA2[['log10 Freq Hz', 'log10 E\" Pa', 'T °C']].to_numpy()
unshifted_F_tanD2 = data_DMA2[['log10 Freq Hz', 'log10 Tan \u03B4 ', 'T °C']].to_numpy()
unshifted_F_E2 = data_DMA2[['log10 Freq Hz', 'log10 E Pa', 'T °C']].to_numpy()

hShift_F_Ep, totalShift_F_Ep, rawShiftedCurve_F_Ep =     rawShiftFunc(Tref, Nsweeps[0], Nfreq[0], unshifted_F_Ep1, None, False, False)

rawShiftedCurve_F_Edp = rawShiftFunc(Tref, Nsweeps[0], Nfreq[0], unshifted_F_Edp1, totalShift_F_Ep, True)
rawShiftedCurve_F_E = rawShiftFunc(Tref, Nsweeps[0], Nfreq[0], unshifted_F_E1, totalShift_F_Ep, True)
rawShiftedCurve_F_tanD = rawShiftFunc(Tref, Nsweeps[0], Nfreq[0], unshifted_F_tanD1, totalShift_F_Ep)

print("hShift_F_Ep = ")
print(hShift_F_Ep)
print("--------")
print("totalShift_F_Ep = ")
print(totalShift_F_Ep)
np.savetxt("hshift_freqEp1.csv", totalShift_F_Ep, delimiter=";")


# In[14]:


# Plot unshifted data here
plt.figure(num=1)
# sns.scatterplot(data=data_DMA1, x='log10 Freq Hz', y='log10 Tan \u03B4 ', hue='T °C', palette='Paired')
# sns.scatterplot(data=data_DMA1, x='log10 Freq Hz', y='log10 E\" Pa', hue='T °C', palette='Paired')
sns.scatterplot(data=data_DMA1, x='log10 Freq Hz', y='log10 E\' Pa', hue='T °C', palette='Paired')
plt.xlabel("log10 Freq [Hz]",fontdict={'fontsize':12})
plt.ylabel("log10 E\' [Pa]",fontdict={'fontsize':12})
plt.title("Storage Modulus vs Frequency (Temperature Sweeps)", fontdict={'fontsize':11, 'weight': 'bold'})
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=11)
# plt.savefig("DMA1.jpg", dpi = 100,bbox_inches = 'tight')


# In[15]:


# Plot the shifted data if NEEDED
rawShiftedCurve_F_Ep_DF = pd.DataFrame(rawShiftedCurve_F_Ep, columns = ['log10 shifted Hz','log10 Moduli','T °C'])
rawShiftedCurve_F_Edp_DF = pd.DataFrame(rawShiftedCurve_F_Edp, columns = ['log10 shifted Hz','log10 Moduli','T °C'])
rawShiftedCurve_F_tanD_DF = pd.DataFrame(rawShiftedCurve_F_tanD, columns = ['log10 shifted Hz','log10 Moduli','T °C'])
rawShiftedCurve_F_E_DF = pd.DataFrame(rawShiftedCurve_F_E, columns = ['log10 shifted Hz','log10 Moduli','T °C'])

# plt.figure(num=2)
# sns.scatterplot(data=rawShiftedCurve_F_Ep_DF, x='log10 shifted Hz', y='log10 E\'', hue='T °C', palette='Paired')
# sns.scatterplot(data=rawShiftedCurve_F_Edp_DF, x='log10 shifted Hz', y='log10 E\"', hue='T °C', palette='Paired')
# sns.scatterplot(data=rawShiftedCurve_F_tanD_DF, x='log10 shifted Hz', y='log10 Moduli', hue='T °C', palette='Paired')
# plt.legend(loc='lower right')
# rawShiftedCurve_F_Edp_DF


# In[16]:


## Trim and extrapolate the MasterCurve

# NOTE: Priority is for lower frequencies, interpolation is wrt lower frequencies of the LC/UC

# Overlap - True -> Remove overlap points from LC
# Overlap - False-> Add the extrapolated points to LC at every 0.15 log10 Hz (apparently the freq steps are  equidistant)

def makeMasterCurve(Nsweeps, Nfreq, Tref, rawShiftedCurve, orderInterpolation, rawShiftedCurve_dependent = None, order_dependent = None):
    # For the dependent curve -> construct the interpolating polynomial and extrapolate at the xValues used for the independent curve

    # Initialise
    MasterCurve = np.zeros((1,3))
    if (np.all(rawShiftedCurve_dependent) != None):
        MasterCurve_dependent = np.zeros((1,3))

    # Main Loop
    for i in range(Nsweeps):

        if i < (Nsweeps-1):

            T = rawShiftedCurve[ (Nfreq*(i)), 2]
            LC = rawShiftedCurve[ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
            UC = rawShiftedCurve[ (Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ]

            if (np.all(rawShiftedCurve_dependent) == None):
                LC,OWflag = trim_AND_extrapolate(LC,UC,orderInterpolation)
            else:
                LC_dependent  = rawShiftedCurve_dependent [ (Nfreq*i):(Nfreq*(i+1)), [0,1] ]
                UC_dependent  = rawShiftedCurve_dependent [ (Nfreq*(i+1)):(Nfreq*(i+2)), [0,1] ]
                LC,OWflag,LC_dependent = trim_AND_extrapolate(LC,UC,orderInterpolation, LC_dependent, UC_dependent, order_dependent)
                tempArr_dependent = np.zeros((np.size(LC_dependent,0),3))
                tempArr_dependent[:,[0,1]] = LC_dependent
                tempArr_dependent[:, 2] = T

            tempArr = np.zeros((np.size(LC,0),3))
            tempArr[:,[0,1]] = LC
            tempArr[:, 2] = T

        else: # Since UC is unedited we put it here
            T = rawShiftedCurve[ ( Nfreq*(i)), 2]
            UC = rawShiftedCurve[ (Nfreq*(i)):(Nfreq*(i+1)), [0,1] ]
            tempArr = np.zeros((np.size(UC,0),3))
            tempArr[:,[0,1]] = UC
            tempArr[:, 2] = T

            if (np.all(rawShiftedCurve_dependent) != None):
                UC_dependent = rawShiftedCurve_dependent[ (Nfreq*(i)):(Nfreq*(i+1)), [0,1] ]
                tempArr_dependent = np.zeros((np.size(UC_dependent,0),3))
                tempArr_dependent[:,[0,1]] = UC_dependent
                tempArr_dependent[:, 2] = T

        MasterCurve = np.append(MasterCurve, tempArr, axis = 0)

        if (np.all(rawShiftedCurve_dependent) != None):
            MasterCurve_dependent = np.append(MasterCurve_dependent, tempArr_dependent, axis = 0)

    MasterCurve = np.delete(MasterCurve, 0, axis = 0)    # Remove the first row

    if (np.all(rawShiftedCurve_dependent) != None):
        MasterCurve_dependent = np.delete(MasterCurve_dependent, 0, axis = 0)    # Remove the first row

    if (np.all(rawShiftedCurve_dependent) == None):
        return MasterCurve
    else:
        return MasterCurve, MasterCurve_dependent

MasterCurve_F_Ep, MasterCurve_F_Edp = makeMasterCurve(Nsweeps[0], Nfreq[0], Tref, rawShiftedCurve_F_Ep, 2, rawShiftedCurve_F_Edp, 2)
MasterCurve_F_Ep, MasterCurve_F_E = makeMasterCurve(Nsweeps[0], Nfreq[0], Tref, rawShiftedCurve_F_Ep, 2, rawShiftedCurve_F_E, 2)

# MasterCurve_F_Edp = makeMasterCurve(Nsweeps[0], Nfreq[0], Tref, rawShiftedCurve_F_Edp, 10)
# print(MasterCurve_F_Edp)


# In[17]:


## Filter attempts

"""
T = 1.4         # Sample Period
fs = 30.0       # sample rate, Hz
cutoff = 0.5      # desired cutoff frequency of the filter, Hz ,  slightly higher than actual 1.2 Hz
nyq = 0.5 * fs  # Nyquist Frequency
order = 2       # sin wave can be approx represented as quadratic
n = int(T * fs) # total number of samples

def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

# MasterCurve_F_Ep[:,1] = butter_lowpass_filter(MasterCurve_F_Ep[:,1], 0.2, fs, order)
# MasterCurve_F_Edp[:,1] = butter_lowpass_filter(MasterCurve_F_Edp[:,1], cutoff, fs, order)
# MasterCurve_F_E[:,1] = butter_lowpass_filter(MasterCurve_F_E[:,1], 0.2, fs, order)
"""


# In[18]:


MasterCurve_F_Ep_DF = pd.DataFrame(MasterCurve_F_Ep, columns = ['log10 F Hz','log10 Moduli','T °C'])
plt.figure(num=3)
sns.scatterplot(data=MasterCurve_F_Ep_DF, x='log10 F Hz', y='log10 Moduli', color=".05", s=50)
sns.scatterplot(data=rawShiftedCurve_F_Ep_DF, x='log10 shifted Hz', y='log10 Moduli', hue='T °C', palette='Paired')
plt.xlabel("log10 Freq [Hz]",fontdict={'fontsize':12})
plt.ylabel("log10 E\' [Pa]",fontdict={'fontsize':12})
plt.title("Storage Modulus Master Curve", fontdict={'fontsize':11, 'weight': 'bold'})
plt.legend(loc='lower right', fontsize=11)
# plt.savefig("DMA3.jpg", dpi = 100,bbox_inches = 'tight')


# In[19]:


MasterCurve_F_Edp_DF = pd.DataFrame(MasterCurve_F_Edp, columns = ['log10 F Hz','log10 Moduli','T °C'])
plt.figure(num=4)
sns.scatterplot(data=MasterCurve_F_Edp_DF, x='log10 F Hz', y='log10 Moduli', color=".0", s=50)
sns.scatterplot(data=rawShiftedCurve_F_Edp_DF, x='log10 shifted Hz', y='log10 Moduli', hue='T °C', palette='Paired')
plt.xlabel("log10 Freq [Hz]",fontdict={'fontsize':12})
plt.ylabel("log10 E\" [Pa]",fontdict={'fontsize':12})
plt.title("Loss Modulus Master Curve (dep.)", fontdict={'fontsize':11, 'weight': 'bold'})
plt.legend(loc='lower right', fontsize=11)
# plt.savefig("DMA4.jpg", dpi = 100,bbox_inches = 'tight')


# In[20]:


MasterCurve_F_E_DF = pd.DataFrame(MasterCurve_F_E, columns = ['log10 F Hz','log10 Moduli','T °C'])
plt.figure(num=5)
sns.scatterplot(data=MasterCurve_F_E_DF, x='log10 F Hz', y='log10 Moduli', color=".0", s=100)
sns.scatterplot(data=rawShiftedCurve_F_E_DF, x='log10 shifted Hz', y='log10 Moduli', hue='T °C', palette='Paired')
plt.xlabel("log10 Freq [Hz]",fontdict={'fontsize':12})
plt.ylabel("log10 E [Pa]",fontdict={'fontsize':12})
plt.title("Dynamic Modulus Master Curve (dep.)", fontdict={'fontsize':11, 'weight': 'bold'})
plt.legend(loc='lower right', fontsize=11)
# plt.savefig("DMA5.jpg", dpi = 100,bbox_inches = 'tight')
plt.show()

# In[21]:


np.savetxt("MasterCurve_freqEp1.csv", MasterCurve_F_Ep, delimiter=";")
np.savetxt("MasterCurve_freqEdp1.csv", MasterCurve_F_Edp, delimiter=";")
np.savetxt("MasterCurve_freqE1.csv", MasterCurve_F_E, delimiter=";")
