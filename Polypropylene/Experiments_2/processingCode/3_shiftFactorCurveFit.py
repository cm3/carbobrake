#!/usr/bin/env python
# coding: utf-8

"""
# CODE 3. This is the 3rd CODE of the Package -> GENERATES SHIFT FACTOR CURVE FITS -> Generates the CSV files (I think)

THIS CODE PRODUCES WLF, Arrhenius, and FullTTSP curve fits.

NOTE 1: Uses two different packages independently to produce the same results: lmfit & scipy.optimise
"""
# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import csv

from scipy.optimize import curve_fit
from lmfit import minimize, Parameters, fit_report


# In[2]:


hshift_freqEp1 = pd.read_csv("/home/ujwal/Desktop/Experiments/Kepa_2/Code for processing/hshift_freqEp1.csv",sep=';',header = None)
# hshift_freqEp2 = pd.read_csv("/home/ujwal/Desktop/Experiments/Kepa_2/Code for processing/hshift_freqEp2.csv",sep=';',header = None)

# In[3]:


hshift_freqEp1 = hshift_freqEp1.to_numpy()
# hshift_freqEp2 = hshift_freqEp2.to_numpy()
# print(hshift_freqEp1)


# In[4]:

print("hshift_freqEp1 : ")
print(hshift_freqEp1)


# In[5]:


# Get Tref -> log10(a) is 0 at Tref
index = np.where(hshift_freqEp1[:,0] == 0.)
# print(index)
Tref = float(hshift_freqEp1[index,1])
print('Tref = ' + str(Tref) + ' °C')


# In[6]:


def WLFshift_residual(params, x, ydata = None, uncertainty = None):
    C1 = params['C1']
    C2 = params['C2']
    Tref = params['Tref']

    model_WLF = - ( C1 * (x - Tref) )/ ( C2 + x - Tref )

    if ydata is None:
        return model_WLF
    if uncertainty is None:
        return model_WLF - ydata
    return (model_WLF - ydata) / uncertainty

def WLFshift(x, C1, C2):
    return (- ( C1 * x )/ ( C2 + x ))  # x = T-Tref

def ArrheniusShift_residual(params, x, ydata = None, uncertainty = None):
    Ea = params['Ea']
    # R = params['R']
    Tref_K = params['Tref']

    model_Arr = (Ea/(2.303*8.314))*( 1/(x) - 1/(Tref_K) )

    if ydata is None:
        return model_Arr
    if uncertainty is None:
        return model_Arr - ydata
    return (model_Arr - ydata) / uncertainty

def ArrheniusShift(x, Ea):
    return (Ea/(2.303*8.314) * x)  # x = (1/T - 1/Tref)  T is in Kelvin

def fullTTSP_residual(params, x, ydata = None, uncertainty = None):
    Ea1 = params['Ea1']
    Ea2 = params['Ea2']
    H = params['H']
    Tg = params['Tg']
    Tref_K = params['Tref']

    model_Full =  (Ea1/(2.303*8.314))*( 1/(x) - 1/(Tref_K) )*H*(Tg-x) +                     ( (Ea1/(2.303*8.314))*( 1/(Tg) - 1/(Tref_K) ) + ( Ea2/(2.303*8.314))*(1/(x) - 1/(Tg)) )                                                                         * (1-H*(Tg-x))

    if ydata is None:
        return model_Full
    if uncertainty is None:
        return model_Full - ydata
    return (model_Full - ydata) / uncertainty

def fullTTSP(x, Ea1, Ea2, Tg, H):

    Tref_K = 20.+273.15
    model_Full =  (Ea1/(2.303*8.314))*( 1/(x) - 1/(20.+Tref_K) )*H*(Tg-x) +                     ( (Ea1/(2.303*8.314))*( 1/(Tg) - 1/(Tref_K) ) + ( Ea2/(2.303*8.314))*(1/(x) - 1/(Tg)) )                                                                         * (1-H*(Tg-x))

    return model_Full


# In[7]:


# ------
## WLF
# ------
# hshift_freqEp1 = np.append(hshift_freqEp1,hshift_freqEp2, axis = 0) # USE IF AVAILABLE Two sets of data

print("WLF")
T = hshift_freqEp1[:,1]
ydata = hshift_freqEp1[:,0] * np.log(10)  # Since we like ln(a) instead of log10(a)

# print data
print("T")
print(T)
print("hShift")
print(ydata)

# lmfit
paraWLF = Parameters()
paraWLF.add('C1', value=16.5, min=0.0)
paraWLF.add('C2', value=54.4, min=0.0)
paraWLF.add('Tref', value=Tref, vary = False)

resultWLF = minimize(WLFshift_residual, paraWLF, args=(T, ydata))

print("-------lmfit--------")
print(fit_report(resultWLF))
print("%%%%%%%%%%%%%%%%%%%%")

# scipy_curve_fit
print("-------curve_fit--------")
Tmod1 = hshift_freqEp1[:,1] - Tref
print("Tmod1")
print(Tmod1)
popt1, pcov = curve_fit(WLFshift, Tmod1, ydata, p0 = [16.5, 54.4], bounds=([0., 0.], [np.inf, np.inf]))
print(popt1)
print("--------------------")


# In[8]:


# plot WLF curve_fit
plt.figure(num=1)
plt.plot(T, ydata, 'bs', label='DMA shift data')
plt.plot(T, WLFshift(Tmod1, *popt1), 'r+',label = 'fit: C1=%5.3f, C2=%5.3f' % tuple([135, 258]))
# plt.plot(T, WLFshift(Tmod1, 12, 51), 'g*', label ='exp:C1=12, C2=42')
plt.grid(color='k', linewidth=0.1)
plt.xlabel('T [°C]',fontdict={'fontsize':12})
plt.ylabel('log a',fontdict={'fontsize':13})
plt.legend(loc = "upper right",fontsize=11)
plt.title("WLF Shift Factor Fit", fontdict={'fontsize':12, 'weight': 'bold'})
# plt.savefig("shift1.jpg", dpi = 100,bbox_inches = 'tight')
# plt.show()


# In[9]:


## ------
## Arrhenius
# ------

print("Arrhenius")

# print data
print("T")
print(T)
print("hShift")
print(ydata)

T_K = T + 273.15
Tref_K = Tref + 273.15

# lmfit
paraArr = Parameters()
paraArr.add('Ea', value=1.0e+3, min=0.0)
# paraArr.add('R', value=8.314, vary = False)
paraArr.add('Tref', value=Tref_K, vary = False)

resultArr = minimize(ArrheniusShift_residual, paraArr, args=(T_K, ydata))

print("-------lmfit--------")
print(fit_report(resultArr))
print("%%%%%%%%%%%%%%%%%%%%")

# scipy_curve_fit
print("-------curve_fit--------")
Tmod2 = ( 1/(T_K) - 1/(Tref_K) )
print("Tmod2")
print(Tmod2)
popt2, pcov = curve_fit(ArrheniusShift, Tmod2, ydata, bounds=([0.], [1.e+10]))
print(popt2)
print("--------------------")

# plot curve_fit
plt.figure(num=2)
plt.plot(T_K, ydata, 'bs', label='data')
plt.plot(T_K, ArrheniusShift(Tmod2, *popt2), 'r+',label='fit: Ea=%5.3f' % tuple(popt2))
# plt.plot(T_K, ArrheniusShift(Tmod2, 10.1389e+05), 'g*', label ='exp:Ea=10.1389e+05')
plt.grid(color='k', linewidth=0.1)
plt.xlabel('T [K]')
plt.ylabel('ydata - ln(a)')
plt.title("Arrhenius Shift Factor Fit", fontdict={'fontsize':12, 'weight': 'bold'})
plt.legend()


# In[11]:


# ------
## FullTTSP
# ------

# print data
print("T")
print(T)
print("hShift")
print(ydata)

# T_K = T + 273.15
# Tref_K = Tref + 273.15

# Tg limits
Tg_up = 25.+273.15
Tg_low = 0.+273.15

# lmfit
paraFull = Parameters()
paraFull.add('Ea1', value=1.0e+3, min=0.0)
paraFull.add('Ea2', value=1.0e+3, min=0.0)
paraFull.add('H', value=1.0)
paraFull.add('Tg', value=0., min=Tg_low, max = Tg_up)
paraFull.add('Tref', value=Tref_K, vary = False)

# resultFULL = minimize(fullTTSP_residual, paraArr, args=(T_K, ydata)) # DOESNT WORKS (WHO CARES)

print("-------lmfit--------")
# print(fit_report(resultFULL))
print("%%%%%%%%%%%%%%%%%%%%")

# scipy_curve_fit
print("-------curve_fit--------")
print("T_K")
print(T_K)
popt3, pcov = curve_fit(fullTTSP, T_K, ydata, bounds=( [0., 0., Tg_low, -1*np.inf], [1.e+6, 1.e+6, Tg_up, np.inf]) )
print(popt3)
print("--------------------")

# plot curve_fit
plt.figure(num=3)
plt.plot(T_K, ydata, 'bs', label='data')
plt.plot(T_K, fullTTSP(T_K, *popt3), 'r+',label='curve_fit')
plt.grid(color='k', linewidth=0.1)
plt.xlabel('T [K]')
plt.ylabel('ydata - ln(a)')
plt.title("FULL TTSP Shift Factor Fit", fontdict={'fontsize':12, 'weight': 'bold'})
plt.legend()
plt.show()
