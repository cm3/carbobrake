#!/usr/bin/env python
# coding: utf-8

"""
# CODE 2. This is the 2nd CODE of the Package -> Relaxation Spectrum -> Generates the CSV file for a given value of N

THIS CODE optimises for RELAXATION SPECTRUM (Einf, Ei and ei) using Least Squares objetive function

NOTE 1: SET N below
NOTE 2: Requires MasterCurve data of Storage and Loss Moduli, and Dynamic Moduli -> see below
"""

# In[1]:


import os
import numpy as np
import pandas as pd
import scipy as sp
from scipy.optimize import least_squares
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from math import exp
import matplotlib.pyplot as plt
from functools import reduce

###########################################################################################""
# Set N
N = 20
############################################################################################

# In[2]:


## Import DMA Data - Only for reference
 # data_DMA1 = pd.read_csv('../FTS_TTS_1.csv', sep = ';')
# Remove any other values (Here we drop all Freq < 0.6 and Freq > 20 because of erratic data)
# data_DMA1.drop(data_DMA1.loc[data_DMA1["Freq Hz"] < 0.6].index, inplace = True)
# data_DMA1.drop(data_DMA1.loc[data_DMA1["Freq Hz"] > 20].index, inplace = True)


# In[3]:


# Import Compiled Master Curve Data
MC_freqEp_1 = pd.read_csv("MasterCurve_freqEp1.csv",sep=';',header = None).to_numpy()
MC_freqEdp_1 = pd.read_csv("MasterCurve_freqEdp1.csv",sep=';',header = None).to_numpy()
MC_freqE_1 = pd.read_csv("MasterCurve_freqE1.csv",sep=';',header = None).to_numpy()
# MC_freqEp_2 = pd.read_csv("MasterCurve_freqEp2.csv",sep=';',header = None).to_numpy()
# MC_freqEdp_2 = pd.read_csv("MasterCurve_freqEdp2.csv",sep=';',header = None).to_numpy()
# MC_freqE_2 = pd.read_csv("MasterCurve_freqE2.csv",sep=';',header = None).to_numpy()


# In[4]:


# Form the full df/arrays -> Data Set 1
idx_full = range(0,np.size(MC_freqEp_1,0),1)
freq_full = (10**MC_freqEp_1[idx_full,0]) * 2 * np.pi  #  in rad/s
Ep_full = 10**MC_freqEp_1[idx_full,1]                  # in Pa
Edp_full = 10**MC_freqEdp_1[idx_full,1]                # in Pa
E_full = 10**MC_freqE_1[idx_full,1]                    # in Pa
time_full = 1/(10**MC_freqEp_1[idx_full,0])            # in s
# print(time_full)
# print(Ep_full)

# Data Set 2
"""
idx_full2 = range(0,np.size(MC_freqEp_2,0),1)
freq_full2 = (10**MC_freqEp_2[idx_full2,0]) * 2 * np.pi  #  in rad/s
Ep_full2 = 10**MC_freqEp_2[idx_full2,1]                  # in Pa
Edp_full2 = 10**MC_freqEdp_2[idx_full2,1]                # in Pa
E_full2 = 10**MC_freqE_2[idx_full2,1]                    # in Pa
time_full2 = 1/(10**MC_freqEp_2[idx_full2,0])            # in s
"""


# In[5]:


# Form the partial df/arrays

nVals = 5
idx = range(0, np.size(MC_freqEp_1,0), nVals)
if idx[-1] != (np.size(MC_freqEp_1,0)-1):
    idx = list(idx) + [np.size(MC_freqEp_1,0)-1]  # add the last index regardless

freq = (10**MC_freqEp_1[idx,0]) * 2 * np.pi  #  in rad/s
Ep = 10**MC_freqEp_1[idx,1]                  # in Pa
Edp = 10**MC_freqEdp_1[idx,1]                # in Pa
E = 10**MC_freqE_1[idx,1]                    # in Pa
time = 1/(10**MC_freqEp_1[idx,0])        # in s
# print(time)
# print(list(idx))
# print(Ep)


# In[6]:


# Form the loss function
def func_LeastSquares(x, freq, Ep, Edp):

    ## x -> [Einf, E1, E2, ....., En, e1, e2, ...., en]  2*N+1 elements
             # Ej in 1e-8 Pa
             # ej in log10

    N = int(0.5 * (x.size - 1))

    # Temperorary Variables
    Ep_calc = np.zeros(freq.size) # + Einf
    Edp_calc = np.zeros(freq.size)

    # Initialise the OUTPUT - every 2 elements of this array correspond to a frequency
    F = np.zeros(2*freq.size)

    k = 0
    for i in range(freq.size):

        for j in range(1,N+1):

            Ep_calc[i]  += (x[j]) * (( freq[i] * 10**(x[N+j]))**2) / (1 + ( freq[i] * 10**(x[N+j]))**2)
            Edp_calc[i] += (x[j]) * (  freq[i] * 10**(x[N+j]))     / (1 + ( freq[i] * 10**(x[N+j]))**2)

        Ep_calc[i] += x[0]

        # Ep_calc and Edp_calc are in Pa -> convert to log10 Pa --> then +8 --> Effectively, 1e*8 Pa
        F[k] =   ( (1e+8*(Ep_calc[i]) ) / Ep[i] - 1)**1
        F[k+1] = ( (1e+8*(Edp_calc[i])) / Edp[i] - 1)**1
        k += 2

    return F
# ------


# In[7]:


# Set N
# N = 20

# -------
## Least_Squares Method
# -------

# --------
# 1. Bounds

# 1.1 Bounds for Time
time_min = np.min(time)
time_max = np.max(time)
time_range = (np.linspace(np.log10(time_min), np.log10(time_max), N+1))
# print(time_range)

time_bound_low = list(time_range[0:N])
time_bound_up = list(time_range[1:N+1])

# 1.2 Bounds for Moduli -> 0 to +1*inf
lowerBounds = list(np.zeros((N+1)))
upperBounds = list(np.ones ((N+1))*1e+2)


# Add time bounds to the above lists
lowerBounds += time_bound_low
upperBounds += time_bound_up

bounds = tuple([ lowerBounds, upperBounds])
print("bounds = ", bounds)

# 2. Initialisation
x0 = np.random.rand(2*N+1)

# Bounds for time constants -> in log10
x0[N+1:2*N+2] = (np.array(lowerBounds[N+1:2*N+2])                  + x0[N+1:2*N+2]*(np.array(upperBounds[N+1:2*N+2]) - np.array(lowerBounds[N+1:2*N+2]) ))

print("------------")
print("x0 = ", x0)

res_lsq = least_squares(func_LeastSquares, x0, args=(freq, Ep, Edp), bounds=bounds, method = 'dogbox' )


# In[8]:


print("leastSquares fit - 2*N+1 parameters: ")
res_lsq.x


# In[9]:


# Use the solution to plot the relaxation function -> Data Set 1
Einf = 1e+8*(res_lsq.x[0])
Ej = 1e+8*(res_lsq.x[1:N+1])
ej = 10**(res_lsq.x[N+1:2*N+1])
E_calc_full = Einf*np.ones((time_full.shape))
E_calc = Einf*np.ones((time.shape))

for i in range(time_full.size):
    for j in range(N):
        E_calc_full[i] += Ej[j]*exp(-time_full[i]/ej[j])

for i in range(time.size):
    for j in range(N):
        E_calc[i] += Ej[j]*exp(-time[i]/ej[j])

plt.figure(num=1)
plt.plot(np.log10(time_full), np.log10(E_full), label = "Master Curve",linewidth=2)
# plt.plot(np.log10(time), np.log10(E), label = "MasterCurve_partial",linewidth=2)
# plt.plot(np.log10(time_full), np.log10(E_calc_full), label = "RelaxationSpectra_full",linewidth=4)
plt.plot(np.log10(time), np.log10(E_calc), label = "Relaxation Function",linewidth=2)
# plt.plot(np.log10(time_full2), np.log10(E_full2), label = "MasterCurve_full2",linewidth=1)
plt.grid(color='k', linewidth=0.1)
plt.xlabel("log10 t [s]",fontdict={'fontsize':11.5})
plt.ylabel("log10 E(t) [Pa]",fontdict={'fontsize':11.5})
plt.title("Relaxation Modulus vs. Time, N = %d" %N, fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.legend(fontsize = 11)
# plt.savefig("Evst.jpg",dpi = 100)

print("Einf = ", Einf)
print("Ej = ", Ej)
print("ej = ", ej)


# In[10]:


# plot relaxation spectrum
plt.figure(num=3)
plt.scatter(np.log10(ej), np.log10(Ej), label = "Relaxation Spectrum",marker = '.')
plt.xlabel("Time Constants [log10 s]",fontdict={'fontsize':11.5})
plt.ylabel("Branch Moduli [log10 MPa]",fontdict={'fontsize':11.5})
plt.title("Optimised Parameters Ei and ei, N = %d" %N, fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.legend(fontsize = 11)


# In[11]:


Ep_calc_full = Einf*np.ones((freq_full.shape))
for i in range(freq_full.size):
    for j in range(N):
        Ep_calc_full[i] += Ej[j] * (np.square(freq_full[i]*ej[j])) / (1 + np.square(freq_full[i]*ej[j]))

plt.figure(num=2)
plt.plot(np.log10(freq_full), np.log10(Ep_full), label = "MasterCurve_full",linewidth=6)
plt.plot(np.log10(freq), np.log10(Ep), label = "MasterCurve_partial",linewidth=2)
plt.plot(np.log10(freq_full), np.log10(Ep_calc_full), label = "RelaxationSpectra_full",linewidth=4)
plt.title("Storage Modulus vs. Freq, log10-log10 scale, N = %d" %N, fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.legend()
plt.show()


# In[12]:


## TO CHECK
# print(Ep_full)
# print(Ep_calc)


# In[15]:


# print to a csv file

relSpec = np.zeros((res_lsq.x.shape))
relSpec[0] = Einf
relSpec[1:N+1] = Ej
relSpec[N+1:2*N+2] = ej
np.savetxt("relaxationSpectrum1_N%d.csv" %N, relSpec, delimiter=";")

# print(relSpec)
# print(res_lsq.x)

"""
def func_localmin(x, freq, Ep, Edp):
        # x -> [Einf, E1, E2, ....., En, e1, e2, ...., en]  2*N+1 elements

    N = int(0.5 * (x.size - 1))

    # Temperorary Variables
    Ep_calc = np.zeros(freq.size) # + Einf
    Edp_calc = np.zeros(freq.size)

    # Initialise the OUTPUT - every 2 elements of this array correspond to a frequency
    F = 0.

    k = 0
    for i in range(freq.size):

        for j in range(1,N+1):

            Ep_calc[i] += x[j] * (np.square(freq[i]*x[N+j])) / (1 + np.square(freq[i]*x[N+j]))
            Edp_calc[i] += x[j] * (freq[i]*x[N+j]) / (1 + np.square(freq[i]*x[N+j]))

        Ep_calc[i] += x[0]

        F += np.square(Ep_calc[i]/Ep[i] - 1)
        F += np.square(Edp_calc[i]/Edp[i] - 1)

    return F


# Local Minimization
bounds = tuple([tuple([0, None])])
for i in range(2*N):
    bounds += tuple([tuple([0, None])])
constraint = tuple([{'type':'ineq', 'fun': lambda x,i=i: x[i+1] - x[i]} for i in range(N+1, 2*N)]
           + [{'type':'eq', 'fun': lambda x,i=i: 0 if x[j] != x[j+1] else 1} for j in range(2*N)])
res_slspq = minimize(func_localmin, x0, args=(freq, Ep, Edp), method='SLSQP', bounds = bounds,\
             options={'ftol': 1e-06, 'disp': True}, constraints = constraint)
"""
"""
# Global Minimisation
x0_global = res_slspq.x
minimizer_kwargs={"method": "trust-constr", "args" : tuple([freq, Ep, Edp]), "bounds" : bounds, "constraints" :  constraint}

class globalBounds:

    def __init__(self, xmax=list(np.ones((2*N+1,))*np.inf), xmin=list(np.zeros((2*N+1,))) ):
        self.xmax = np.array(xmax)
        self.xmin = np.array(xmin)

    def __call__(self, **kwargs):
        x = kwargs["x_new"]
        tmax = bool(np.all(x <= self.xmax))
        tmin = bool(np.all(x >= self.xmin))

        return tmax and tmin

res_bh = basinhopping(func_localmin, x0_global, niter=100, T=100.0, stepsize=1, minimizer_kwargs=minimizer_kwargs,\
                            take_step=None, accept_test=None, callback=None, interval=10, \
                            disp=True, niter_success=None, seed=None)
"""
# In[ ]:
