#!/usr/bin/env python
# coding: utf-8

"""
# CODE 3.1 This is the Last CODE of the Package -> INCOMPLETE for NOW

THIS CODE NEEDS MasterCurve, Relaxation Spectrum and hShift DATA

NOTE 1: YOU HAVE to GIVE THE DATA MANUALLY for GIBSON CUrve FIT
NOTE 2: Cp, CTE, KTHCon are not accurate, you must give them data separately "INCOMPLETE CODE"
"""
# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import csv

from scipy.optimize import curve_fit
from lmfit import minimize, Parameters, fit_report


# In[2]:


# Load MasterCurve

MC_freqE_1 = pd.read_csv("MasterCurve_freqE1.csv",sep=';',header = None).to_numpy()


# In[3]:


# Load Relaxation Spectrum

relSpec1 = pd.read_csv("relaxationSpectrum1.csv",sep=';',header = None).to_numpy()


# In[4]:


# Load shift data for Tref

hshift_freqEp1 = pd.read_csv("hshift_freqEp1.csv",sep=';',header = None).to_numpy()
# hshift_freqEp2 = pd.read_csv("hshift_freqEp2.csv",sep=';',header = None)

# Get Tref -> log10(a) is 0 at Tref
index = np.where(hshift_freqEp1[:,0] == 0.)
# print(index)
Tref = float(hshift_freqEp1[index,1]) + 273.15
print('Tref = ' + str(Tref) + ' K')


# In[5]:


# WLF and Arrhenius Curve Fits

hshift_freqEp1 = np.append(hshift_freqEp1,hshift_freqEp2, axis = 0)

def WLFshift(x, C1, C2):
    return (- ( C1 * x )/ ( C2 + x ))  # x = T-Tref

 # ------

def ArrheniusShift(x, Ea):
    return (Ea/(2.303*8.314) * x)  # x = (1/T - 1/Tref)  T is in Kelvin

 # ------


# In[6]:


# Initiate some data

hshift_freqEp1 = np.append(hshift_freqEp1,hshift_freqEp2, axis = 0)

T = hshift_freqEp1[:,1]
T += 273.15
ydata = hshift_freqEp1[:,0] * np.log(10)  # Since we like ln(a) instead of log10(a)


# In[7]:


# WLF and Arrhenius Curve Fits

print("WLF")
Tmod1 = hshift_freqEp1[:,1] - Tref
popt1, pcov = curve_fit(WLFshift, Tmod1, ydata, p0 = [16.5, 54.4], bounds=([0., 0.], [np.inf, np.inf]))
print(popt1)
print("--------------------")

print("Arrhenius")
Tmod2 = ( 1/(T) - 1/(Tref) )
popt2, pcov = curve_fit(ArrheniusShift, Tmod2, ydata, bounds=([0.], [1.e+10]))
print(popt2)
print("--------------------")

# plot curve_fit
plt.figure(num=1)
plt.plot(T, ydata, 'bs', label='data')
plt.plot(T, WLFshift(Tmod1, *popt1), 'r+',label = 'fit: C1=%5.3f, C2=%5.3f' % tuple(popt1))
plt.plot(T, ArrheniusShift(Tmod2, *popt2), 'r+',label='fit: Ea=%5.3f' % tuple(popt2))
plt.xlabel('T [°C]')
plt.ylabel('ydata - ln(a)')
plt.legend(loc = "upper center")


# In[8]:


# Gibson and Huang Functions

def Gibson_residual(params, x, ydata = None, uncertainty = None):
    PU = params['PU']
    PR = params['PR']
    Tref = params['Tref']
    kprime = params['kprime']

    model_Gibson = (PU+PR)/2 - (PU-PR)/2 * np.tanh(kprime*(x-Tref))

    if ydata is None:
        return model_Gibson
    if uncertainty is None:
        return model_Gibson - ydata
    return (model_Gibson - ydata) / uncertainty

def Gibson(x, PU, PR, kprime):
    return ( (PU+PR)/2 - (PU-PR)/2 * np.tanh(kprime*(x-Tref)) )

 # ------

def Huang_residual(params, x, ydata = None, uncertainty = None):
    PU = params['PU']
    PR = params['PR']
    Tref = params['Tref']
    k = params['k']
    m = params['m']

    model_Huang = PU - (PU-PR) / ((m*np.exp(-m*k*(x-Tref)) + 1)**(1/m))

    if ydata is None:
        return model_Huang
    if uncertainty is None:
        return model_Huang - ydata
    return (model_Huang - ydata) / uncertainty

def Huang(x, PU, PR, k, m):
    return PU - (PU-PR) / ((m*np.exp(-m*k*(x-Tref)) + 1)**(1/m))

# ------


# In[9]:


# Generate data for Gibson/Huang fits - > From QS Tensile Experiments, we get the Moduli

data = np.array([[(2446.85+2599.66+2581.83)/3 *1e+6, -10+273.15],                  [(1533.37+1553.33+1612.56)/3 *1e+6, 23+273.15],                  [(608.82+646.855)/2 *1e+6, 70+273.15]])


# In[10]:


# Gibson and Huang Curve fit

T_range = np.linspace(-60,100,100)
T_range += 273.15
T_range_mod1 = T_range - Tref
T_range_mod2 = (1/(T_range) - 1/(Tref))

WLF_result = WLFshift(T_range_mod1, *popt1)
Arr_result = ArrheniusShift(T_range_mod2, *popt2)

# lmfit
paraGibson = Parameters()
paraGibson.add('PU', value=1.0e+9, min=10**np.min(MC_freqE_1[:,1]), max=1.e+10)
paraGibson.add('PR', value=1.0e+9, min = 0., max=5*10**np.max(MC_freqE_1[:,1]))
paraGibson.add('Tref', value=Tref, vary = False)
paraGibson.add('kprime', value=0.005, min=0.0)

resultGibson = minimize(Gibson_residual, paraGibson, args=(data[:,1], data[:,0]))

print("-------lmfit--------")
print(fit_report(resultGibson))
print("%%%%%%%%%%%%%%%%%%%%")

# scipy_curve_fit
print("-------curve_fit--------")
popt3, pcov = curve_fit(Gibson, data[:,1], data[:,0],                         bounds=( [10**np.min(MC_freqE_1[:,1]), 0., 0.],                                  [10.e+9, 5*10**np.min(MC_freqE_1[:,1]), np.inf]) )
popt4, pcov = curve_fit(Huang, data[:,1], data[:,0],                         bounds=( [10**np.min(MC_freqE_1[:,1]), 0., 0., 0.],                                  [10.e+9, 5*10**np.min(MC_freqE_1[:,1]), np.inf, np.inf]) )
print(popt3)
print("--------------------")
print(popt4)
print("--------------------")


# In[11]:


# plot curve_fit
plt.figure(num=2)
plt.plot(data[:,1]-273.15, data[:,0], 'bs', label='QS Tension data')
# plt.plot(T_range, Gibson(T_range, *popt3), 'r+',label = 'fit: PU=%5.3f, PR=%5.3f, kprime=%5.3f' % tuple(popt3))
plt.plot(T_range-273.15, Gibson(T_range, *popt3), 'r+',label = 'fit: PU=2.8e+9,\n     PR=5.5e+8,\n     k\'=0.034')
# plt.plot(T_range, Huang(T_range, *popt4), 'g+',label = 'fit: PU=%5.3f, PR=%5.3f, k=%5.3f, m=%5.3f' % tuple(popt4))
plt.grid(color='k', linewidth=0.1)
plt.xlabel('T [°C]', fontdict={'fontsize':12})
plt.ylabel('Young\'s Modulus [Pa]', fontdict={'fontsize':12})
plt.legend(loc = "upper center")
plt.legend(loc='upper right', fontsize = 11)
plt.title("Gibson Temperature Function Fit", fontdict={'fontsize':12, 'weight': 'bold'})
# plt.savefig("gibson1.jpg", dpi = 100,bbox_inches = 'tight')


# In[12]:


# Specific Heat (Trial)
   # plot curve_fit
fig3 = plt.figure(num=3)
plt.plot(T_range-273.15, Gibson(T_range, 1700, 2100, 3.36435338e-02)/1000, 'r+',label = 'fit')
# plt.plot(T_range, Huang(T_range, *popt4), 'g+',label = 'fit: PU=%5.3f, PR=%5.3f, k=%5.3f, m=%5.3f' % tuple(popt4))
plt.grid(color='k', linewidth=0.1)
plt.xlabel('T [°C]', fontdict={'fontsize':12})
plt.ylabel('Cp [kJ/kg/K]', fontdict={'fontsize':12})
plt.legend(loc = "upper center")
# plt.xlim([200, 400])
# plt.ylim([0, 3.25e+9])
plt.legend(loc='upper left', fontsize = 11)
plt.title("Gibson Temperature Function - Specific Heat", fontdict={'fontsize':12, 'weight': 'bold'})
# plt.savefig("gibson2.jpg", dpi = 100,bbox_inches = 'tight')


# In[13]:


# Alpha (Trial)
   # plot curve_fit
plt.figure(num=4)
plt.plot(T_range, Gibson(T_range, 0.2, 1, 3.36435338e-02), 'b+',label = 'fit')
# plt.plot(T_range, Huang(T_range, *popt4), 'g+',label = 'fit: PU=%5.3f, PR=%5.3f, k=%5.3f, m=%5.3f' % tuple(popt4))
plt.xlabel('T [K]')
plt.ylabel('CTE [*1e-5]')
plt.legend(loc = "upper center")
plt.xlim([200, 400])
# plt.ylim([0, 3.25e+9])
plt.legend(loc='center left')


# In[14]:


# KThCon (Trial)
   # plot curve_fit
plt.figure(num=5)
plt.plot(T_range, Gibson(T_range, 0.10, 1.5*0.12, 3.36435338e-02), 'g+',label = 'fit')
# plt.plot(T_range, Huang(T_range, *popt4), 'g+',label = 'fit: PU=%5.3f, PR=%5.3f, k=%5.3f, m=%5.3f' % tuple(popt4))
plt.xlabel('T [K]')
plt.ylabel('KThCon (SI units)')
plt.legend(loc = "upper center")
plt.xlim([200, 400])
# plt.ylim([0, 3.25e+9])
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()


# In[ ]:
