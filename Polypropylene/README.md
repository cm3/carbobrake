# Material: 

Borealis Polypropylene BJ380MO

# Tests performed

1) [Information: Initial planning of thermo(visco)mechanical material experimental campaign](./Information)
2) [Experiments_1: Temperature sweep, DMA demo](./Experiments_1)
3) [Experiments_2: Tension DMA and code for master curve, etc.](./Experiments_2)
4) [Experiments_3: Compression and Tension Uniaxial Tests](./Experiments_3)
5) [Experiments_4: Isothermal cyclic tests and Thermal followed by mechanical cyclic tests](./Experiments_4)
6) [Experiments_5: Thermo-mech cyclic tests](./Experiments_5)
7) [Experiments_6: Compression and bending DMA](./Experiments_6)
