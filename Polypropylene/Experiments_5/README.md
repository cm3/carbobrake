# Thermo-mech cyclic tests

In this folder is 1 folder [CARBOBRAKE_CYCLIC TESTS](./CARBOBRAKE_CYCLIC TESTS), within which there are 9 folders, within which there are 2 folders each (Don't Panic: Its only half of a Russian Doll):

Full test plan is enlisted along with the contents of the folders.

## Mechanical Cycles Isothermal -> These tests are the same as the cyclic loading tests shown before

### Tension

1. FOLDERS: A.1/ to A.3/ -> Each folder has 2 folders within = 2 attempts
 * 1. QS tests:
   * A.1) -10°C ->  VE: 0.5%, Pre-Peak: 1.0%, Peak Region: 2.0%, Post-Peak:
3.0-4.5-6.0%
   * A.2) 23°C  ->  VE: 0.5%, Pre-Peak: 1.0-1.5-2.0-2.5-3.0-3.5%, Peak Region:
4.0%, Post-Peak: 4.5-5.0%
   * A.3) 70°C  -> (yields too fast) Pre-Peak: 1.0-2.5%, Peak: 5.0%,  Post-Peak:
10.0-20.0-30.0%
 * 2) Repeat the cyclic loading of point 1) after one and 10 thermal loading up to high temperature and cooling to check if the thermal cycle affect the mechanical properties

2. FOLDERS: 2.1/ to 2.4/ -> Each folder has 2 folders within = 2 attempts_
  * 1. Thermal load -> Mech load (just one post-peak)
    * 2.1) Start at room temperature -> cool to -10°C -> heat to 70°C -> repeat 5
times (-10 to 70°C) -> perform a.3  (Rubbery)
    * 2.2) Start at room temperature -> cool to -10°C -> heat to 70°C -> cool to
23°C -> perform a.2 (Glassy)
    * 2.3) Repeat 2.2 10 times -> perform a.2
    * 2.4) Start at room temperature -> cool to -10°C -> heat to 70°C -> cool
to -10°C -> repeat 5 times -> perform a.1 (Glassy)
  * 2. Thermomech cycles and load reversal if possible

3. FOLDERS: 3.1/ to 3.2/ -> Each folder has 2 folders within = 2 attempts
  * 3.1) Start at Room temperature -> cool to -10 °C (glass) -> deform to 0.5%
VE, hold -> heat to 23°C (Tg region)-> unload -> deform to 0.5% VE, hold ->
cool to -10°C -> ..(repeat 2 times, end to 23°C).. ->
      -> heat to 70°C (rubbery) -> unload -> deform to 3.0% pre-peak,
hold ->  cool to -10°C -> unload -> deform to 4.0% post-peak
  * 3.2) Start at Room temperature -> cool to -10°C -> deform to 3.0% tension,
hold -> heat to 70 °C -> unload -> deform to 5%, hold -> cool to -10 °C ->
unload -> deform to 3.0% compression
