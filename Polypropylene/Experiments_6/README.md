# Compression and bending DMA

 * 1) CB_PP_FTS.csv - Compression DMA, Frequency sweeps of 1 to 100 Hz at 70 to -20 °C with 10 °C increments.
 * 2) CB_PP_TEMPERATURE.csv - Compression DMA, Temperature Sweep of -25 to 70 °C at 10 Hz.
 * 3) PP_bending_TTS.csv - Bending DMA, Frequency sweeps of 1 to 10 Hz at 70 to -20 °C with 5 °C increments.
