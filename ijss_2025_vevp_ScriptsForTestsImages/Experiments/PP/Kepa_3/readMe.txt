In this folder:

Material: Borealis Polypropylene BJ380MO

1) ZT1075_Carbo Brake_Compression_20220607.xlsx - Uniaxial Compression Tests at 23, 70, -10 °C Quasi-steady.
2) ZT1075_Carbo Brake_Tensile_20220607.xlsx - Uniaxial Tension Tests at 23, 70, -10 °C Quasi-steady (Attempt 1 Repeat).
