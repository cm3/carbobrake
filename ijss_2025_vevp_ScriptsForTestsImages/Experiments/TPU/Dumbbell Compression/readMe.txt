This directory contains the experimental data obtained from the mechanical tests on Hollow Dumbbell 3D printed TPU samples done by JKU.
The samples tested are printed in transversal (Z) and parallel (X) directions.
The folders are named as TPUalt_X_????mms_1 or TPUalt_Z_????mms_1. Strain rate is within the title.
    - TPUalt_X_????mms_1 - Uniaxial compression tests at room temperature (RT) at different strain rates - printed in X.
    - TPUalt_Z_????mms_1 - Uniaxial compression tests at room temperature (RT) at different strain rates - printed in Z.
Also:
    - "infrarot" - infrared Aramis videos for temperatures during compression testing.


Note: Only samples printed in X direction are used in the paper to get the full displacement range response.
