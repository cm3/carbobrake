This directory contains the experimental data obtained from the mechanical tests on bulk 3D printed TPU samples done by IMDEA.
The samples tested are printed in transversal (H) and parallel (V) directions, and an extra direction, longitudinal (L) for shear tests.
The tests are
    - Uniaxial tensile tests at room temperature (RT) at different strain rates. - Tensile_bulk_RoomT_TPU.zip
    - Uniaxial tensile tests at 45ºC at different strain rates. (to be done)
    - Uniaxial compression tests at room temperature (RT) at different strain rates. - Compression_bulk_RoomT_TPU.zip
    - Uniaxial compression tests at 45ºC at different strain rates. (to be done)
    - Stress relaxation tests by stepped uniaxial compression. - Relaxation_bulk_RoomT_TPU.zip
    - Load-unload compresison tests by uniaxial compression. - Hysteresis_softening_RoomT_TPU.zip
    - Load-unload cyclic tests by uniaxial tension. (low sucess) - Cyclic_Tensile_RoomT_TPU.zip
    - Load-unload cyclic tests by uniaxial compression. - Cyclic_Compressive_RoomT_TPU.zip

As files are uploaded, .zip file names will appear referring to each section


Note: Refer to the paper for sample sizes and experimental setup.
