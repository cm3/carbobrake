Data obtained directly from the machine measurements
Very low experimental success, so only one direction and one strain rate was tested.

To compensate the absence of tensile cyclic characterization, cyclic compression tests are performed (in other .zip)

Name of file: testtype_specimenlabelanddirection_strainrate_temperature_material.csv

Uniaxial tensile tests were performed on SLS samples printed ONLY in parallel (V) direction as shown in layers.pdf

Universal testing machine: INSTRON 5966
Load cell: 2kN
Temperature: Room temperature (RT)
Strain rate (s-1) :
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)