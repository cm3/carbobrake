Data obtained directly from the machine measurements

Name of file: testtype_specimenlabeldirection_maxstrain_strainrate_temperature_material.csv

Hysteresis loops were obtained by compressive loading and unloading at different strain rates following the next steps:
	1. Load up to 0.5 or 1 true strain. 
	2. Unload to F=0N

Compression stress and strain in positive values.

Universal testing machine: INSTRON 3384
Load cell: 10kN
Temperature: Room temperature (RT)
Strain rate (s-1) : 
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)