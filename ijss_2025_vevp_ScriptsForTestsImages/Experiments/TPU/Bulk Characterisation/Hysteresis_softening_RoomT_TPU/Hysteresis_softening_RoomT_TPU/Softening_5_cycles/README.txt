Data obtained directly from the machine measurements

Name of file: testtype_specimenlabeldirection_strainrate_temperature_material.csv

Softening was studied by compressive loading and unloading cyclically at 0.01 s.1 strain rate following the next steps:
	1. Load up to 1 true strain. 
	2. Unload to F=0N
	3. Repeat steps 1 and 2 four times (5 cycles)


Compression stress and strain in positive values.

Universal testing machine: INSTRON 3384
Load cell: 10kN
Temperature: Room temperature (RT)
Strain rate (s-1) : 
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)