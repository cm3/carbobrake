Data obtained directly from the machine measurements

Name of file: testtype_specimenlabeldirectionregime_strainrate_temperature_material.csv

Different tests under uniaxial compression were performed on SLS samples printed in transverse (H) and parallel (V) direction as shown in layers.pdf to study:
 	Hysteresis loops
	Softening