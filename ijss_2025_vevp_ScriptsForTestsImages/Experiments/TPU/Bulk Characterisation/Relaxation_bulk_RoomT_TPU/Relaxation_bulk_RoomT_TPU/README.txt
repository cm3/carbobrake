Data obtained directly from the machine measurements

Name of file: testtype_specimenlabeldirection_strainrate_temperature_material.csv

Stress relaxation was studied by compressive loading and unloading by steps at 0.1 s-1 strain rate following the next steps:
	1. Load up to 0.2 true strain
	2. Hold strain for 1 min
	3. Load to 0.4 true strain
	4. Hold strain for 1 min
	5. Load to 0.6 true strain
	6. Hold strain for 1 min
	7. Load to 0.8 true strain
	8. Hold strain for 1 min
	9. Load to 1 true strain
	10. Unload following reverse steps.	

These tests were performed on SLS samples printed in transverse (H) and parallel (V) direction as shown in layers.pdf

Compression stress and strain in positive values.

Universal testing machine: INSTRON 3384
Load cell: 10kN
Temperature: Room temperature (RT)
Strain rate (s-1) : 
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)