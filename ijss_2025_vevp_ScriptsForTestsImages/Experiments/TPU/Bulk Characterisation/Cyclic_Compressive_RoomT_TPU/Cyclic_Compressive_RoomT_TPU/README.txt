Data obtained directly from the machine measurements.
Very low experimental success, so only one direction and one strain rate was tested.

To compensate the absence of tensile cyclic characterization, cyclic compression tests are performed at 3 different strain rates:
	2.78E-3, 5.56E-3 and 2.78E-2

Samples are loaded until the extension is -1mm more than the step before, and then they are unloaded until the force reaches -1N.

Name of file: testtype_specimenlabelanddirection_strainrate_temperature_material.csv

Uniaxial cyclic compression tests were performed on SLS samples printed in transverse (H) and parallel (V) direction as shown in layers.pdf

Attention: All compressive stress and strains are in positive values.

Universal testing machine: INSTRON 5966
Load cell: 10kN
Temperature: Room temperature (RT)
Strain rate (s-1) :
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)