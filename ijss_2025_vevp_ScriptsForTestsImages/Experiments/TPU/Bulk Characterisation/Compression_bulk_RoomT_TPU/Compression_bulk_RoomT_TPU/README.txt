Data obtained directly from the machine measurements.

Compression tensile tests were performed on SLS samples printed in transverse (H) and parallel (V) direction as shown in layers.pdf

Name of file: testtype_specimenlabelanddirection_strainrate_temperature_material.csv

Compressive Eng.stress and Eng.strains have positive values.

Testing machine: INSTRON 3384
Load cell: 10kN
Temperature: Room temperature (RT)
Strain rate (s-1) :
Specimen properties : Specimen label
Specimen properties : Thickness (mm)
Specimen properties : Width (mm)
Specimen properties : Height (mm)

Time (s)	Extension (mm)	Load (N)	Eng.strain	Eng.stress (MPa)


