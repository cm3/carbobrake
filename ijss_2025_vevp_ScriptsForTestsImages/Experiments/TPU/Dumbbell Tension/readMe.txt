This directory contains the tension experiments on Hollow Dumbbell 3D printed TPU samples done by JKU taken from:
  Characterization of the Fatigue Behavior of SLS Thermoplastics, Zoltan Major 2019.

The samples tested are printed in X directions.

The files are named as "0_1/1/10/100"mm_s. Strain rate is within the title.
