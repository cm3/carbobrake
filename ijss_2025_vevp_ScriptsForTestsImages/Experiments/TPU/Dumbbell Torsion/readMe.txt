This directory contains the torsion experiments on Hollow Dumbbell 3D printed TPU samples done by JKU.

The files are named as TPUneu_"X/Z"_"strainRate"deg_s_"experiment number"_"F0/S0"_Kanäle _Messwerte__1.
   - The columns are named within the files. Specific columns are extracted.

The samples tested are printed in transversal (Z) and parallel (X) directions. Strain rate is in the title.
F0 - axial displacement is unconstrained; (Zero axial force to see Poynting effect).
S0 - axial displacement is constrained; (Non-zero axial force).

Note: Only samples printed in X direction are used in the paper to get the full displacement range response.
