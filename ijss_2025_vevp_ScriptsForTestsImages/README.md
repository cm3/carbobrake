# Carbobrake

This research has been funded by the Walloon Region under the agreement no. 2010092-CARBOBRAKE in the context of the M-ERA.Net Join Call 2020. Funded by the European Union under the Grant Agreement no. 101102316. Views and opinions expressed are those of the author(s) only and do not necessarily reflect those of the European Union or the European Commission. Neither the European Union nor the granting authority can be held responsible for them.

# MOAMMM 

TPU experimental results were obtained in MOAMMM
project which has received funding from the H2020-EU.1.2.1.-FET Open Programme project MOAMMM
under grant No 862015. Views and opinions expressed are those of the author(s) only and do not
necessarily reflect those of the European Union or the European Commission. Neither the European
Union nor the granting authority can be held responsible for them.

# License

Licensed under the Creative Commons Attribution 4.0 International (CC BY 4.0) (https://creativecommons.org/licenses/by/4.0).

# Directories

This directory contains 3 Folders with several subfolders:

1. Calibration 
  - Split into two subfolders by material type.
  
  - Each folder has the following:
    * "Polypropylene/TPU"_Experiments_monotonic_cyclic.py - Run to generate requisite experimental plots and their pdfs.
    * loadCase_ParaValues_"PP/TPU"_cube.py - Library of parameter values (No output).
    * runCaseScript_"PP/TPU"_cube.py - This is base input file fully parametrised (No output).
    * runScript_"PP/TPU"_cube.py - MAIN file to run and generate "dat" files and folders with the calibration results.
      - Creates "dat" binary files for deformation (F)/ material parameters (M)/ stress (P)/ temperature (T)/ equivalent plastic strain (epl)/ loadcase (L)
      - Also, Creates folders for each case to postproc in gmsh if required.)
    * makePlots.py - Run to generate the calibration exp. vs num. plots and their pdfs based on existing results.
    * The other files if any include data for cyclic loading.


2. Experiments 
  - The requisite experimental data for calibration and validation on PP BJ380MO by Leartiker and TPU EOS 1301 by IMDEA/JKU
  - Several subfolders - split first on material type and then the last folder contains a readMe.txt to explain the experiment files.
  - THIS FOLDER HAS NO PLOTS, only data. USE ....Experiments.... in the other folder to generate these plots.

3. Validation  - All validation tests for TPU split by test type and simulation numbers as follows in respective folders:
  - Compression - Uses TPU tests - A_viii, A_ix, A_x, A_xi
  - Tension - Uses TPU tests - A_iii, A_iv, A_v, A_vi
  - Torsion - F22, F23, F24 with axial displacement constraint, F25, F26, F27 without axial displacement constraint
  - TorsionRotationCorrection - F26 (w/o correction), F43 (with correction)
  - Images_Paper1 - Has the generated figures
    - Run to generate the validation exp. vs num. plots and their pdfs based on existing results:
    ``` python3 makePlots.py ```