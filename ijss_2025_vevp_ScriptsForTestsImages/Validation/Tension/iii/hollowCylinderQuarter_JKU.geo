// Parameters
mm = 1.;
Rint = 4*mm;
Rext = 15*mm; // Gauge external
R_outer = 25*mm; // Radius outer
R_notch = 5*mm; // Notch radius
H = 20*mm;    // Gauge length

lsca = 0.2*Rext;
lsca_int = 0.25*Rint;
lsca_outer = 0.6*Rint; // 0.2*R_outer;
lsca_notch = 0.6*Rint; // 0.2*R_notch;

//+
Point(1) = {4, 0, 0, lsca_int};
//+
Point(2) = {7.5, 0, 0, lsca_int};
//+
Point(3) = {7.5, 10, 0, lsca_int};
//+
Point(4) = {12.5, 15, 0, lsca_outer};
//+
Point(5) = {12.5, 25, 0, lsca_outer};
//+
Point(6) = {4, 25, 0, lsca_int};
//+
Point(7) = {12.5, 10, 0, lsca_notch};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Circle(3) = {3, 7, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};
//+
Curve Loop(1) = {6, 1, 2, 3, 4, 5};
//+
Plane Surface(1) = {1};
//+
Recombine Surface{1};
//+
Extrude {{0, 1, 0}, {0, 0, 0}, Pi/2} {
  Surface{1}; Layers{5}; Recombine;
}

// Physicals
Physical Volume(11) = {1};
Physical Surface(1) = {1}; // front -> block in z
Physical Surface(2) = {21}; // bottom -> block in y
Physical Surface(3) = {37}; // top -> apply disp
Physical Surface(4) = {38}; // left -> block in x
Physical Surface(5) = {33}; // outer curved surface -> apply disp aliter
Physical Point(1) = {1};
Physical Point(2) = {2};
Physical Point(3) = {3};
Physical Point(4) = {4};
Physical Point(5) = {5};
Physical Point(6) = {6};
Physical Point(7) = {7};
