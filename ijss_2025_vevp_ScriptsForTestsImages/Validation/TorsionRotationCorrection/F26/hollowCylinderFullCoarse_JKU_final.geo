//+
SetFactory("OpenCASCADE");

Cylinder(1) = {0, 0, 0, 0, 15., 0, 12.5, 2*Pi};
Cylinder(2) = {0, 0, 0, 0, 15., 0, 4., 2*Pi};
//+
BooleanDifference{ Volume{1}; Delete;}{ Volume{2}; Delete;}

//+
// Physical Volume(10) = {1};
//+
Cylinder(3) = {0, 15, 0, 0, 20., 0, 7.5, 2*Pi};
Cylinder(4) = {0, 15, 0, 0, 20., 0, 4., 2*Pi};
//+
BooleanDifference{ Volume{3}; Delete;}{ Volume{4}; Delete;}
//+
BooleanUnion{ Volume{1}; Delete;}{ Volume{3}; Delete;}
//+
Cylinder(5) = {0, 35, 0, 0, 15., 0, 12.5, 2*Pi};
Cylinder(6) = {0, 35, 0, 0, 15., 0, 4., 2*Pi};
//+
BooleanDifference{ Volume{5}; Delete;}{ Volume{6}; Delete;}
//+
BooleanUnion{ Volume{1}; Delete;}{ Volume{5}; Delete;}

//+
// Fillet {1}{6}{4.5}
//+
Torus(2) = {0, 15, 0, 12.5, 5., 2*Pi};
//+
Rotate {{1, 0, 0}, {0, 15, 0}, Pi/2} {
  Volume{2};
}
Rotate {{0, 1, 0}, {0, 15, 0}, 3*Pi/2} {
  Volume{2};
}
//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Delete; }
//+
Torus(2) = {0, 35, 0, 12.5, 5, 2*Pi};
//+
Rotate {{1, 0, 0}, {0, 35, 0}, Pi/2} {
  Volume{2};
}
Rotate {{0, 1, 0}, {0, 35, 0}, 3*Pi/2} {
  Volume{2};
}
//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Delete; }
//+
// Physicals
Physical Volume(11) = {1};
Physical Surface(1) = {4}; // front -> cylindrical gauge
Physical Surface(2) = {3}; // bottom -> block in y
Physical Surface(3) = {7}; // top -> apply disp
Physical Surface(4) = {1}; // bottom cylinder ->
Physical Surface(5) = {8}; // top cylinder ->
Physical Surface(6) = {5}; // inner cylinder ->
Physical Point(1) = {1};
Physical Point(2) = {2};
Physical Point(3) = {3};
Physical Point(4) = {4};
Physical Point(5) = {5};
Physical Point(6) = {6};
Physical Point(7) = {7};
Physical Point(8) = {8};

