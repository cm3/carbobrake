#coding-Utf-8-*-
from gmshpy import *
# from dG3DpyDebug import*
from dG3Dpy import*
from math import*
import csv, os
import numpy as np

# Script to launch dogbone problem with a python script
lawnum = 1 # unique number of law

# Load the csv data for relaxation spectrum
with open('TPU_relaxationSpectrum_Et_N27_31_07_24_Trefm30.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
      if i == 0:
          relSpec[i] = float(' '.join(row))
      else:
          relSpec = np.append(relSpec, float(' '.join(row)))
      i += 1
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6    # convert to MPa

E = relSpec[0] #MPa # 2700
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 1110e-12 # Bulk mass  kg/m3 -> tonne/mm3
Cp = rho*1850e+6 # J/kg/K -> Nmm/tonne/K
KThCon = 0.2332  # W/m/K -> Nmm/s/mm/K
Alpha = 1.5e-4 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 524.39721767
C2 = 699.76815543

# Temp Settings
Tref = 273.15-30.

# isoHard and tempfuncs
hardenc = LinearExponentialJ2IsotropicHardening(1, 4.5, 5.0, 5., 5.)
hardent = LinearExponentialJ2IsotropicHardening(2, 4.5, 5.0, 7.5, 12.5)

hardenk = PolynomialKinematicHardening(3,1)
hardenk.setCoefficients(0,20.)
hardenk.setCoefficients(1,25.)


law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-8,1e-6)
law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setYieldPowerFactor(3.5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.64)

law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

law1.useRotationCorrectionBool(False,2) # bool, int_Scheme = 0 -> R0, 1 -> R1

# extra + TVE
law1.setExtraBranchNLType(5)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,5)
law1.setVolumeCorrection(0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
law1.setAdditionalVolumeCorrections(8.5, 0.75, 0.028, 8.5, .075, 0.028)
law1.setExtraBranch_CompressionParameter(1.,0.,0.)
law1.setTensionCompressionRegularisation(100.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, 0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
        law1.setCompressionCorrectionsAllBranchesTVE(i, 1.0)
        law1.setAdditionalCorrectionsAllBranchesTVE(i, 8.5, 0.75, 0.028, 8.5, .075, 0.028)

# Mullins Effect
mullins = linearScaler(4, 0.99)
law1.setMullinsEffect(mullins)

# Viscosity - TVP
eta = constantViscosityLaw(1,1000.)
law1.setViscosityEffect(eta,0.21)

# geometry
geofile= "hollowCylinderFullCoarse_JKU_final.geo"
meshfile= "hollowCylinderFullCoarse_JKU_final.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime = 40.   # Final time (used only if soltype=1)
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
averageStrainBased = False

# creation of ElasticField
nfield = 11
pertFactor = 1e-8
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,pertFactor)
ThermoMechanicsEqRatio = 1.e1 # setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio)
thermalSource = True
mecaSource = True
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
# myfield1.strainSubstep(2, 10)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
# mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

mysolver.snlManageTimeStep(25,5,2.,50)
# maxIterations # timeStepLimit below which increase time-step if reduced before for iterations more than maximal # factor of reduction/increase # no.of fails allowed
optimalNumber = 5
maximalStep = 3.e-1
minimalStep = 1e-3
power = 1
maximalFailStep = 10
# mysolver.setTimeIncrementAdaptation(True,optimalNumber,power,maximalStep,minimalStep,maximalFailStep) # automatic time stepping

# Dimension
H = 20. # Gauge length

# BC
# functions to prescribe displacements (x,z) -> rotation about y
def disp_x_rotY(x,y,z,t,extra):
    # print("%f,%f,%f,%f",x,y,z,t)
    return x*(np.cos(extra*t)-1) + z*(np.sin(extra*t))

def disp_z_rotY(x,y,z,t,extra):
    return x*(-np.sin(extra*t)) + z*(np.cos(extra*t)-1)

# thetaMax = 2*np.pi
omega = 10.*np.pi/180
fX_rotY = PythonBCfunctionDouble(disp_x_rotY,omega) # fTheta[:,1])
fZ_rotY = PythonBCfunctionDouble(disp_z_rotY,omega) # fTheta[:,1])

mysolver.displacementBC("Face",2,0,0.)
mysolver.displacementBC("Face",2,2,0.)
mysolver.displacementBC("Face",2,1,0.) # bottom -> blocked in y
# mysolver.displacementBC("Face",3,1,0.) # top -> blocked in y
mysolver.displacementBC("Face",5,0,fX_rotY) # upper cylinder -> disp in x, surface 5
mysolver.displacementBC("Face",5,2,fZ_rotY) # upper cylinder -> disp in z, surface 5

# Added
mysolver.initialBC("Volume","Position",nfield,3,Tinitial)
fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial);    # Linear Displacement with time
# mysolver.displacementBC("Volume",nfield,3,fT)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, 1)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,nstepArch)


mysolver.archivingInternalForceOnPhysicalGroup("Face",5,1)
# mysolver.archivingInternalForceOnPhysicalGroup("Node",6,1)
# mysolver.archivingInternalForceOnPhysicalGroup("Node",8,1)
mysolver.archivingForceOnPhysicalGroup("Face",3,1)
mysolver.archivingNodeDisplacement(6,0,1)
mysolver.archivingNodeDisplacement(6,1,1)
mysolver.archivingNodeDisplacement(6,2,1)
mysolver.archivingNodeDisplacement(8,0,1)
mysolver.archivingNodeDisplacement(8,1,1)
mysolver.archivingNodeDisplacement(8,2,1)

# Solve
mysolver.solve()
