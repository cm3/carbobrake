import os, re
from os import listdir, makedirs
from os.path import isfile, join, exists, isdir
import numpy as np
import pandas as pd
import scipy as sp
from scipy.optimize import least_squares
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from math import exp, cos, sin
import matplotlib.pyplot as plt
from functools import reduce
from scipy.signal import butter, lfilter, freqz, savgol_filter
from scipy import interpolate
from matplotlib.animation import FuncAnimation
import matplotlib.cm as cm
import matplotlib_inline
# matplotlib_inline.backend_inline.set_matplotlib_formats('pdf')

#################################################################################################

#####
#### TENSION -- Validation - Quarter Dumbell Zoltan's paper - Tension only
#####

# A/iii) hollow dumbell JKU (quarter) - tension with Mechsrc and different strain rate
prefix2 = "Tension"
disp_A_iii = pd.read_csv(prefix2 + "/iii/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_iii *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_iii = pd.read_csv(prefix2 + "/iii/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_iii *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/iv) hollow dumbell JKU (quarter) - tension with Mechsrc and different strain rate
disp_A_iv = pd.read_csv(prefix2 + "/iv/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_iv *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_iv = pd.read_csv(prefix2 + "/iv/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_iv *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/v) hollow dumbell JKU (quarter) - tension with Mechsrc and different strain rate
disp_A_v = pd.read_csv(prefix2 + "/v/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_v *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_v = pd.read_csv(prefix2 + "/v/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_v *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/vi) hollow dumbell JKU (quarter) - tension with Mechsrc and different strain rate
disp_A_vi = pd.read_csv(prefix2 + "/vi/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_vi *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_vi = pd.read_csv(prefix2 + "/vi/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_vi *= 1/(np.pi*(7.5**2-4**2)*0.25)

## Experiments
prefix_zoltan = "../Experiments/TPU/Dumbbell Tension/"
UTT_10_m1 = pd.read_csv(prefix_zoltan+'0_1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_0 = pd.read_csv(prefix_zoltan+'1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_1 = pd.read_csv(prefix_zoltan+'10mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_2 = pd.read_csv(prefix_zoltan+'100mm_s.csv',sep =",",header=None).to_numpy()

plt.figure(num=10)
plt.plot(UTT_10_m1[:,0], UTT_10_m1[:,1],label="Exp. 0.1mm/s", lw = 1.,ls ="none",color="b",
        marker='s',markeredgecolor="b",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_0[:,0], UTT_10_0[:,1],label="Exp. 1mm/s", lw = 1.,ls ="none",color="g",
        marker='o',markeredgecolor="g",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_1[:,0], UTT_10_1[:,1],label="Exp. 10mm/s", lw = 1.,ls ="none",color="r",
        marker='*',markeredgecolor="r",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_2[:,0], UTT_10_2[:,1],label="Exp. 100mm/s", lw = 1.,ls ="none",color="m",
        marker='+',markeredgecolor="m",mew=2, ms=8,markerfacecolor='none')

plt.plot(disp_A_iii[:,1], -force_A_iii[:,1], alpha = 0.8, color="b",lw=2.0,label='Num. 0.1mm/s')
plt.plot(disp_A_iv[:,1], -force_A_iv[:,1], alpha = 0.8, color="g",lw=2.0,label='Num. 1mm/s')
plt.plot(disp_A_v[:,1], -force_A_v[:,1], alpha = 0.8, color="r",lw=2.0,label='Num. 10mm/s')
plt.plot(disp_A_vi[:,1], -force_A_vi[:,1], alpha = 0.8, color="m",lw=2.0,label='Num. 100mm/s')

# plt.title("Hollow Dumbell: Uniaxial Tension @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Displacement [mm]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'lower right') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Tension_HollowDumbbellZoltan_Numerical.pdf", bbox_inches="tight")
plt.show()

#################################################################################################

#####
#### Compression -- Validation - Quarter Dumbell Zoltan's experiments - Compression only
#####

prefix_zoltan2 = "../Experiments/TPU/Dumbbell Compression"

area_top = (np.pi*(7.5**2-4**2))

UCT_X_10_0 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1mms_1/TPUalt_X_1mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_1 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_10mms_1/TPUalt_X_10mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_2 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_100mms_1/TPUalt_X_100mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_3 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1000mms_1/TPUalt_X_1000mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()

prefix2 = "Compression"
# A/viii) hollow dumbell JKU (quarter) - compression with Mechsrc and different strain rate
disp_A_viii = pd.read_csv(prefix2 + "/viii/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_viii *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_viii = pd.read_csv(prefix2 + "/viii/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_viii *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/ix) hollow dumbell JKU (quarter) - compression with Mechsrc and different strain rate
disp_A_ix = pd.read_csv(prefix2 + "/ix/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_ix *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_ix = pd.read_csv(prefix2 + "/ix/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_ix *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/x) hollow dumbell JKU (quarter) - compression with Mechsrc and different strain rate
disp_A_x = pd.read_csv(prefix2 + "/x/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_x *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_x = pd.read_csv(prefix2 + "/x/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_x *= 1/(np.pi*(7.5**2-4**2)*0.25)

# A/xi) hollow dumbell JKU (quarter) - compression with Mechsrc and different strain rate
disp_A_xi = pd.read_csv(prefix2 + "/xi/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        sep =";",header=None).to_numpy()
disp_A_xi *= 15/10*2 # 1./10 # 10 is half the gauge length
force_A_xi = pd.read_csv(prefix2 + "/xi/force2comp1_part1.csv",\
                        sep =";",header=None).to_numpy()
force_A_xi *= 1/(np.pi*(7.5**2-4**2)*0.25)

# d) hollow dumbell JKU (quarter) - compression with Mechsrc and different strain rate
plt.figure(num=11)
plt.plot(-UCT_X_10_0[:,1], -UCT_X_10_0[:,2]/area_top,label="Exp. 1mm/s", lw = 1.,ls ="none",color="b",
        marker='s',markeredgecolor="b",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_1[:,1], -UCT_X_10_1[:,2]/area_top,label="Exp. 10mm/s", lw = 1.,ls ="none",color="g",
        marker='o',markeredgecolor="g",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_2[:,1], -UCT_X_10_2[:,2]/area_top,label="Exp. 100mm/s", lw = 1.,ls ="none",color="r",
        marker='*',markeredgecolor="r",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_3[:,1], -UCT_X_10_3[:,2]/area_top,label="Exp. 1000mm/s", lw = 1.,ls ="none",color="m",
        marker='+',markeredgecolor="m",mew=3, ms=10,markerfacecolor='none') #,markevery=0.001)

plt.plot(-disp_A_viii[:,1], force_A_viii[:,1], alpha = 0.8, color="b",lw=2.0,label='Num. 1mm/s')
plt.plot(-disp_A_ix[:,1], force_A_ix[:,1], alpha = 0.8, color="g",lw=2.0,label='Num. 10mm/s')
plt.plot(-disp_A_x[:,1], force_A_x[:,1], alpha = 0.8, color="r",lw=2.0,label='Num. 100mm/s')
plt.plot(-disp_A_xi[:,1], force_A_xi[:,1], alpha = 0.8, color="m",lw=2.0,label='Num. 1000mm/s')

# plt.title("Hollow Dumbell: Uniaxial Compression @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Displacement [mm]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.5})
plt.xlim([-0.001, 22])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
legend = plt.legend(fontsize = 12, loc = 'upper right') #,bbox_to_anchor=(1.015, 1.0))
legend.get_frame().set_alpha(0.75)
plt.savefig("Images_Paper1/TPU_Compression_HollowDumbbellZoltan_Numerical.pdf", bbox_inches="tight")
plt.show()

#################################################################################################

#####
#### TORSION -- Validation - Quarter Dumbell Zoltan's experiments - Compression only
#####

### Load Experimental Results
prefix_Exp_Torsion = "../Experiments/TPU/Dumbbell Torsion/"
Import2 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_01deg_s_F0_Kanäle _Messwerte__1.csv',skiprows=[1],delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import3 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_1deg_s_F0_Kanäle _Messwerte__2.csv',skiprows=[1],delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import4 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_10deg_s_F0_3_Kanäle _Messwerte__1.csv',skiprows=[1],delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import5 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_01deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=110,delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import6 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_1deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=[1],delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import7 = pd.read_table(prefix_Exp_Torsion+'TPUneu_X_10deg_s_weg0_3_Kanäle _Messwerte__1.csv',skiprows=[1],delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()

## Functions to calculate numerical moment
def moment_Y(x,z,Fx,Fz):
    return -x*Fz + z*Fx

def getMoment(verNum, forces, omega):
    time = forces["Time"].to_numpy() # Disps.shape[0] is the length
    MY_total = np.zeros(np.shape(time)[0])
    MY = np.zeros((np.shape(time)[0],np.shape(verNum)[0])) # int(Disps.shape[1]/3) is the same length

    for i in range(np.shape(verNum)[0]): # loop on n_nodes
        x = np.ones(np.shape(time)[0])
        z = np.ones(np.shape(time)[0])

        stringForceX = "F_"+str(verNum["node"][i])+"_X"
        stringForceZ = "F_"+str(verNum["node"][i])+"_Z"
        Fx = forces[forces.columns[forces.columns.to_series().str.contains(stringForceX)]].to_numpy()
        Fz = forces[forces.columns[forces.columns.to_series().str.contains(stringForceZ)]].to_numpy()
        if (Fx.size == 0):
            Fx = np.zeros(np.shape(time)[0])
            # print("Here")
        if (Fz.size == 0):
            Fz = np.zeros(np.shape(time)[0])
        for j in range(np.shape(time)[0]): # loop on n_steps
            x[j] = verNum["x"][i]*np.cos(omega*time[j]) + verNum["z"][i]*np.sin(omega*time[j])
            z[j] = verNum["x"][i]*(-np.sin(omega*time[j])) + verNum["z"][i]*(np.cos(omega*time[j]))
            MY[j,i] = moment_Y(x[j],z[j],Fx[j],Fz[j])
            MY_total[j] += MY[j,i]
    return MY_total



prefix_force = "Torsion/"
## Load Numerical Results
verNum100 = pd.read_csv(prefix_force+"F22/numver5.txt",delimiter=";",\
                      names=["node","x","y","z"])

forces_F_22_0 = pd.read_csv(prefix_force+"F22/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_22_1 = pd.read_csv(prefix_force+"F22/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_22_2 = pd.read_csv(prefix_force+"F22/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_22_3 = pd.read_csv(prefix_force+"F22/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_22_2.columns = forces_F_22_2.columns.astype(str)
forces_F_22_3.columns = forces_F_22_3.columns.astype(str)

forces_F_22_01 = forces_F_22_0.add(forces_F_22_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a22is=1)
forces_F_22_23 = forces_F_22_2.add(forces_F_22_3, fill_value=0)
forces_F_22 = forces_F_22_01.add(forces_F_22_23, fill_value=0)
forces_F_22['Time'] = forces_F_22['Time'].div(4.)


forces_F_23_0 = pd.read_csv(prefix_force+"F23/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_23_1 = pd.read_csv(prefix_force+"F23/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_23_2 = pd.read_csv(prefix_force+"F23/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_23_3 = pd.read_csv(prefix_force+"F23/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_23_2.columns = forces_F_23_2.columns.astype(str)
forces_F_23_3.columns = forces_F_23_3.columns.astype(str)

forces_F_23_01 = forces_F_23_0.add(forces_F_23_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a23is=1)
forces_F_23_23 = forces_F_23_2.add(forces_F_23_3, fill_value=0)
forces_F_23 = forces_F_23_01.add(forces_F_23_23, fill_value=0)
forces_F_23['Time'] = forces_F_23['Time'].div(4.)


forces_F_24_0 = pd.read_csv(prefix_force+"F24/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_24_1 = pd.read_csv(prefix_force+"F24/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_24_2 = pd.read_csv(prefix_force+"F24/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_24_3 = pd.read_csv(prefix_force+"F24/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_24_2.columns = forces_F_24_2.columns.astype(str)
forces_F_24_3.columns = forces_F_24_3.columns.astype(str)

forces_F_24_01 = forces_F_24_0.add(forces_F_24_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a24is=1)
forces_F_24_23 = forces_F_24_2.add(forces_F_24_3, fill_value=0)
forces_F_24 = forces_F_24_01.add(forces_F_24_23, fill_value=0)
forces_F_24['Time'] = forces_F_24['Time'].div(4.)


forces_F_25_0 = pd.read_csv(prefix_force+"F25/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_25_1 = pd.read_csv(prefix_force+"F25/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_25_2 = pd.read_csv(prefix_force+"F25/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_25_3 = pd.read_csv(prefix_force+"F25/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_25_2.columns = forces_F_25_2.columns.astype(str)
forces_F_25_3.columns = forces_F_25_3.columns.astype(str)

forces_F_25_01 = forces_F_25_0.add(forces_F_25_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a25is=1)
forces_F_25_23 = forces_F_25_2.add(forces_F_25_3, fill_value=0)
forces_F_25 = forces_F_25_01.add(forces_F_25_23, fill_value=0)
forces_F_25['Time'] = forces_F_25['Time'].div(4.)


forces_F_26_0 = pd.read_csv(prefix_force+"F26/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_26_1 = pd.read_csv(prefix_force+"F26/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_26_2 = pd.read_csv(prefix_force+"F26/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_26_3 = pd.read_csv(prefix_force+"F26/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_26_2.columns = forces_F_26_2.columns.astype(str)
forces_F_26_3.columns = forces_F_26_3.columns.astype(str)

forces_F_26_01 = forces_F_26_0.add(forces_F_26_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a26is=1)
forces_F_26_23 = forces_F_26_2.add(forces_F_26_3, fill_value=0)
forces_F_26 = forces_F_26_01.add(forces_F_26_23, fill_value=0)
forces_F_26['Time'] = forces_F_26['Time'].div(4.)



forces_F_27_0 = pd.read_csv(prefix_force+"F27/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_27_1 = pd.read_csv(prefix_force+"F27/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_27_2 = pd.read_csv(prefix_force+"F27/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_27_3 = pd.read_csv(prefix_force+"F27/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()

forces_F_27_2.columns = forces_F_27_2.columns.astype(str)
forces_F_27_3.columns = forces_F_27_3.columns.astype(str)

forces_F_27_01 = forces_F_27_0.add(forces_F_27_1, fill_value=0) # pd.concat([forces8_2, forces8_3], a27is=1)
forces_F_27_23 = forces_F_27_2.add(forces_F_27_3, fill_value=0)
forces_F_27 = forces_F_27_01.add(forces_F_27_23, fill_value=0)
forces_F_27['Time'] = forces_F_27['Time'].div(4.)


prefix_force2 = "TorsionRotationCorrection/"

# F26 but with rotation correction
verNum_F_43 = pd.read_csv(prefix_force2+"F43/numver5.txt",delimiter=";",\
                      names=["node","x","y","z"])
forces_F_43_0 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part0.csv",\
                        delimiter=";",header=0)
forces_F_43_1 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part1.csv",\
                        delimiter=";",header=0)
forces_F_43_2 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part2.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_3 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part3.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_4 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part4.csv",\
                        delimiter=";",header=0)
forces_F_43_5 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part5.csv",\
                        delimiter=";",header=0)
forces_F_43_6 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part6.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_7 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part7.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_8 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part8.csv",\
                        delimiter=";",header=0)
forces_F_43_9 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part9.csv",\
                        delimiter=";",header=0)
forces_F_43_10 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part10.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_11 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part11.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_12 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part12.csv",\
                        delimiter=";",header=0)
forces_F_43_13 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part13.csv",\
                        delimiter=";",header=0)
forces_F_43_14 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part14.csv",\
                        delimiter=";",header=0).dropna()
forces_F_43_15 = pd.read_csv(prefix_force2+"F43/internalForceOnPhysicalFace5_part15.csv",\
                        delimiter=";",header=0).dropna()

forces_F_43_2.columns = forces_F_43_2.columns.astype(str)
forces_F_43_3.columns = forces_F_43_3.columns.astype(str)
forces_F_43_6.columns = forces_F_43_6.columns.astype(str)
forces_F_43_7.columns = forces_F_43_7.columns.astype(str)

forces_F_43_01 = forces_F_43_0.add(forces_F_43_1, fill_value=0)
forces_F_43_23 = forces_F_43_2.add(forces_F_43_3, fill_value=0)
forces_F_43_45 = forces_F_43_4.add(forces_F_43_5, fill_value=0)
forces_F_43_67 = forces_F_43_6.add(forces_F_43_7, fill_value=0)
forces_F_43_89 = forces_F_43_8.add(forces_F_43_9, fill_value=0)
forces_F_43_1011 = forces_F_43_10.add(forces_F_43_11, fill_value=0)
forces_F_43_1213 = forces_F_43_12.add(forces_F_43_13, fill_value=0)
forces_F_43_1415 = forces_F_43_14.add(forces_F_43_15, fill_value=0)

forces_F_43_0123 = forces_F_43_01.add(forces_F_43_23, fill_value=0)
forces_F_43_4567 = forces_F_43_45.add(forces_F_43_67, fill_value=0)
forces_F_43_891011 = forces_F_43_89.add(forces_F_43_1011, fill_value=0)
forces_F_43_12131415 = forces_F_43_1213.add(forces_F_43_1415, fill_value=0)

forces_F_43_01234567 = forces_F_43_0123.add(forces_F_43_4567, fill_value=0)
forces_F_43_89101112131415 = forces_F_43_891011.add(forces_F_43_12131415, fill_value=0)

forces_F_43 = forces_F_43_01234567.add(forces_F_43_89101112131415, fill_value=0)
forces_F_43['Time'] = forces_F_43['Time'].div(16.)

## Get Numerical Moments
MY_total_F_22 = getMoment(verNum100,forces_F_22,0.1*np.pi/180)
MY_total_F_23 = getMoment(verNum100,forces_F_23,1*np.pi/180)
MY_total_F_24 = getMoment(verNum100,forces_F_24,10.*np.pi/180)
MY_total_F_25 = getMoment(verNum100,forces_F_25,0.1*np.pi/180)
MY_total_F_26 = getMoment(verNum100,forces_F_26,1.*np.pi/180)
MY_total_F_27 = getMoment(verNum100,forces_F_27,10.*np.pi/180)
MY_total_F_43 = getMoment(verNum_F_43,forces_F_43,1.*np.pi/180)
# print("numericalTorque_F22=", MY_total_F_22[-1]*1e-3)


# Moment - No axial constraint

fig, ax = plt.subplots()
ax.plot(Import2[:,0][0:-1:500],(Import2[:,1][0:-1:500]),label="Exp.F0 0.1°/s",lw =2.,ls='dotted',color="r")
ax.plot(Import3[:,0][0:-1:500],Import3[:,1][0:-1:500],label="Exp.F0 1°/s ", lw =2,ls='dashed',color="g")
ax.plot(Import4[:,7][0:-1:500],Import4[:,0][0:-1:500],label="Exp.F0 10°/s ",lw =2,ls='dashdot',color="b")

ax.plot(0.1*forces_F_25["Time"].to_numpy(),MY_total_F_25*1e-3, color="r",lw=1.5,
        label='Num. 0.1°/s',marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*forces_F_26["Time"].to_numpy(),MY_total_F_26*1e-3, color="g",lw=1.5,
        label='Num. 1°/s',marker='o',markeredgecolor="g",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(10*forces_F_27["Time"].to_numpy()[0:800],MY_total_F_27[0:800]*1e-3, color="b",lw=1.5,
        label='Num. 10°/s',marker='x',markeredgecolor="b",mew=2, ms=6,markerfacecolor='none',markevery=0.05)

ax.set_xlabel("Theta [°]",fontdict={'fontsize':12.5})
ax.set_ylabel("Torque [Nm]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# ax.set_title("TPU Dumbell Torsion - Fax=0", fontdict={'fontsize':11.5, 'weight': 'bold'})
# ax.set_xlim([-0.001, 500])
# ax.ylim([-1., 10.])
ax.grid('on')
ax.legend(loc='lower right', fontsize=12) # bbox_to_anchor=(1.1, 1.05)) #)
plt.savefig("Images_Paper1/torsion_Fax0_Numerical.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()


# Moment - Axial constraint

fig, ax = plt.subplots()
ax.plot(Import5[:,0][0:-1:500],Import5[:,2][0:-1:500],label="Exp.S0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
ax.plot(Import6[:,7][0:-1:500],Import6[:,0][0:-1:500],label="Exp.S0 1°/s ", lw =2.,
        ls='dashed',color="g")
ax.plot(Import7[:,7][0:-1:500],Import7[:,0][0:-1:500],label="Exp.S0 10°/s ",lw =2.,
        ls='dashdot',color="b")

ax.plot(0.1*forces_F_22["Time"].to_numpy(),MY_total_F_22*1e-3, color="r",lw=1.5,
        label='Num. 0.1°/s',marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*forces_F_23["Time"].to_numpy(),MY_total_F_23*1e-3, color="g",lw=1.5,
        label='Num. 1°/s',marker='o',markeredgecolor="g",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(10*forces_F_24["Time"].to_numpy(),MY_total_F_24*1e-3, color="b",lw=1.5,
        label='Num. 10°/s',marker='x',markeredgecolor="b",mew=2, ms=6,markerfacecolor='none',markevery=0.05)

ax.set_xlabel("Theta [°]",fontdict={'fontsize':12.5})
ax.set_ylabel("Torque [Nm]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# ax.set_title("TPU Dumbell Torsion - Fax=0", fontdict={'fontsize':11.5, 'weight': 'bold'})
ax.set_xlim([-0.001, 470])
# ax.ylim([-1., 10.])
ax.grid('on')
ax.legend(loc='lower right', fontsize=12) # bbox_to_anchor=(1.1, 1.05)) #)
plt.savefig("Images_Paper1/torsion_Sax0_Numerical.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()


# Upward axial displacement - no axial constraint to see Poynting effect
ydisp_F_25 = pd.read_csv(prefix_force+"F25/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()
ydisp_F_26 = pd.read_csv(prefix_force+"F26/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()
ydisp_F_27 = pd.read_csv(prefix_force+"F27/NodalDisplacementPhysical6Num6comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()

fig, ax = plt.subplots()
ax.plot(Import2[:,0][0:-1:500],(Import2[:,3][0:-1:500]),label="Exp.F0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
ax.plot(Import3[:,0][0:-1:500],Import3[:,4][0:-1:500],label="Exp.F0 1°/s ", lw =2,
        ls='dashed',color="g")
ax.plot(Import4[:,7][0:-1:500],Import4[:,6][0:-1:500],label="Exp.F0 10°/s ",lw =2,
        ls='dashdot',color="b")

ax.plot(0.1*forces_F_25["Time"].to_numpy(),ydisp_F_25[:,1], color="r",lw=1.5,
        label='Num. 0.1°/s',marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*forces_F_26["Time"].to_numpy(),ydisp_F_26[:,1], color="g",lw=1.5,
        label='Num. 1°/s',marker='o',markeredgecolor="g",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(10*forces_F_27["Time"].to_numpy()[0:4020],ydisp_F_27[:,1], color="b",lw=1.5,
        label='Num. 10°/s',marker='x',markeredgecolor="b",mew=2, ms=6,markerfacecolor='none',markevery=0.05)

ax.set_xlabel("Theta [°]",fontdict={'fontsize':12.5})
ax.set_ylabel("Displacement [mm]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# ax.set_title("TPU Dumbell Coarse Mesh", fontdict={'fontsize':11.5, 'weight': 'bold'})
ax.set_xlim([-0.001, 470])
# ax.ylim([-1., 10.])
ax.grid('on')
ax.legend(loc='upper left', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/verticalDisplacementFax0_Numerical.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()

# Axial reaction force - axial constraint
axialForce_F_22_0 = pd.read_csv(prefix_force+"F22/force3comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_22_1 = pd.read_csv(prefix_force+"F22/force3comp1_part3.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_22 = axialForce_F_22_0[0:843,1] + axialForce_F_22_1[0:843,1]

axialForce_F_23_0 = pd.read_csv(prefix_force+"F23/force3comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_23_1 = pd.read_csv(prefix_force+"F23/force3comp1_part3.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_23 = axialForce_F_23_0[0:827,1] + axialForce_F_23_1[0:827,1]

axialForce_F_24_0 = pd.read_csv(prefix_force+"F24/force3comp1_part2.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_24_1 = pd.read_csv(prefix_force+"F24/force3comp1_part3.csv",\
                        delimiter=";",header=None).to_numpy()
axialForce_F_24 = axialForce_F_24_0[0:788,1] + axialForce_F_24_1[0:788,1]

fig, ax = plt.subplots()
ax.plot(Import5[:,0][0:-1:500],-0.001*(Import5[:,4][0:-1:500]),label="Exp.S0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
ax.plot(Import6[:,7][0:-1:500],-0.001*Import6[:,2][0:-1:500],label="Exp.S0 1°/s ", lw =2.,
        ls='dashed',color="g")
ax.plot(Import7[:,7][0:-1:500],-0.001*Import7[:,2][0:-1:500],label="Exp.S0 10°/s ",lw =2.,
        ls='dashdot',color="b")

ax.plot(0.1*forces_F_22["Time"].to_numpy(), -0.001*axialForce_F_22, color="r",lw=1.5,
        label='Num. 0.1°/s',marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*forces_F_23["Time"].to_numpy(), -0.001*axialForce_F_23, color="g",lw=1.5,
        label='Num. 1°/s',marker='o',markeredgecolor="g",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(10*forces_F_24["Time"].to_numpy(), -0.001*axialForce_F_24, color="b",lw=1.5,
        label='Num. 10°/s',marker='x',markeredgecolor="b",mew=2, ms=6,markerfacecolor='none',markevery=0.05)

ax.set_xlabel("Theta [°]",fontdict={'fontsize':12.5})
ax.set_ylabel("Force [kN]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# ax.set_title("TPU Dumbell Coarse Mesh", fontdict={'fontsize':11.5, 'weight': 'bold'})
ax.set_xlim([-0.001, 470])
# ax.ylim([-1., 10.])
ax.grid('on')
ax.legend(loc='upper left', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/axialForceSax0_Numerical.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()


## energy considerations -> F26 vs. F43, without and with coaxial correction

# F26
energy_F_26_0 = pd.read_csv(prefix_force+"F26/energy_part0.csv",\
                        delimiter=";",header=0)
energy_F_26_1 = pd.read_csv(prefix_force+"F26/energy_part1.csv",\
                        delimiter=";",header=0)
energy_F_26_2 = pd.read_csv(prefix_force+"F26/energy_part2.csv",\
                        delimiter=";",header=0).dropna()
energy_F_26_3 = pd.read_csv(prefix_force+"F26/energy_part3.csv",\
                        delimiter=";",header=0).dropna()

energy_F_26_2.columns = energy_F_26_2.columns.astype(str)
energy_F_26_3.columns = energy_F_26_3.columns.astype(str)

energy_F_26_01 = energy_F_26_0.add(energy_F_26_1, fill_value=0) # pd.concat([energy8_2, energy8_3], a26is=1)
energy_F_26_23 = energy_F_26_2.add(energy_F_26_3, fill_value=0)
energy_F_26 = energy_F_26_01.add(energy_F_26_23, fill_value=0)
energy_F_26['Time'] = energy_F_26['Time'].div(4.)

# F43
energy_F_43_0 = pd.read_csv(prefix_force2+"F43/energy_part0.csv",\
                        delimiter=";",header=0)
energy_F_43_1 = pd.read_csv(prefix_force2+"F43/energy_part1.csv",\
                        delimiter=";",header=0)
energy_F_43_2 = pd.read_csv(prefix_force2+"F43/energy_part2.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_3 = pd.read_csv(prefix_force2+"F43/energy_part3.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_4 = pd.read_csv(prefix_force2+"F43/energy_part4.csv",\
                        delimiter=";",header=0)
energy_F_43_5 = pd.read_csv(prefix_force2+"F43/energy_part5.csv",\
                        delimiter=";",header=0)
energy_F_43_6 = pd.read_csv(prefix_force2+"F43/energy_part6.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_7 = pd.read_csv(prefix_force2+"F43/energy_part7.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_8 = pd.read_csv(prefix_force2+"F43/energy_part8.csv",\
                        delimiter=";",header=0)
energy_F_43_9 = pd.read_csv(prefix_force2+"F43/energy_part9.csv",\
                        delimiter=";",header=0)
energy_F_43_10 = pd.read_csv(prefix_force2+"F43/energy_part10.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_11 = pd.read_csv(prefix_force2+"F43/energy_part11.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_12 = pd.read_csv(prefix_force2+"F43/energy_part12.csv",\
                        delimiter=";",header=0)
energy_F_43_13 = pd.read_csv(prefix_force2+"F43/energy_part13.csv",\
                        delimiter=";",header=0)
energy_F_43_14 = pd.read_csv(prefix_force2+"F43/energy_part14.csv",\
                        delimiter=";",header=0).dropna()
energy_F_43_15 = pd.read_csv(prefix_force2+"F43/energy_part15.csv",\
                        delimiter=";",header=0).dropna()

energy_F_43_2.columns = energy_F_43_2.columns.astype(str)
energy_F_43_3.columns = energy_F_43_3.columns.astype(str)
energy_F_43_6.columns = energy_F_43_6.columns.astype(str)
energy_F_43_7.columns = energy_F_43_7.columns.astype(str)

energy_F_43_01 = energy_F_43_0.add(energy_F_43_1, fill_value=0) # pd.concat([energy8_2, energy8_3], a40is=1)
energy_F_43_23 = energy_F_43_2.add(energy_F_43_3, fill_value=0)
energy_F_43_45 = energy_F_43_4.add(energy_F_43_5, fill_value=0) # pd.concat([energy8_2, energy8_3], a40is=1)
energy_F_43_67 = energy_F_43_6.add(energy_F_43_7, fill_value=0)
energy_F_43_89 = energy_F_43_8.add(energy_F_43_9, fill_value=0) # pd.concat([energy8_2, energy8_3], a40is=1)
energy_F_43_1011 = energy_F_43_10.add(energy_F_43_11, fill_value=0)
energy_F_43_1213 = energy_F_43_12.add(energy_F_43_13, fill_value=0) # pd.concat([energy8_2, energy8_3], a40is=1)
energy_F_43_1415 = energy_F_43_14.add(energy_F_43_15, fill_value=0)

energy_F_43_0123 = energy_F_43_01.add(energy_F_43_23, fill_value=0)
energy_F_43_4567 = energy_F_43_45.add(energy_F_43_67, fill_value=0)
energy_F_43_891011 = energy_F_43_89.add(energy_F_43_1011, fill_value=0)
energy_F_43_12131415 = energy_F_43_1213.add(energy_F_43_1415, fill_value=0)

energy_F_43_01234567 = energy_F_43_0123.add(energy_F_43_4567, fill_value=0)
energy_F_43_89101112131415 = energy_F_43_891011.add(energy_F_43_12131415, fill_value=0)

energy_F_43 = energy_F_43_01234567.add(energy_F_43_89101112131415, fill_value=0)
energy_F_43['Time'] = energy_F_43['Time'].div(16.)

# Plot deformation energy
fig, ax = plt.subplots()

ax.plot(1*energy_F_26["Time"].to_numpy(), 0.001*energy_F_26["Deformation"], color="k",lw=3.5,
        label='Num.F0') #,marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*energy_F_43["Time"].to_numpy(), 0.001*energy_F_43["Deformation"], color="r",lw=2,
        label='Num.F0 with coaxial correction',marker='o',markeredgecolor="r",mew=2, ms=6,\
        markerfacecolor='none',markevery=0.08)

ax.plot(1*energy_F_26["Time"].to_numpy(), 0.001*energy_F_26["Plastic"], color="k",lw=3.5,
        label='_no_label') #,marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*energy_F_43["Time"].to_numpy(), 0.001*energy_F_43["Plastic"], color="r",lw=2,
        label='_no_label',marker='o',markeredgecolor="r",mew=2, ms=6,\
        markerfacecolor='none',markevery=0.08)

"""
ax.plot(1*energy_F_26["Time"].to_numpy(), 0.001*(energy_F_26["Wext"]-energy_F_26["Plastic"]-energy_F_26["Deformation"]), color="k",lw=3.5,
        label='_no_label') #,marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*energy_F_43["Time"].to_numpy(), 0.001*(energy_F_43["Wext"]-energy_F_43["Plastic"]-energy_F_43["Deformation"]), color="r",lw=2,
        label='_no_label',marker='o',markeredgecolor="r",mew=2, ms=6,\
        markerfacecolor='none',markevery=0.08)
"""

textstr1 = ''.join(('Viscoelastic Deformation Energy'))
props1 = dict(boxstyle='round', facecolor='ivory', alpha=1)
ax.text(0.281, 0.32, textstr1, transform=ax.transAxes, fontsize=10,verticalalignment='top', bbox=props1, \
        rotation=36, rotation_mode='anchor')

textstr2 = ''.join(('Viscoplastic Dissipated Energy'))
props2 = dict(boxstyle='round', facecolor='azure', alpha=1)
ax.text(0.4, 0.15, textstr2, transform=ax.transAxes, fontsize=10,verticalalignment='top', bbox=props2, \
        rotation=12, rotation_mode='anchor')

ax.set_xlabel("Theta [°]",fontdict={'fontsize':13})
ax.set_ylabel("Specific Energy [J/$m^3$]",fontdict={'fontsize':13})
plt.xticks(fontsize=12.5)
plt.yticks(fontsize=12.5)
ax.grid('on')
ax.legend(loc='upper left', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/energyComparison.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()


# Plot plastic energy
fig, ax = plt.subplots()
ax.plot(Import3[:,0],Import3[:,1],label="Exp.F0", lw =1.,alpha=1.0,color="k")
ax.plot(1*forces_F_26["Time"].to_numpy(), 0.001*MY_total_F_26, color="b",lw=4.5,
        label='Num.F0') #,marker='s',markeredgecolor="r",mew=2, ms=6,markerfacecolor='none',markevery=0.05)
ax.plot(1*forces_F_43["Time"].to_numpy(), 0.001*MY_total_F_43, color="r",lw=2.5,
        label='Num.F0 with coaxial correction') #,marker='o',markeredgecolor="g",mew=2, ms=6,markerfacecolor='none',markevery=0.05)

ax.set_xlabel("Theta [°]",fontdict={'fontsize':13})
ax.set_ylabel("Torque [Nm]",fontdict={'fontsize':13})
plt.xticks(fontsize=12.5)
plt.yticks(fontsize=12.5)
ax.grid('on')
ax.legend(loc='lower right', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/torque.pdf", bbox_inches="tight", pad_inches=0.3, dpi = 300)
plt.show()
