import csv, os, random, pickle
import numpy as np
import scipy as sp
from scipy import interpolate
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from matplotlib_inline.backend_inline import set_matplotlib_formats
set_matplotlib_formats('pdf')

## Get All data
prefix = "../../Experiments/TPU/Bulk Characterisation/"

## Compression - @ Room Temperature
prefix1 = "Compression_bulk_RoomT_TPU/Compression_bulk_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
UCT_2AH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2AH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2AV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2AV_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2BH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2BH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2BV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2BV_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2CH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2CH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2CV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2CV_2p78E-3_RT_PA12.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3AH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3AH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3AV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3AV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3BH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3BH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3BV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3BV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3CH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3CH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3CV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3CV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4AH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4AH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4AV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4AV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4BH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4BH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4BV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4BV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4CH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4CH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4CV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4CV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()

## Tension - @ Room Temperature
prefix2 = "Tensile_bulk_RoomT_TPU/Tensile_bulk_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
UTT_1H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_1H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_1V_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_1V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_2H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_2H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_2V_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_2V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_3H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_3H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_3V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_3V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_4H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_4H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_4V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_4V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_5H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_5H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_5V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_5V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_6H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_6H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_6V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_6V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_7H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_7H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_7V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_7V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_8H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_8H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_8V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_8V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_9H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_9H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()


## Compression cyclic - @ Room Temperature
prefix3 = "Cyclic_Compressive_RoomT_TPU/Cyclic_Compressive_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
MCC_1H_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_1H_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_1V_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_1V_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,11)).to_numpy()
MCC_2H_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_2H_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_2V_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_2V_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_3V_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_3V_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_4H_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_4H_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_4V_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_4V_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_5H_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_5H_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_5V_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_5V_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_6H_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_6H_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_6V_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_6V_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_7H_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_7H_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()

## Tension cyclic - @ Room Temperature
prefix4 = "Cyclic_Tensile_RoomT_TPU/Cyclic_Tensile_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
MCT_1V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_1V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCT_3V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_3V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCT_4V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_4V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()


## PLOT EXPERIMENTAL data

## Plot Compression @RT
plt.figure(num=1)
plt.plot(UCT_2AH_278_m3[:,3], UCT_2AH_278_m3[:,4],label="_no_label_",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2BH_278_m3[:,3], UCT_2BH_278_m3[:,4],label="_no_label_", lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2CH_278_m3[:,3], UCT_2CH_278_m3[:,4],label="Exp. 2.78e-3/s",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)

plt.plot(UCT_3AH_278_m2[:,3], UCT_3AH_278_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
        #,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_3BH_278_m2[:,3], UCT_3BH_278_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
        #, marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_3CH_278_m2[:,3], UCT_3CH_278_m2[:,4],label="Exp. 2.78e-2/s",lw = 1.5,ls ="dashed",color="DarkBlue")
        #,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)

plt.plot(UCT_4AH_278_m1[:,3], UCT_4AH_278_m1[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_4BH_278_m1[:,3], UCT_4BH_278_m1[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_4CH_278_m1[:,3], UCT_4CH_278_m1[:,4],label="Exp. 2.78e-1/s",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)

plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Eng. Strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xlim([-0.005, 0.70])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.ylim([-1., 35.])
# plt.title("UTC TPU Strainrates RT", fontdict={'fontsize':10.5, 'weight': 'bold'})
plt.legend(fontsize = 12, loc = 'upper left') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Compression_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Plot Tension @RT
plt.figure(num=2)
plt.plot(UTT_1V_737_m4[:,3], UTT_1V_737_m4[:,4],label="_no_label_", lw = 2.5,ls ="dotted",color="DarkGreen")
         # ,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)
plt.plot(UTT_2V_737_m4[:,3], UTT_2V_737_m4[:,4],label="Exp. 7.37e-4/s", lw = 2.5,ls ="dotted",color="DarkGreen")
         #,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)

plt.plot(UTT_3V_737_m3[:,3], UTT_3V_737_m3[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)
plt.plot(UTT_4V_737_m3[:,3], UTT_4V_737_m3[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)
plt.plot(UTT_5V_737_m3[:,3], UTT_5V_737_m3[:,4],label="Exp. 7.37e-3/s",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)

plt.plot(UTT_6V_737_m2[:,3], UTT_6V_737_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         # ,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UTT_7V_737_m2[:,3], UTT_7V_737_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         # ,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UTT_8V_737_m2[:,3], UTT_8V_737_m2[:,4],label="Exp. 7.37e-2/s",lw = 1.5,ls ="dashdot",color="DarkRed")
         # ,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)

# plt.title("TPU Monotonic Loading @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.005, 0.05])
# plt.ylim([-0.1, 5.])
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Eng. Strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'lower right') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Tension_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Plot Compression cyclic @RT
plt.figure(num=3)
# plt.plot(UCT_2AH_278_m3[:,3], UCT_2AH_278_m3[:,4],label="2.78e-3 2AH", linewidth =2.0,alpha=0.8,color="DarkBlue")
#plt.plot(MCC_1H_278_m3[:,3], MCC_1H_278_m3[:,4],label="_no_label_",lw = 1.,ls ="solid",\
#        color="DarkGreen")
# plt.plot(MCC_1H_278_m3[:,3], MCC_1H_278_m3[:,4],label="_no_label_",lw = 1.,ls ="solid",\
#         color="DarkGreen")
# plt.plot(MCC_1V_278_m3[:,3], MCC_1V_278_m3[:,4],label="2.78e-3 1V", linewidth =1.0,alpha=0.8,color="DarkRed")
# plt.plot(MCC_2H_278_m3[:,3][0:-1:100], MCC_2H_278_m3[:,4][0:-1:100],label="Exp. 2.78e-3/s",lw = 1.,ls ="solid",\
#          marker='x',markersize=7,color="DarkGreen")
plt.plot(MCC_2H_278_m3[:,3], MCC_2H_278_m3[:,4],label="Exp. 2.78e-3/s",lw = 3.,ls ="solid",color="DarkGreen")
# plt.plot(MCC_2V_278_m3[:,3], MCC_2V_278_m3[:,4],label="2.78e-3 2V", linewidth =1.0,alpha=0.8,color="DarkRed")

# plt.plot(MCC_3V_556_m3[:,3], MCC_3V_556_m3[:,4],label="5.56e-3 3V", linewidth =1.0,alpha=0.8,color="Yellow")
#plt.plot(MCC_4H_556_m3[:,3], MCC_4H_556_m3[:,4],label="_no_label_",lw = 1.,ls ="dashed",color="DarkBlue")
# plt.plot(MCC_4V_556_m3[:,3], MCC_4V_556_m3[:,4],label="5.56e-3 4V", linewidth =1.0,alpha=0.8,color="Yellow")
# plt.plot(MCC_5H_556_m3[:,3][0:-1:200], MCC_5H_556_m3[:,4][0:-1:200],label="Exp. 5.56e-3/s",lw = 1.,ls ="dashed",\
#          marker='o',markersize=7,markerfacecolor='none',color="DarkBlue")
plt.plot(MCC_5H_556_m3[:,3], MCC_5H_556_m3[:,4],label="Exp. 5.56e-3/s",lw = 2.,ls ="dashed", color="DarkBlue")

# plt.plot(MCC_5V_278_m2[:,3], MCC_5V_278_m2[:,4],label="2.78e-2 5V", linewidth =1.0,alpha=0.8,color="DarkRed")
#plt.plot(MCC_6H_278_m2[:,3], MCC_6H_278_m2[:,4],label="_no_label_",lw = 1.,ls ="dashdot",color="DarkRed")
# plt.plot(MCC_6V_278_m2[:,3], MCC_6V_278_m2[:,4],label="2.78e-2 6V", linewidth =1.0,alpha=0.8,color="DarkRed")
#plt.plot(MCC_7H_278_m2[:,3][0:-1:200], MCC_7H_278_m2[:,4][0:-1:200],label="Exp. 2.78e-2/s", lw = 1.,ls ="dashdot",\
#         marker='s',markersize=7,markerfacecolor='none',color="DarkRed")
plt.plot(MCC_7H_278_m2[:,3], MCC_7H_278_m2[:,4],label="Exp. 2.78e-2/s", lw = 1.,ls ="dashdot", color="DarkRed")

plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Eng. Strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'upper left') # bbox_to_anchor=(1.1, 1.05))
# plt.title("TPU Cyclic Loading @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.25])
# plt.ylim([-0.5, 6.])
plt.savefig("Images_Paper1/TPU_Compression_cyclicMultiple_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Plot Compression cyclic @RT
plt.figure(num=4)
plt.plot(MCC_2H_278_m3[:,3], MCC_2H_278_m3[:,4],label="Exp. 2.78e-3/s",lw = 1.,ls ="solid",color="DarkGreen")
# plt.plot(MCC_2V_278_m3[:,3], MCC_2V_278_m3[:,4],label="2.78e-3 2V", linewidth =1.0,alpha=0.8,color="DarkRed")

plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Eng. Strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'upper left') # bbox_to_anchor=(1.1, 1.05))
# plt.title("TPU Cyclic Loading @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.25])
# plt.ylim([-0.5, 6.])
plt.savefig("Images_Paper1/TPU_Compression_278m3_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Plot Tension cyclic
plt.figure(num=5)
# plt.plot(UTT_1H_737_m4[:,3], UTT_1H_737_m4[:,4],label="7.37e-4 1H", linewidth =2.0,alpha=0.8,color="DarkBlue")
# plt.plot(UTT_1V_737_m4[:,3], UTT_1V_737_m4[:,4],label="7.37e-4 1V", linewidth =1.0,alpha=0.8,color="DarkRed")
# plt.plot(MCT_1V_737_m4[:,3], MCT_1V_737_m4[:,4],label="7.37e-4 1V", linewidth =1.0,alpha=0.8,color="DarkRed")
# plt.plot(MCT_3V_737_m4[:,3], MCT_3V_737_m4[:,4],label="7.37e-4 3V", linewidth =1.0,alpha=0.8,color="DarkBlue")
# plt.plot(MCT_4V_737_m4[:,3], MCT_4V_737_m4[:,4],label="7.37e-4 4V", linewidth =1.0,alpha=0.8,color="DarkGreen")
plt.plot(MCT_3V_737_m4[:,3], MCT_3V_737_m4[:,4],label="Exp. 7.37e-4/s", lw = 1.,ls ="solid",color="DarkRed")
# plt.title("Experiments: TPU Cyclic Loading @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Eng. Strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'upper left') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Tension_737m4_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Import Zoltan's experiments in tension (mm/s)
prefix_zoltan = "../../Experiments/TPU/Dumbbell Tension/"
UTT_10_m1 = pd.read_csv(prefix_zoltan+'0_1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_0 = pd.read_csv(prefix_zoltan+'1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_1 = pd.read_csv(prefix_zoltan+'10mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_2 = pd.read_csv(prefix_zoltan+'100mm_s.csv',sep =",",header=None).to_numpy()

# d) hollow dumbell JKU (quarter) - tension with Mechsrc and different strain rate
plt.figure(num=6)
plt.plot(UTT_10_m1[:,0], UTT_10_m1[:,1],label="Exp. 10.e-1/s", lw = 1.,ls ="none",color="b",
        marker='s',markeredgecolor="b",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_0[:,0], UTT_10_0[:,1],label="Exp. 10.e-0/s", lw = 1.,ls ="none",color="g",
        marker='o',markeredgecolor="g",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_1[:,0], UTT_10_1[:,1],label="Exp. 10.e+1/s", lw = 1.,ls ="none",color="r",
        marker='*',markeredgecolor="r",mew=2, ms=8,markerfacecolor='none')
plt.plot(UTT_10_2[:,0], UTT_10_2[:,1],label="Exp. 10.e+2/s", lw = 1.,ls ="none",color="m",
        marker='+',markeredgecolor="m",mew=2, ms=8,markerfacecolor='none')
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Displacement [mm]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'lower right') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Tension_HollowDumbbellZoltan_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Import Zoltan's experiments in compression
area_top = (np.pi*(7.5**2-4**2))

prefix_zoltan2 = "../../Experiments/TPU/Dumbbell Compression"
UCT_X_10_0 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1mms_1/TPUalt_X_1mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_1 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_10mms_1/TPUalt_X_10mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_2 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_100mms_1/TPUalt_X_100mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_3 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1000mms_1/TPUalt_X_1000mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_0 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_1mms_1/TPUalt_Z_1mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_1 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_10mms_1/TPUalt_Z_10mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_2 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_100mms_1/TPUalt_Z_100mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_3 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_1000mms_1/TPUalt_Z_1000mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()


plt.figure(num=7)
plt.plot(-UCT_X_10_0[:,1], -UCT_X_10_0[:,2]/area_top,label="Exp. X 1mm/s", lw = 1.,ls ="none",color="b",
        marker='s',markeredgecolor="b",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_1[:,1], -UCT_X_10_1[:,2]/area_top,label="Exp. X 10mm/s", lw = 1.,ls ="none",color="g",
        marker='o',markeredgecolor="g",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_2[:,1], -UCT_X_10_2[:,2]/area_top,label="Exp. X 100mm/s", lw = 1.,ls ="none",color="r",
        marker='*',markeredgecolor="r",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_X_10_3[:,1], -UCT_X_10_3[:,2]/area_top,label="Exp. X 1000mm/s", lw = 1.,ls ="none",color="m",
        marker='+',markeredgecolor="m",mew=2, ms=8,markerfacecolor='none',markevery=0.05)

"""
plt.plot(-UCT_Z_10_0[:,1], -UCT_Z_10_0[:,2]/area_top,label="Exp. Z 1mm/s", lw = 1.,ls ="none",color="c",
        marker='s',markeredgecolor="c",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_Z_10_1[:,1], -UCT_Z_10_1[:,2]/area_top,label="Exp. Z 10mm/s", lw = 1.,ls ="none",color="k",
        marker='o',markeredgecolor="k",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_Z_10_2[:,1], -UCT_Z_10_2[:,2]/area_top,label="Exp. Z 100mm/s", lw = 1.,ls ="none",color="y",
        marker='*',markeredgecolor="y",mew=2, ms=8,markerfacecolor='none',markevery=0.05)
plt.plot(-UCT_Z_10_3[:,1], -UCT_Z_10_3[:,2]/area_top,label="Exp. Z 1000mm/s", lw = 1.,ls ="none",color="DarkBlue",
        marker='+',markeredgecolor="DarkBlue",mew=2, ms=8,markerfacecolor='none')
"""

# plt.title("Experiments: TPU Cyclic Loading @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
plt.grid(color='k',alpha=0.3) #, linewidth=0.2)
plt.xlabel("Displacement [mm]",fontdict={'fontsize':12.})
plt.ylabel("Eng. Stress [MPa]",fontdict={'fontsize':12.})
plt.xlim([-0.001, 21])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = 'upper left') # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_Compression_HollowDumbbellZoltan_Experimental.pdf", bbox_inches="tight")
#plt.show()


## Import Zoltan's experiments in torsion
prefix_zoltan3 = "../../Experiments/TPU/Dumbbell Torsion/"
Import2 = pd.read_table(prefix_zoltan3+'TPUneu_X_01deg_s_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import3 = pd.read_table(prefix_zoltan3+'TPUneu_X_1deg_s_F0_Kanäle _Messwerte__2.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import4 = pd.read_table(prefix_zoltan3+'TPUneu_X_10deg_s_F0_3_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import5 = pd.read_table(prefix_zoltan3+'TPUneu_X_01deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=110,\
                        delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import6 = pd.read_table(prefix_zoltan3+'TPUneu_X_1deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import7 = pd.read_table(prefix_zoltan3+'TPUneu_X_10deg_s_weg0_3_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import8 = pd.read_table(prefix_zoltan3+'TPUneu_Z_1deg_s_2_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import9 = pd.read_table(prefix_zoltan3+'TPUneu_Z_1deg_s_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import10 = pd.read_table(prefix_zoltan3+'TPUneu_Z_01deg_s_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import11 = pd.read_table(prefix_zoltan3+'TPUneu_Z_01deg_s_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import12 = pd.read_table(prefix_zoltan3+'TPUneu_Z_10deg_s_F0_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import13 = pd.read_table(prefix_zoltan3+'TPUneu_Z_10deg_s_weg0_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()



plt.figure(num=8)
plt.plot(Import2[:,0][0:-1:500],(Import2[:,1][0:-1:500]),label="Exp.F0 0.1°/s",lw =2.,
        ls='dotted',color="r")
plt.plot(Import3[:,0][0:-1:500],Import3[:,1][0:-1:500],label="Exp.F0 1°/s ", lw =2,
        ls='dashed',color="g")
plt.plot(Import4[:,7][0:-1:500],Import4[:,0][0:-1:500],label="Exp.F0 10°/s ",lw =2,
        ls='dashdot',color="b")
plt.xlabel("Theta [°]",fontdict={'fontsize':12.})
plt.ylabel("Torque [Nm]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("TPU Dumbell Torsion - Fplt=0", fontdict={'fontsize':11.5, 'weight': 'bold'})
# plt.xlim([-0.001, 500])
# plt.ylim([-1., 10.])
plt.grid('on')
plt.legend(loc='lower right', fontsize=12) # bbox_to_anchor=(1.1, 1.05)) #)
plt.savefig("Images_Paper1/TPU_torsion_Fax0_Experimental.pdf", bbox_inches="tight") #, pad_inches=0.3, dpi = 300)
#plt.show()

plt.figure(num=9)
plt.plot(Import5[:,0][0:-1:500],Import5[:,2][0:-1:500],label="Exp.S0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
plt.plot(Import6[:,7][0:-1:500],Import6[:,0][0:-1:500],label="Exp.S0 1°/s ", lw =2.,
        ls='dashed',color="g")
plt.plot(Import7[:,7][0:-1:500],Import7[:,0][0:-1:500],label="Exp.S0 10°/s ",lw =2.,
        ls='dashdot',color="b")
plt.xlabel("Theta [°]",fontdict={'fontsize':12.})
plt.ylabel("Torque [Nm]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("TPU Dumbell Torsion - Fplt=0", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.xlim([-0.001, 470])
# plt.ylim([-1., 10.])
plt.grid('on')
plt.legend(loc='lower right', fontsize=12) # bbox_to_anchor=(1.1, 1.05)) #)
plt.savefig("Images_Paper1/TPU_torsion_Sax0_Experimental.pdf", bbox_inches="tight") #, pad_inches=0.3, dpi = 1000)
#plt.show()

plt.figure(num=10)
plt.plot(Import2[:,0][0:-1:500],(Import2[:,3][0:-1:500]),label="Exp.F0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
plt.plot(Import3[:,0][0:-1:500],Import3[:,4][0:-1:500],label="Exp.F0 1°/s ", lw =2,
        ls='dashed',color="g")
plt.plot(Import4[:,7][0:-1:500],Import4[:,6][0:-1:500],label="Exp.F0 10°/s ",lw =2,
        ls='dashdot',color="b")
plt.xlabel("Theta [°]",fontdict={'fontsize':12.})
plt.ylabel("Displacement [mm]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("TPU Dumbell Coarse Mesh", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.xlim([-0.001, 470])
# plt.ylim([-1., 10.])
plt.grid('on')
plt.legend(loc='upper left', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_verticalDisplacementFax0_Experimental.pdf", bbox_inches="tight") #, pad_inches=0.3, dpi = 300)
#plt.show()

plt.figure(num=11)
plt.plot(Import5[:,0][0:-1:500],-0.001*(Import5[:,4][0:-1:500]),label="Exp.S0 0.1°/s ",lw =2.,
        ls='dotted',color="r")
plt.plot(Import6[:,7][0:-1:500],-0.001*Import6[:,2][0:-1:500],label="Exp.S0 1°/s ", lw =2.,
        ls='dashed',color="g")
plt.plot(Import7[:,7][0:-1:500],-0.001*Import7[:,2][0:-1:500],label="Exp.S0 10°/s ",lw =2.,
        ls='dashdot',color="b")
plt.xlabel("Theta [°]",fontdict={'fontsize':12.})
plt.ylabel("Force [kN]",fontdict={'fontsize':12.})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("TPU Dumbell Coarse Mesh", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.xlim([-0.001, 470])
# plt.ylim([-1., 10.])
plt.grid('on')
plt.legend(loc='upper left', fontsize=12) # bbox_to_anchor=(1.1, 1.05))
plt.savefig("Images_Paper1/TPU_axialForceSax0_Experimental.pdf", bbox_inches="tight") #, pad_inches=0.3, dpi = 300)
#plt.show()
