#coding-Utf-8-*-
import os, shutil, fnmatch, pickle, csv
import pandas as pd
from loadCase_ParaValues_TPU_cube import *
# from runCaseScript import RunCase

os.system('python3 TPU_Experiments_monotonic_cyclic.py')

DirCase=["Case10"]
# Case10 is exclusively for TPU cube simulations

#################################################################
#### Case 10: TVP Tests using TPU material parameters
#################################################################
parent_dir = ""

#### Case 10_3 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
LoadCase.append(( 1, 7.37e-4, 23,1.85))
LoadCase.append(( 1, 7.37e-3, 23,1.85))
LoadCase.append(( 1, 7.37e-2, 23,1.85))
# LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_3"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_3 = []
F_Case_10_3 = []
epl_Case_10_3 = []
M_Case_10_3 = []
L_Case_10_3 = []
T_Case_10_3 = []
for a in range(len(LoadCase)):
    for i in range(1): # len(isoPara1)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [100]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_3.append(P_XX)
                    F_Case_10_3.append(F_XX)
                    epl_Case_10_3.append(epl_)
                    M_Case_10_3.append(MatPara)
                    L_Case_10_3.append(LoadCase[a])
                    T_Case_10_3.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_3.dat','wb') as data3:
    pickle.dump(P_Case_10_3,data3)
with open('F_Case_10_3.dat','wb') as data4:
    pickle.dump(F_Case_10_3,data4)
with open('epl_Case_10_3.dat','wb') as data:
    pickle.dump(epl_Case_10_3,data)
with open('M_Case_10_3.dat','wb') as data3:
    pickle.dump(M_Case_10_3,data3)
with open('L_Case_10_3.dat','wb') as data4:
    pickle.dump(L_Case_10_3,data4)
with open('T_Case_10_3.dat','wb') as data4:
    pickle.dump(T_Case_10_3,data4)


#### Case 10_4 - Baseline TVP response - Coompression monotonic, yes thermSrc, yes mechSrc

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
# LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
LoadCase.append((-1, 2.78e-3, 23,0.65))
LoadCase.append((-1, 2.78e-2, 23,0.65))
LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_4"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_4 = []
F_Case_10_4 = []
epl_Case_10_4 = []
M_Case_10_4 = []
L_Case_10_4 = []
T_Case_10_4 = []
for a in range(len(LoadCase)):
    for i in range(1): # len(isoPara1)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [100]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_4.append(P_XX)
                    F_Case_10_4.append(F_XX)
                    epl_Case_10_4.append(epl_)
                    M_Case_10_4.append(MatPara)
                    L_Case_10_4.append(LoadCase[a])
                    T_Case_10_4.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_4.dat','wb') as data3:
    pickle.dump(P_Case_10_4,data3)
with open('F_Case_10_4.dat','wb') as data4:
    pickle.dump(F_Case_10_4,data4)
with open('epl_Case_10_4.dat','wb') as data:
    pickle.dump(epl_Case_10_4,data)
with open('M_Case_10_4.dat','wb') as data3:
    pickle.dump(M_Case_10_4,data3)
with open('L_Case_10_4.dat','wb') as data4:
    pickle.dump(L_Case_10_4,data4)
with open('T_Case_10_4.dat','wb') as data4:
    pickle.dump(T_Case_10_4,data4)


#### Case 10_7 - Baseline TVP response - compression cyclic, yes thermSrc, yes mechSrc, no rotation

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
# LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
# LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_7"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_7 = []
F_Case_10_7 = []
epl_Case_10_7 = []
M_Case_10_7 = []
L_Case_10_7 = []
T_Case_10_7 = []
for a in range(len(LoadCase)):
    for i in range(1): # len(isoPara1)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [100]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_7.append(P_XX)
                    F_Case_10_7.append(F_XX)
                    epl_Case_10_7.append(epl_)
                    M_Case_10_7.append(MatPara)
                    L_Case_10_7.append(LoadCase[a])
                    T_Case_10_7.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_7.dat','wb') as data3:
    pickle.dump(P_Case_10_7,data3)
with open('F_Case_10_7.dat','wb') as data4:
    pickle.dump(F_Case_10_7,data4)
with open('epl_Case_10_7.dat','wb') as data:
    pickle.dump(epl_Case_10_7,data)
with open('M_Case_10_7.dat','wb') as data3:
    pickle.dump(M_Case_10_7,data3)
with open('L_Case_10_7.dat','wb') as data4:
    pickle.dump(L_Case_10_7,data4)
with open('T_Case_10_7.dat','wb') as data4:
    pickle.dump(T_Case_10_7,data4)


#### Case 10_8 - Baseline TVP response - Tension cyclic, yes thermSrc, yes mechSrc, no rotation

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
# LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
# LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_8"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_8 = []
F_Case_10_8 = []
epl_Case_10_8 = []
M_Case_10_8 = []
L_Case_10_8 = []
T_Case_10_8 = []
for a in range(len(LoadCase)):
    for i in range(1): # len(isoPara1)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [100]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_8.append(P_XX)
                    F_Case_10_8.append(F_XX)
                    epl_Case_10_8.append(epl_)
                    M_Case_10_8.append(MatPara)
                    L_Case_10_8.append(LoadCase[a])
                    T_Case_10_8.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_8.dat','wb') as data3:
    pickle.dump(P_Case_10_8,data3)
with open('F_Case_10_8.dat','wb') as data4:
    pickle.dump(F_Case_10_8,data4)
with open('epl_Case_10_8.dat','wb') as data:
    pickle.dump(epl_Case_10_8,data)
with open('M_Case_10_8.dat','wb') as data3:
    pickle.dump(M_Case_10_8,data3)
with open('L_Case_10_8.dat','wb') as data4:
    pickle.dump(L_Case_10_8,data4)
with open('T_Case_10_8.dat','wb') as data4:
    pickle.dump(T_Case_10_8,data4)


#### Case 10_9 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc, regularisation constant

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
# LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_9"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_9 = []
F_Case_10_9 = []
epl_Case_10_9 = []
M_Case_10_9 = []
L_Case_10_9 = []
T_Case_10_9 = []
for a in range(len(LoadCase)):
    for i in range(len(regularisation_constant)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [regularisation_constant[i]]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_9.append(P_XX)
                    F_Case_10_9.append(F_XX)
                    epl_Case_10_9.append(epl_)
                    M_Case_10_9.append(MatPara)
                    L_Case_10_9.append(LoadCase[a])
                    T_Case_10_9.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_9.dat','wb') as data3:
    pickle.dump(P_Case_10_9,data3)
with open('F_Case_10_9.dat','wb') as data4:
    pickle.dump(F_Case_10_9,data4)
with open('epl_Case_10_9.dat','wb') as data:
    pickle.dump(epl_Case_10_9,data)
with open('M_Case_10_9.dat','wb') as data3:
    pickle.dump(M_Case_10_9,data3)
with open('L_Case_10_9.dat','wb') as data4:
    pickle.dump(L_Case_10_9,data4)
with open('T_Case_10_9.dat','wb') as data4:
    pickle.dump(T_Case_10_9,data4)


#### Case 10_10 - Baseline TVP response - Compression monotonic, yes thermSrc, yes mechSrc, regularisation constant

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
# LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_10"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_10 = []
F_Case_10_10 = []
epl_Case_10_10 = []
M_Case_10_10 = []
L_Case_10_10 = []
T_Case_10_10 = []
for a in range(len(LoadCase)):
    for i in range(len(regularisation_constant)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [False]
                    regularisation = [regularisation_constant[i]]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_10.append(P_XX)
                    F_Case_10_10.append(F_XX)
                    epl_Case_10_10.append(epl_)
                    M_Case_10_10.append(MatPara)
                    L_Case_10_10.append(LoadCase[a])
                    T_Case_10_10.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_10.dat','wb') as data3:
    pickle.dump(P_Case_10_10,data3)
with open('F_Case_10_10.dat','wb') as data4:
    pickle.dump(F_Case_10_10,data4)
with open('epl_Case_10_10.dat','wb') as data:
    pickle.dump(epl_Case_10_10,data)
with open('M_Case_10_10.dat','wb') as data3:
    pickle.dump(M_Case_10_10,data3)
with open('L_Case_10_10.dat','wb') as data4:
    pickle.dump(L_Case_10_10,data4)
with open('T_Case_10_10.dat','wb') as data4:
    pickle.dump(T_Case_10_10,data4)



#### Case 10_5 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc, yes rotation

### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase=[]
LoadCase.append(( 1, 7.37e-4, 23,1.85))
# LoadCase.append(( 1, 7.37e-3, 23,1.85))
# LoadCase.append(( 1, 7.37e-2, 23,1.85))
LoadCase.append((-1, 2.78e-3, 23,0.65))
# LoadCase.append((-1, 2.78e-2, 23,0.65))
# LoadCase.append((-1, 2.78e-1, 23,0.65))
# LoadCase.append((2, 7.37e-4, 23,1.85))
# LoadCase.append((3, 2.78e-3, 23,0.65))
#------------------------------------------------------

### directory
child_dir = "TPU_case_10_5"
path = os.path.join(parent_dir, child_dir)
if os.path.exists(path) and os.path.isdir(path):
    shutil.rmtree(path)
    os.mkdir(path)
else:
    os.mkdir(path)

P_Case_10_5 = []
F_Case_10_5 = []
epl_Case_10_5 = []
M_Case_10_5 = []
L_Case_10_5 = []
T_Case_10_5 = []
for a in range(len(LoadCase)):
    for i in range(1): # len(isoPara1)):
        for j in range(1): # len(isoPara2)):
            for k in range(1): # len(isoPara3)):
                for l in range(1): # len(isoPara4)):
                    extra = []
                    extra = extra + extraBranch[0]
                    extra.append(extraBranch_Comp1[0])
                    extra = extra + extraBranch_additional[0]
                    IsoHard = [True,yieldStrenght[0],isoPara1[0],isoPara2[0],isoPara3[0],1.0,False,0.,0.,0.,0,isoPara4[0],isoPara5[0],isoPara6[0],isoPara7[0]]
                    KinHard = [True,kinPara1[0],kinPara2[0],kinPara3[0],False,0.,0.,0.,kinPara4[0]]
                    YieldFunctionParas = [3.5,0.64]
                    Viscosity = [True,1000.,0.21,False,0.,0.,0.]
                    TVE = [True,True] # extraBranchBoolean[i]]
                    Mullins = [True,mullinsPara1[0],False,0.,0.,0.]
                    HeatSrc = [True,True]
                    rotationCorrection = [True]
                    regularisation = [100]
                    MatPara = FillMatPara(extra,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,Mullins,HeatSrc,rotationCorrection,regularisation)
                    print(MatPara)
                    print(LoadCase[a])
                    with open('LoadCases.dat','wb') as data1:
                        pickle.dump(LoadCase[a],data1)
                    with open('MatPara.dat','wb') as data2:
                        pickle.dump(MatPara,data2)

                    os.system('python3 runCaseScript_TPU_cube.py')

                    # Load results
                    P = pd.read_csv("Average_P_XX.csv", sep=';', names=["Time", "avg_P"])
                    F = pd.read_csv("Average_F_XX.csv", sep=';', names=["Time", "avg_F"])
                    T = pd.read_csv("Average_TEMPERATURE.csv", sep=';', names=["Time", "avg_T"])
                    epl = pd.read_csv("Average_PLASTICSTRAIN.csv", sep=';', names=["Time", "avg_epl"])
                    if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                        P.loc[:,"avg_P"] *= -1
                        F.loc[:,"avg_F"] *= -1
                    P_XX, F_XX, epl_, T_ = [], [], [], []
                    for step in range(len(P)):
                        P_XX.append(P.loc[step].values[1])
                        T_.append(T.loc[step].values[1])
                        epl_.append(epl.loc[step].values[1])
                        if (LoadCase[a][0] == -1 or LoadCase[a][0] == -2):
                            F_XX.append(F.loc[step].values[1]+1.)
                        else:
                            F_XX.append(F.loc[step].values[1]-1.)
                    P_Case_10_5.append(P_XX)
                    F_Case_10_5.append(F_XX)
                    epl_Case_10_5.append(epl_)
                    M_Case_10_5.append(MatPara)
                    L_Case_10_5.append(LoadCase[a])
                    T_Case_10_5.append(T_)

                    # Move files
                    grandChild_dir = "LoadCase_"+str(a)
                    childPath = os.path.join(path, grandChild_dir)
                    if os.path.exists(childPath) and os.path.isdir(childPath):
                        shutil.rmtree(childPath)
                        os.mkdir(childPath)
                    else:
                        os.mkdir(childPath)

                    for file in os.scandir('./'):
                        if fnmatch.fnmatch(file, '*.csv') or fnmatch.fnmatch(file, '*.msh'):
                            shutil.move(file.name,childPath)
                            # os.remove(file) # to remove

                    print("###### End load case ######")

with open('P_Case_10_5.dat','wb') as data3:
    pickle.dump(P_Case_10_5,data3)
with open('F_Case_10_5.dat','wb') as data4:
    pickle.dump(F_Case_10_5,data4)
with open('epl_Case_10_5.dat','wb') as data:
    pickle.dump(epl_Case_10_5,data)
with open('M_Case_10_5.dat','wb') as data3:
    pickle.dump(M_Case_10_5,data3)
with open('L_Case_10_5.dat','wb') as data4:
    pickle.dump(L_Case_10_5,data4)
with open('T_Case_10_5.dat','wb') as data4:
    pickle.dump(T_Case_10_5,data4)
