import os, pickle
from os import listdir, makedirs
from os.path import isfile, join, exists, isdir
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from loadCase_ParaValues_TPU_cube import *
import matplotlib.cm as cm
from matplotlib_inline.backend_inline import set_matplotlib_formats
set_matplotlib_formats('pdf')

## Get All data
prefix = "../../Experiments/TPU/Bulk Characterisation/"

## Compression - @ Room Temperature
prefix1 = "Compression_bulk_RoomT_TPU/Compression_bulk_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
UCT_2AH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2AH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2AV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2AV_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2BH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2BH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2BV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2BV_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2CH_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2CH_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_2CV_278_m3 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_2CV_2p78E-3_RT_PA12.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3AH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3AH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3AV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3AV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3BH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3BH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3BV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3BV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3CH_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3CH_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_3CV_278_m2 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_3CV_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4AH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4AH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4AV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4AV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4BH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4BH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4BV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4BV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4CH_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4CH_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UCT_4CV_278_m1 = pd.read_csv(prefix+prefix1+"Uniaxial_compression_4CV_2p78E-1_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()

## Tension - @ Room Temperature
prefix2 = "Tensile_bulk_RoomT_TPU/Tensile_bulk_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
UTT_1H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_1H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_1V_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_1V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_2H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_2H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_2V_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_2V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_3H_737_m4 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_3H_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_3V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_3V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_4H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_4H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_4V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_4V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_5H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_5H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_5V_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_5V_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_6H_737_m3 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_6H_7p37E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_6V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_6V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_7H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_7H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_7V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_7V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_8H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_8H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_8V_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_8V_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
UTT_9H_737_m2 = pd.read_csv(prefix+prefix2+"Uniaxial_tension_9H_7p37E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()


## Compression cyclic - @ Room Temperature
prefix3 = "Cyclic_Compressive_RoomT_TPU/Cyclic_Compressive_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
MCC_1H_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_1H_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_1V_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_1V_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,11)).to_numpy()
MCC_2H_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_2H_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_2V_278_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_2V_2p78E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_3V_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_3V_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_4H_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_4H_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_4V_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_4V_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_5H_556_m3 = pd.read_csv(prefix+prefix3+"Cyclic_compression_5H_5p56E-3_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_5V_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_5V_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_6H_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_6H_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_6V_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_6V_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCC_7H_278_m2 = pd.read_csv(prefix+prefix3+"Cyclic_compression_7H_2p78E-2_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()

## Tension cyclic - @ Room Temperature
prefix4 = "Cyclic_Tensile_RoomT_TPU/Cyclic_Tensile_RoomT_TPU/"

# columns = [Time(s)	Extension(mm)	Load(N) 	Eng.strain(Mpa)	Eng.stress]
MCT_1V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_1V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCT_3V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_3V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()
MCT_4V_737_m4 = pd.read_csv(prefix+prefix4+"Cyclic_tension_4V_7p37E-4_RT_TPU.csv",\
    sep='\s', header = None, skiprows = range(0,10)).to_numpy()

## Import Zoltan's experiments in tension (mm/s)
prefix_zoltan = "../../Experiments/TPU/Dumbbell Tension/"
UTT_10_m1 = pd.read_csv(prefix_zoltan+'0_1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_0 = pd.read_csv(prefix_zoltan+'1mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_1 = pd.read_csv(prefix_zoltan+'10mm_s.csv',sep =",",header=None).to_numpy()
UTT_10_2 = pd.read_csv(prefix_zoltan+'100mm_s.csv',sep =",",header=None).to_numpy()

## Import Zoltan's experiments in compression
area_top = (np.pi*(7.5**2-4**2))

prefix_zoltan2 = "../../Experiments/TPU/Dumbbell Compression"
UCT_X_10_0 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1mms_1/TPUalt_X_1mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_1 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_10mms_1/TPUalt_X_10mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_2 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_100mms_1/TPUalt_X_100mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_X_10_3 = pd.read_csv(prefix_zoltan2+'/TPUalt_X_1000mms_1/TPUalt_X_1000mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_0 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_1mms_1/TPUalt_Z_1mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_1 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_10mms_1/TPUalt_Z_10mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_2 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_100mms_1/TPUalt_Z_100mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()
UCT_Z_10_3 = pd.read_csv(prefix_zoltan2+'/TPUalt_Z_1000mms_1/TPUalt_Z_1000mms_1Std_Data.csv',\
    sep='\s', header = None, skiprows = range(0,5)).to_numpy()



## Import Zoltan's experiments in torsion
prefix_zoltan3 = "../../Experiments/TPU/Dumbbell Torsion/"
Import2 = pd.read_table(prefix_zoltan3+'TPUneu_X_01deg_s_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import3 = pd.read_table(prefix_zoltan3+'TPUneu_X_1deg_s_F0_Kanäle _Messwerte__2.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import4 = pd.read_table(prefix_zoltan3+'TPUneu_X_10deg_s_F0_3_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import5 = pd.read_table(prefix_zoltan3+'TPUneu_X_01deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=110,\
                        delimiter = "\t",dtype=np.float64,decimal=",",on_bad_lines='skip').to_numpy()
Import6 = pd.read_table(prefix_zoltan3+'TPUneu_X_1deg_s_weg0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import7 = pd.read_table(prefix_zoltan3+'TPUneu_X_10deg_s_weg0_3_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import8 = pd.read_table(prefix_zoltan3+'TPUneu_Z_1deg_s_2_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import9 = pd.read_table(prefix_zoltan3+'TPUneu_Z_1deg_s_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import10 = pd.read_table(prefix_zoltan3+'TPUneu_Z_01deg_s_F0_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import11 = pd.read_table(prefix_zoltan3+'TPUneu_Z_01deg_s_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import12 = pd.read_table(prefix_zoltan3+'TPUneu_Z_10deg_s_F0_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()
Import13 = pd.read_table(prefix_zoltan3+'TPUneu_Z_10deg_s_weg0_2_Kanäle _Messwerte__1.csv',skiprows=[1],\
                        delimiter = "\t",dtype=np.float64,decimal=",").to_numpy()


## PLOT Experimental vs. Numerical data

## Case 10_3 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc
####
with open('P_Case_10_3.dat','rb') as data:
    stress_Case_10_3=pickle.load(data)
with open('F_Case_10_3.dat','rb') as data:
    strain_Case_10_3=pickle.load(data)
with open('M_Case_10_3.dat','rb') as data:
    MatPara_Case_10_3=pickle.load(data)
with open('L_Case_10_3.dat','rb') as data:
    LoadCase_Case_10_3=pickle.load(data)
with open('T_Case_10_3.dat','rb') as data:
    temperature_Case_10_3=pickle.load(data)

## Plot 3) Case 10_3
plt.figure(num=3)
discr = np.linspace(0, 1, 10)
colors = cm.hsv(discr)
temp = 0

# plt.plot(UCT_2AH_278_m3[:,3], UCT_2AH_278_m3[:,4],label="_no_label_",lw = 2.,ls ="dotted",color="DarkGreen")
plt.plot(UTT_1V_737_m4[:,3], UTT_1V_737_m4[:,4],label="_no_label_", lw = 2.5,ls ="dotted",color="DarkGreen")
         # ,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)
plt.plot(UTT_2V_737_m4[:,3], UTT_2V_737_m4[:,4],label="Exp. 7.37e-4/s", lw = 2.5,ls ="dotted",color="DarkGreen")
         #,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)

plt.plot(UTT_3V_737_m3[:,3], UTT_3V_737_m3[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)
plt.plot(UTT_4V_737_m3[:,3], UTT_4V_737_m3[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)
plt.plot(UTT_5V_737_m3[:,3], UTT_5V_737_m3[:,4],label="Exp. 7.37e-3/s",lw = 1.5,ls ="dashed",color="DarkBlue")
         # ,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=500)

plt.plot(UTT_6V_737_m2[:,3], UTT_6V_737_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         # ,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UTT_7V_737_m2[:,3], UTT_7V_737_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         # ,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UTT_8V_737_m2[:,3], UTT_8V_737_m2[:,4],label="Exp. 7.37e-2/s",lw = 1.5,ls ="dashdot",color="DarkRed")

for i in range(len(LoadCase_Case_10_3)):
    strain_Case_10_3[temp].insert(0,0)
    stress_Case_10_3[temp].insert(0,0)
    temperature_Case_10_3[temp].insert(0,273.15+23)
    temp += 1
"""
plt.plot(strain_Case_10_1[0], stress_Case_10_1[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s', color="g",
        ls='solid',marker="o",markeredgecolor="g",mew=2, ms=10,markevery=20,markerfacecolor='none')
plt.plot(strain_Case_10_1[1], stress_Case_10_1[1], alpha = 1.,lw=1.5, label='Num. 7.37e-3/s', color="b",
        ls='solid',marker="x",markeredgecolor="b",mew=2, ms=10,markevery=15,markerfacecolor='none')
plt.plot(strain_Case_10_1[-1], stress_Case_10_1[-1], alpha = 1.,lw=1.5, label='Num. 7.37e-2/s', color="r",
        ls='solid',marker="s",markeredgecolor="r",mew=2, ms=8,markevery=20,markerfacecolor='none')
"""
plt.plot(strain_Case_10_3[0], stress_Case_10_3[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s', color="g",
        ls='dashdot',marker="o",markeredgecolor="g",mew=2, ms=10,markevery=20,markerfacecolor='none')
plt.plot(strain_Case_10_3[1], stress_Case_10_3[1], alpha = 1.,lw=1.5, label='Num. 7.37e-3/s', color="b",
        ls='dashdot',marker="x",markeredgecolor="b",mew=2, ms=10,markevery=15,markerfacecolor='none')
plt.plot(strain_Case_10_3[-1], stress_Case_10_3[-1], alpha = 1.,lw=1.5, label='Num. 7.37e-2/s', color="r",
        ls='dashdot',marker="s",markeredgecolor="r",mew=2, ms=8,markevery=20,markerfacecolor='none')
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "lower right") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.7])
plt.ylim([-1., 10.])
# plt.savefig("Images_Paper1/TPU_Tension_DispEffect_Numerical.pdf", bbox_inches="tight")
plt.savefig("Images_Paper1/TPU_Tension_Dissipation_Numerical.pdf", bbox_inches="tight")
#plt.show()

## Plot 4) Case 10_3 - Temperature
plt.figure(num=4)
plt.plot(strain_Case_10_3[0], temperature_Case_10_3[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s', color="c",
        ls='dashdot',marker="o",markeredgecolor="c",mew=2, ms=10,markevery=20,markerfacecolor='none')
plt.plot(strain_Case_10_3[1], temperature_Case_10_3[1], alpha = 1.,lw=1.5, label='Num. 7.37e-3/s', color="m",
        ls='dashdot',marker="x",markeredgecolor="m",mew=2, ms=10,markevery=15,markerfacecolor='none')
plt.plot(strain_Case_10_3[-1], temperature_Case_10_3[-1], alpha = 1.,lw=1.5, label='Num. 7.37e-2/s', color="k",
        ls='dashdot',marker="s",markeredgecolor="k",mew=2, ms=8,markevery=20,markerfacecolor='none')

plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Temperature [K]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "lower right") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.savefig("Images_Paper1/TPU_Tension_DispEffect_Temperature_Numerical.pdf", bbox_inches="tight")
#plt.show()


#### Case 10_4 - Baseline TVP response - Compression monotonic, yes thermSrc, yes mechSrc
####
with open('P_Case_10_4.dat','rb') as data:
    stress_Case_10_4=pickle.load(data)
with open('F_Case_10_4.dat','rb') as data:
    strain_Case_10_4=pickle.load(data)
with open('M_Case_10_4.dat','rb') as data:
    MatPara_Case_10_4=pickle.load(data)
with open('L_Case_10_4.dat','rb') as data:
    LoadCase_Case_10_4=pickle.load(data)
with open('T_Case_10_4.dat','rb') as data:
    temperature_Case_10_4=pickle.load(data)

## Plot 5) Case 10_4
plt.figure(num=5)
discr = np.linspace(0, 1, 10)
colors = cm.hsv(discr)
temp = 0

plt.plot(UCT_2AH_278_m3[:,3], UCT_2AH_278_m3[:,4],label="_no_label_",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2BH_278_m3[:,3], UCT_2BH_278_m3[:,4],label="_no_label_", lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2CH_278_m3[:,3], UCT_2CH_278_m3[:,4],label="Exp. 2.78e-3/s",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)

plt.plot(UCT_3AH_278_m2[:,3], UCT_3AH_278_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
        #,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_3BH_278_m2[:,3], UCT_3BH_278_m2[:,4],label="_no_label_",lw = 1.5,ls ="dashed",color="DarkBlue")
        #, marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_3CH_278_m2[:,3], UCT_3CH_278_m2[:,4],label="Exp. 2.78e-2/s",lw = 1.5,ls ="dashed",color="DarkBlue")
        #,marker='o',markeredgecolor="b",mew=1, ms=8,markerfacecolor='none',markevery=50)

plt.plot(UCT_4AH_278_m1[:,3], UCT_4AH_278_m1[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_4BH_278_m1[:,3], UCT_4BH_278_m1[:,4],label="_no_label_",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)
plt.plot(UCT_4CH_278_m1[:,3], UCT_4CH_278_m1[:,4],label="Exp. 2.78e-1/s",lw = 1.5,ls ="dashdot",color="DarkRed")
         #,marker='s',markeredgecolor="r",mew=1, ms=8,markerfacecolor='none',markevery=50)

for i in range(len(LoadCase_Case_10_4)):
    strain_Case_10_4[temp].insert(0,0)
    stress_Case_10_4[temp].insert(0,0)
    temperature_Case_10_4[temp].insert(0,273.15+23)
    temp += 1

plt.plot(strain_Case_10_4[0], stress_Case_10_4[0], alpha = 1.,lw=1.5, label='Num. 2.78e-3/s', color="g",
        ls='dashdot',marker="o",markeredgecolor="g",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_4[1], stress_Case_10_4[1], alpha = 1.,lw=1.5, label='Num. 2.78e-2/s', color="b",
        ls='dashdot',marker="x",markeredgecolor="b",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_4[-1], stress_Case_10_4[-1], alpha = 1.,lw=1.5, label='Num. 2.78e-1/s', color="r",
        ls='dashdot',marker="s",markeredgecolor="r",mew=2, ms=8,markevery=0.1,markerfacecolor='none')

plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper left") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.savefig("Images_Paper1/TPU_Compression_DispEffect_Numerical.pdf", bbox_inches="tight")
# plt.savefig("Images_Paper1/TPU_Compression_Dissipation_Numerical.pdf", bbox_inches="tight")
#plt.show()

## Plot 6) Case 10_4 - temperature
plt.figure(num=6)
discr = np.linspace(0, 1, 10)
colors = cm.hsv(discr)
temp = 0
plt.plot(strain_Case_10_4[0], temperature_Case_10_4[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s', color="c",
        ls='dashdot',marker="o",markeredgecolor="c",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_4[1], temperature_Case_10_4[1], alpha = 1.,lw=1.5, label='Num. 7.37e-3/s', color="m",
        ls='dashdot',marker="x",markeredgecolor="m",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_4[-1], temperature_Case_10_4[-1], alpha = 1.,lw=1.5, label='Num. 7.37e-2/s', color="k",
        ls='dashdot',marker="s",markeredgecolor="k",mew=2, ms=8,markevery=0.1,markerfacecolor='none')

plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Temperature [K]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper left") # ,bbox_to_anchor=(1.55, 0.5))
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.savefig("Images_Paper1/TPU_Compression_DispEffect_Temperature_Numerical.pdf", bbox_inches="tight")
#plt.show()


#### Case 10_5 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc, yes rotation
####
with open('P_Case_10_5.dat','rb') as data:
    stress_Case_10_5=pickle.load(data)
with open('F_Case_10_5.dat','rb') as data:
    strain_Case_10_5=pickle.load(data)
with open('M_Case_10_5.dat','rb') as data:
    MatPara_Case_10_5=pickle.load(data)
with open('L_Case_10_5.dat','rb') as data:
    LoadCase_Case_10_5=pickle.load(data)
with open('T_Case_10_5.dat','rb') as data:
    temperature_Case_10_5=pickle.load(data)

## Plot 7) Case 10_5 - Tension
plt.figure(num=7)
plt.plot(UTT_1V_737_m4[:,3], UTT_1V_737_m4[:,4],label="_no_label_", lw = 2.5,ls ="dashed",color="DarkGreen")
         # ,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)
plt.plot(UTT_2V_737_m4[:,3], UTT_2V_737_m4[:,4],label="Exp. 7.37e-4/s", lw = 2.5,ls ="dashed",color="DarkGreen")
         #,marker='x',markeredgecolor="g",mew=1, ms=8,markerfacecolor='none',markevery=3000)

plt.plot(strain_Case_10_3[0], stress_Case_10_3[0], alpha = 1.,lw=3., label='Num. 7.37e-4/s', color="g",
        ls='solid',marker="o",markeredgecolor="g",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_5[0], stress_Case_10_5[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s with correction', color="r",
        ls='solid',marker="x",markeredgecolor="r",mew=2, ms=10,markevery=0.1,markerfacecolor='none')

plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.legend(fontsize = 12,loc = "lower right") #bbox_to_anchor=(1.1, 1.0))
plt.grid('on')
plt.savefig("Images_Paper1/TPU_Tension_Rotation_Numerical.pdf", bbox_inches="tight")
#plt.show()

## Plot 8) Case 10_5 - Compression
plt.figure(num=8)
plt.plot(UCT_2AH_278_m3[:,3], UCT_2AH_278_m3[:,4],label="_no_label_",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2BH_278_m3[:,3], UCT_2BH_278_m3[:,4],label="_no_label_", lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)
plt.plot(UCT_2CH_278_m3[:,3], UCT_2CH_278_m3[:,4],label="Exp. 2.78e-3/s",lw = 2.,ls ="dotted",color="DarkGreen")
        # ,marker='x',markeredgecolor="g",mew=1, ms=10,markerfacecolor='none',markevery=250)

plt.plot(strain_Case_10_4[0], stress_Case_10_4[0], alpha = 1.,lw=3., label='Num. 2.78e-3/s', color="g",
        ls='solid',marker="s",markeredgecolor="g",mew=2, ms=10,markevery=0.1,markerfacecolor='none')
plt.plot(strain_Case_10_5[1], stress_Case_10_5[1], alpha = 1.,lw=1.5, label='Num. 2.78e-3/s with correction', color="r",
        ls='solid',marker="+",markeredgecolor="r",mew=2, ms=10,markevery=0.1,markerfacecolor='none')

plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.legend(fontsize = 12,loc = "upper left") # bbox_to_anchor=(1.1, 1.0),
plt.grid('on')
plt.savefig("Images_Paper1/TPU_Compression_Rotation_Numerical.pdf", bbox_inches="tight")
#plt.show()


#### Case 10_7 - Baseline TVP response - Compression cyclic, no thermSrc, no mechSrc, no rotation
####
with open('P_Case_10_7.dat','rb') as data:
    stress_Case_10_7=pickle.load(data)
with open('F_Case_10_7.dat','rb') as data:
    strain_Case_10_7=pickle.load(data)
with open('M_Case_10_7.dat','rb') as data:
    MatPara_Case_10_7=pickle.load(data)
with open('L_Case_10_7.dat','rb') as data:
    LoadCase_Case_10_7=pickle.load(data)
with open('T_Case_10_7.dat','rb') as data:
    temperature_Case_10_7=pickle.load(data)

## Plot 9) Case 10_7
plt.figure(num=9)
plt.plot(MCC_1H_278_m3[:,3], MCC_1H_278_m3[:,4],label="Exp. 2.78e-3/s", linewidth =2.0,alpha=0.6,color="DarkBlue")
strain_Case_10_7[0] = [i * -1 for i in strain_Case_10_7[0]]
stress_Case_10_7[0] = [i * -1 for i in stress_Case_10_7[0]]
plt.plot(strain_Case_10_7[0], stress_Case_10_7[0], alpha = 1.,lw=1.5, label='Num. 2.78e-3/s',color='r')
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper left") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.savefig("Images_Paper1/TPU_Compression_Cyclic_Dissipation_Numerical.pdf", bbox_inches="tight")
#plt.show()


#### Case 10_8 - Baseline TVP response - Tension cyclic, no thermSrc, no mechSrc, no rotation
####
with open('P_Case_10_8.dat','rb') as data:
    stress_Case_10_8=pickle.load(data)
with open('F_Case_10_8.dat','rb') as data:
    strain_Case_10_8=pickle.load(data)
with open('M_Case_10_8.dat','rb') as data:
    MatPara_Case_10_8=pickle.load(data)
with open('L_Case_10_8.dat','rb') as data:
    LoadCase_Case_10_8=pickle.load(data)
with open('T_Case_10_8.dat','rb') as data:
    temperature_Case_10_8=pickle.load(data)

## Plot 10) Case 10_8
plt.figure(num=10)
plt.plot(MCT_3V_737_m4[:,3], MCT_3V_737_m4[:,4],label="Exp. 7.37e-4/s", lw =1.5,alpha=0.6,color="DarkBlue")
plt.plot(strain_Case_10_8[0], stress_Case_10_8[0], alpha = 1.,lw=1.5, label='Num. 7.37e-4/s',color='r')
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.})
plt.grid(color='k', linewidth=0.25)
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper left") # ,bbox_to_anchor=(1.55, 0.5))
# plt.title("TPU @ 23°C", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
# plt.ylim([-1., 40.])
plt.savefig("Images_Paper1/TPU_Tension_Cyclic_Dissipation_Numerical.pdf", bbox_inches="tight")
#plt.show()
