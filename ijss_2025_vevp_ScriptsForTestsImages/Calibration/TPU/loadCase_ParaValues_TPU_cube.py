######## list MatPara ########
#### MatPara[0][...] -> ExtraBranch -> (4 + 1 parameters)
#### MatPara[1][...] -> IsoHard -> (0-boolean,1-yieldStrenght,2-isoPara1,3-isoPara2,4-isoPara3,5-m,6-tempBoolean,7-isoTempFunc1,8-isoTempFunc2,9-isoTempFunc3,10-TYPE)
#### MatPara[2][...] -> KinHard -> (0-boolean,1-kinPara1,2-kinPara2,3-kinPara3,4-tempBoolean,5-kinTempFunc1,6-kinTempFunc2,7-kinTempFunc3)
#### MatPara[3][...] -> YieldFunctionParas -> (0-YieldPowerFactor,1-FlowRuleFactor)
#### MatPara[4][...] -> Viscosity -> (0-boolean,1-viscPara1,2-viscPara2,3-tempBoolean,4-viscoTempFunc1,5-viscoTempFunc2,6-viscoTempFunc3)
#### MatPara[5][...] -> TVE -> (0-TVE_Boolean, 1-ExtraBranch_Boolean)
#### MatPara[6][...] -> Mullins -> (0-boolean,1-mullinsPara1,2-tempBoolean,3-mullinsTempFunc1,4-mullinsTempFunc2,5-mullinsTempFunc3)
#### MatPara[7][...] -> heatSrc -> (0-boolean ThermSrc and tempBC, 1-boolean MechSrc)
#### MatPara[8][...] -> rotationCorrection -> (0-boolean)
#### MatPara[9][...] -> regularisation -> (0-regularisation constant)

def FillMatPara(extraBranch,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,mullins,heatSrc,rotationCorrection,regularisation):
  MatPara = []
  MatPara.append(extraBranch)
  MatPara.append(IsoHard)
  MatPara.append(KinHard)
  MatPara.append(YieldFunctionParas)
  MatPara.append(Viscosity)
  MatPara.append(TVE)
  MatPara.append(mullins)
  MatPara.append(heatSrc)
  MatPara.append(rotationCorrection)
  MatPara.append(regularisation)
  return MatPara

####### list extraBranch
extraBranchBoolean = [False,True]

# Form:  Av and Bd are => _V3[i]*(1.+tanh(x2))) to _V3[i]*(tanh(x2)))
# x1 = _V1[i]*tr*tr + _V2[i];
# x2 = _V4[i]*tr*tr;
# Av = sigmoid*(1./sqrt(x1)+ _V3[i]*(_V5[i]+tanh(x2))) + (1.-sigmoid)*(_Ci[i]*(1./sqrt(x1) + _V0[i]));

extraBranch = [[0.15, 1500.0, 0.75, 0.15, 150.0, 0.75]]
extraBranch_additional = [[8.5, 0.75, 0.028, 8.5, .075, 0.028]]
extraBranch_Comp1 = [1.]

####### list IsoHard
isoBoolean = [True]
yieldStrenght = [4.5]
isoPara1 = [4.]
isoPara2 = [4.]
isoPara3 = [4.]
m = [1.]
isoTempBoolean = [False]
isoTempFunc1 = [0.]
isoTempFunc2 = [0.]
isoTempFunc3 = [0.]
isoPara4 = [4.5]
isoPara5 = [5]
isoPara6 = [7.5]
isoPara7 = [12.5]

####### list kinHard
kinBoolean = [False,True]
kinPara1 = [20]
kinPara2 = [25]
kinPara3 = [0]
kinTempBoolean = [False]
kinTempFunc1 = [0.]
kinTempFunc2 = [0.]
kinTempFunc3 = [0.]
kinPara4 = [0.]

###### list YieldFunctionParas
YieldPowerFactor = [3.]
FlowRuleFactor = [0.64]

###### list Viscosity
viscBoolean = [False,True]
viscPara1 = [1000.]
viscPara2 = [0.21]
visctempBoolean = [False]
viscTempFunc1 = [0.]
viscTempFunc2 = [0.]
viscTempFunc3 = [0.]

##### list TVE
TVE_Boolean = [True, False]
ExtraBranch_Boolean = [True, False]

##### list Mullins
mullinsBoolean = [True, False]
mullinsPara1 = [0.99]
mullinstempBoolean = [False]
mullinsTempFunc1 = [0.]
mullinsTempFunc2 = [0.]
mullinsTempFunc3 = [0.]

##### list heatSrc
thermSrc_Boolean = [True, False]
mechSrc_Boolean = [True, False]

##### list rotationCorrection
rotationCorrection_Boolean = [True, False]

##### list regularisation
regularisation_constant = [10,100,500,1000,2000] # ,5000]
