// Cube.geo
mm=1.0;	// Units
n=1;		// Number of layers (in thickness / along z)
m = 1;		// Number of element + 1 along y
p = 1; 		// Number of element + 1 along x
L=1.*mm;	// Cube lenght
sl1=L/n;

// Face z = 0 (point in anti-clockwise order)
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,L,0,sl1};
Point(4)={0,L,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};

// Cube extrusion
Extrude {0, 0, L} {
  Surface{6}; Layers{n}; Recombine;
}

// Physical entities
	// Volume
Physical Volume(29) = {1};
	// Surface
Physical Surface(30) = {19}; // Left face x = 0
Physical Surface(31) = {27}; // Right face x = L
Physical Surface(32) = {6};  // Back face z = 0
Physical Surface(33) = {28}; // Front face z = L
Physical Surface(34) = {15}; // Down face y = 0
Physical Surface(35) = {23}; // Up face y = L

// Mesh
Transfinite Line {2, 4} = m Using Progression 1;
Transfinite Line {1, 3} = p Using Progression 1;
Transfinite Surface {6};
Recombine Surface{6};


// To save cube elongation
Physical Point(41) = {1}; // Point on left face (0,0,0)
Physical Point(42) = {4}; // Point on left face (0,L,0)
Physical Point(43) = {2}; // Point on right face (L,0,0)
Physical Point(44) = {3}; // Point on right face (L,L,0)
