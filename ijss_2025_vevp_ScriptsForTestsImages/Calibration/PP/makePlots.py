import os
import pickle
from os import listdir, makedirs
from os.path import isfile, join, exists, isdir
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from loadCase_ParaValues_PP_cube import *
import matplotlib.cm as cm
from matplotlib_inline.backend_inline import set_matplotlib_formats
set_matplotlib_formats('pdf')

#### Import all polypropylene experiments

prefix = ""
with open(prefix+'PP_UTT_QS.dat', 'rb') as data:
    UTT_QS = pickle.load(data)
with open(prefix+'PP_UCT_QS.dat', 'rb') as data:
    UCT_QS = pickle.load(data)
with open(prefix+'PP_UTT_SR_0_5.dat', 'rb') as data:
    UTT_SR_0_5 = pickle.load(data)
with open(prefix+'PP_UTT_SR_5_0.dat', 'rb') as data:
    UTT_SR_5_0 = pickle.load(data)
with open(prefix+'PP_MCT_QS.dat', 'rb') as data:
    MCT_QS = pickle.load(data)


#########
### PLOTS
#########

#### Case 10_1 - Baseline TVP response - Tension monotonic, no thermSrc, no mechSrc
with open('P_Case_10_1.dat', 'rb') as data:
    stress_Case_10_1 = pickle.load(data)
with open('F_Case_10_1.dat', 'rb') as data:
    strain_Case_10_1 = pickle.load(data)
with open('M_Case_10_1.dat', 'rb') as data:
    MatPara_Case_10_1 = pickle.load(data)
with open('L_Case_10_1.dat', 'rb') as data:
    LoadCase_Case_10_1 = pickle.load(data)

## Plot 1) Case 10_1
plt.figure(num=1)
plt.plot(UTT_QS[12], UTT_QS[13], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[14], UTT_QS[15], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[16], UTT_QS[17], label="Exp. -10°C",
         alpha=1.0, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[0], UTT_QS[1], label="_no_label",
         alpha=0.6, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3], label="_no_label",
         alpha=0.8, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5], label="Exp. 23°C",
         alpha=1.0, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[6], UTT_QS[7], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[8], UTT_QS[9], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[10], UTT_QS[11], label="Exp. 70°C",
         alpha=1.0, lw=1.5, ls="dashdot", color="DarkRed")

plt.plot(strain_Case_10_1[-1], stress_Case_10_1[-1], alpha=1., lw=1.5, label='Num. -10°C', color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_1[0], stress_Case_10_1[0], alpha=1., lw=1.5, label='Num. 23°C', color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_1[1], stress_Case_10_1[1], alpha=1., lw=1.5, label='Num. 70°C', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=10, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.20])
plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="upper right")
plt.savefig("Images_Paper1/PP_Tension_Numerical.pdf", bbox_inches="tight")

#### Case 10_2 - Baseline TVP response - Tension monotonic, no thermSrc, no mechSrc
with open('P_Case_10_2.dat', 'rb') as data:
    stress_Case_10_2 = pickle.load(data)
with open('F_Case_10_2.dat', 'rb') as data:
    strain_Case_10_2 = pickle.load(data)
with open('M_Case_10_2.dat', 'rb') as data:
    MatPara_Case_10_2 = pickle.load(data)
with open('L_Case_10_2.dat', 'rb') as data:
    LoadCase_Case_10_2 = pickle.load(data)

## Plot 2) Case 10_2
plt.figure(num=2)
plt.plot(UTT_QS[0], UTT_QS[1], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.6, color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.8, color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5], label="Exp. 23°C, QS",
         lw=1.5, ls="dotted", alpha=1.0, color="DarkGreen")
plt.plot(UTT_SR_0_5[0][0:-1:10], UTT_SR_0_5[1][0:-1:10],
         label="_no_label", lw=1.5, ls="dashed", alpha=0.8, color="DarkBlue")
plt.plot(UTT_SR_0_5[2][0:-1:10], UTT_SR_0_5[3][0:-1:10],
         label="_no_label", lw=1.5, ls="dashed", alpha=0.9, color="DarkBlue")
plt.plot(UTT_SR_0_5[4][0:-1:10], UTT_SR_0_5[5][0:-1:10],
         label="Exp. 23°C, 0.5/s", lw=1.5, ls="dashed", alpha=0.6, color="DarkBlue")
plt.plot(UTT_SR_5_0[0][0:-1:10], UTT_SR_5_0[1][0:-1:10],
         label="_no_label", lw=1.5, ls="dashdot", alpha=0.8, color="DarkRed")
plt.plot(UTT_SR_5_0[2][0:-1:10], UTT_SR_5_0[3][0:-1:10],
         label="_no_label", lw=1.5, ls="dashdot", alpha=0.9, color="DarkRed")
plt.plot(UTT_SR_5_0[4][0:-1:10], UTT_SR_5_0[5][0:-1:10],
         label="Exp. 23°C, 5.0/s", lw=1.5, ls="dashdot", alpha=1.0, color="DarkRed")

plt.plot(strain_Case_10_2[0], stress_Case_10_2[0], alpha=1., lw=1.5, label='Num. 23°C, QS', color="g",
         ls='solid', marker="s", markeredgecolor="g", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_2[1], stress_Case_10_2[1], alpha=1., lw=1.5, label='Num. 23°C, 0.5/s', color="b",
         ls='solid', marker="o", markeredgecolor="b", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_2[-1], stress_Case_10_2[-1], alpha=1., lw=1.5, label='Num. 23°C, 5.0/s', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=20, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.10])
plt.ylim([-1., 30.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="lower right")
plt.savefig("Images_Paper1/PP_Tension_SR_Numerical.pdf", bbox_inches="tight")


#### Case 10_3 - Baseline TVP response - Tension monotonic, no thermSrc, no mechSrc
with open('P_Case_10_3.dat', 'rb') as data:
    stress_Case_10_3 = pickle.load(data)
with open('F_Case_10_3.dat', 'rb') as data:
    strain_Case_10_3 = pickle.load(data)
with open('M_Case_10_3.dat', 'rb') as data:
    MatPara_Case_10_3 = pickle.load(data)
with open('L_Case_10_3.dat', 'rb') as data:
    LoadCase_Case_10_3 = pickle.load(data)

## Plot 3) Case 10_3
plt.figure(num=3)
plt.plot(UCT_QS[12], UCT_QS[13], label="_no_label",
         lw=1.5, ls="dashed", alpha=0.6, color="DarkBlue")
plt.plot(UCT_QS[14], UCT_QS[15], label="_no_label",
         lw=1.5, ls="dashed", alpha=0.8, color="DarkBlue")
plt.plot(UCT_QS[16], UCT_QS[17], label="Exp. -10°C",
         lw=1.5, ls="dashed", alpha=1., color="DarkBlue")
plt.plot(UCT_QS[0], UCT_QS[1], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.6, color="DarkGreen")
plt.plot(UCT_QS[2], UCT_QS[3], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.8, color="DarkGreen")
plt.plot(UCT_QS[4], UCT_QS[5], label="Exp. 23°C", lw=1.5,
         ls="dotted", alpha=1.0, color="DarkGreen")
plt.plot(UCT_QS[6], UCT_QS[7], label="_no_label", lw=1.5,
         ls="dashdot", alpha=0.6, color="DarkRed")
plt.plot(UCT_QS[8], UCT_QS[9], label="_no_label", lw=1.5,
         ls="dashdot", alpha=0.8, color="DarkRed")
plt.plot(UCT_QS[10], UCT_QS[11], label="Exp. 70°C",
         lw=1.5, ls="dashdot", alpha=1.0, color="DarkRed")

plt.plot(strain_Case_10_3[-1], stress_Case_10_3[-1], alpha=1., lw=1.5, label='Num. -10°C', color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=2, markerfacecolor='none')
plt.plot(strain_Case_10_3[0][0:-1:1], stress_Case_10_3[0][0:-1:1], alpha=1., lw=1.5, label='Num. 23°C', color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=8, markevery=0.15, markerfacecolor='none')
plt.plot(strain_Case_10_3[1], stress_Case_10_3[1], alpha=1., lw=1.5, label='Num. 70°C', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=2, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.001, 0.05])
plt.ylim([-1., 90.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="upper left")
plt.savefig("Images_Paper1/PP_Compression_Numerical.pdf", bbox_inches="tight")


#### Case 10_4 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc
with open('P_Case_10_4.dat', 'rb') as data:
    stress_Case_10_4 = pickle.load(data)
with open('F_Case_10_4.dat', 'rb') as data:
    strain_Case_10_4 = pickle.load(data)
with open('M_Case_10_4.dat', 'rb') as data:
    MatPara_Case_10_4 = pickle.load(data)
with open('L_Case_10_4.dat', 'rb') as data:
    LoadCase_Case_10_4 = pickle.load(data)

## Plot 4) Case 10_4
plt.figure(num=4)
plt.plot(UTT_QS[12], UTT_QS[13], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[14], UTT_QS[15], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[16], UTT_QS[17], label="Exp. -10°C",
         alpha=1.0, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[0], UTT_QS[1], label="_no_label",
         alpha=0.6, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3], label="_no_label",
         alpha=0.8, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5], label="Exp. 23°C",
         alpha=1.0, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[6], UTT_QS[7], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[8], UTT_QS[9], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[10], UTT_QS[11], label="Exp. 70°C",
         alpha=1.0, lw=1.5, ls="dashdot", color="DarkRed")

plt.plot(strain_Case_10_4[-1], stress_Case_10_4[-1], alpha=1., lw=1.5, label='Num. -10°C', color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_4[0], stress_Case_10_4[0], alpha=1., lw=1.5, label='Num. 23°C', color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=8, markevery=20, markerfacecolor='none')
plt.plot(strain_Case_10_4[1], stress_Case_10_4[1], alpha=1., lw=1.5, label='Num. 70°C', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=10, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.20])
plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="upper right")
plt.savefig("Images_Paper1/PP_Tension_Disp_Numerical.pdf", bbox_inches="tight")


#### Case 10_5 - Baseline TVP response - Compression monotonic, yes thermSrc, yes mechSrc
with open('P_Case_10_5.dat', 'rb') as data:
    stress_Case_10_5 = pickle.load(data)
with open('F_Case_10_5.dat', 'rb') as data:
    strain_Case_10_5 = pickle.load(data)
with open('M_Case_10_5.dat', 'rb') as data:
    MatPara_Case_10_5 = pickle.load(data)
with open('L_Case_10_5.dat', 'rb') as data:
    LoadCase_Case_10_5 = pickle.load(data)

## Plot 5) Case 10_5
plt.figure(num=5)
plt.plot(UCT_QS[12], UCT_QS[13], label="_no_label",
         lw=1.5, ls="dashed", alpha=0.6, color="DarkBlue")
plt.plot(UCT_QS[14], UCT_QS[15], label="_no_label",
         lw=1.5, ls="dashed", alpha=0.8, color="DarkBlue")
plt.plot(UCT_QS[16], UCT_QS[17], label="Exp. -10°C",
         lw=1.5, ls="dashed", alpha=1., color="DarkBlue")
plt.plot(UCT_QS[0], UCT_QS[1], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.6, color="DarkGreen")
plt.plot(UCT_QS[2], UCT_QS[3], label="_no_label", lw=1.5,
         ls="dotted", alpha=0.8, color="DarkGreen")
plt.plot(UCT_QS[4], UCT_QS[5], label="Exp. 23°C", lw=1.5,
         ls="dotted", alpha=1.0, color="DarkGreen")
plt.plot(UCT_QS[6], UCT_QS[7], label="_no_label", lw=1.5,
         ls="dashdot", alpha=0.6, color="DarkRed")
plt.plot(UCT_QS[8], UCT_QS[9], label="_no_label", lw=1.5,
         ls="dashdot", alpha=0.8, color="DarkRed")
plt.plot(UCT_QS[10], UCT_QS[11], label="Exp. 70°C",
         lw=1.5, ls="dashdot", alpha=1.0, color="DarkRed")

plt.plot(strain_Case_10_5[-1], stress_Case_10_5[-1], alpha=1., lw=1.5, label='Num. -10°C', color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=0.15, markerfacecolor='none')
plt.plot(strain_Case_10_5[0][0:-1:1], stress_Case_10_5[0][0:-1:1], alpha=1., lw=1.5, label='Num. 23°C', color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=8, markevery=0.15, markerfacecolor='none')
plt.plot(strain_Case_10_5[1], stress_Case_10_5[1], alpha=1., lw=1.5, label='Num. 70°C', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=0.15, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.001, 0.05])
plt.ylim([-1., 90.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="upper left")
plt.savefig("Images_Paper1/PP_Compression_Disp_Numerical.pdf", bbox_inches="tight")


#### Case 10_5 - Baseline TVP response - Compression monotonic, yes thermSrc, yes mechSrc
####

### Extra setting: refer to LoadCase

with open('P_Case_10_5.dat', 'rb') as data:
    stress_Case_10_5 = pickle.load(data)
with open('F_Case_10_5.dat', 'rb') as data:
    strain_Case_10_5 = pickle.load(data)
with open('M_Case_10_5.dat', 'rb') as data:
    MatPara_Case_10_5 = pickle.load(data)
with open('L_Case_10_5.dat', 'rb') as data:
    LoadCase_Case_10_5 = pickle.load(data)
with open('T_Case_10_5.dat', 'rb') as data:
    temperature_Case_10_5 = pickle.load(data)

## Plot 6) Case 10_5
plt.figure(num=6)
plt.plot(strain_Case_10_5[-1], temperature_Case_10_5[-1], alpha=1., lw=1.5, label='Num. -10°C', color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=0.15, markerfacecolor='none')
plt.plot(strain_Case_10_5[0][0:-1:1], temperature_Case_10_5[0][0:-1:1], alpha=1., lw=1.5, label='Num. 23°C', color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=8, markevery=0.15, markerfacecolor='none')
plt.plot(strain_Case_10_5[1], temperature_Case_10_5[1], alpha=1., lw=1.5, label='Num. 70°C', color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=0.15, markerfacecolor='none')
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Temperature [K]", fontdict={'fontsize': 12.5})
# plt.title("PP ", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.55])
plt.ylim([250., 360.])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="best", bbox_to_anchor=(0.60, 0.5))
plt.grid('on')
plt.savefig("Images_Paper1/PP_compression_Disp_Temperature_Numerical.pdf",
            bbox_inches="tight")


#### Case 10_6 - Baseline TVP response - Tension cyclic, no thermSrc, no mechSrc
#### Case 10_7 - Baseline TVP response - Tension cyclic, yes thermSrc, yes mechSrc

with open('P_Case_10_6.dat', 'rb') as data:
    stress_Case_10_6 = pickle.load(data)
with open('F_Case_10_6.dat', 'rb') as data:
    strain_Case_10_6 = pickle.load(data)
with open('M_Case_10_6.dat', 'rb') as data:
    MatPara_Case_10_6 = pickle.load(data)
with open('L_Case_10_6.dat', 'rb') as data:
    LoadCase_Case_10_6 = pickle.load(data)
with open('P_Case_10_7.dat', 'rb') as data:
    stress_Case_10_7 = pickle.load(data)
with open('F_Case_10_7.dat', 'rb') as data:
    strain_Case_10_7 = pickle.load(data)
with open('M_Case_10_7.dat', 'rb') as data:
    MatPara_Case_10_7 = pickle.load(data)
with open('L_Case_10_7.dat', 'rb') as data:
    LoadCase_Case_10_7 = pickle.load(data)


#### Case 10_6 and 10_7

## Plot 7) Case 10_7
plt.figure(num=7)
plt.plot(UTT_QS[0], UTT_QS[1], label="_no_label",
         alpha=0.6, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3], label="_no_label",
         alpha=0.8, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5], label="Exp. 23°C Mono",
         alpha=1.0, lw=1.5, ls="dotted", color="DarkGreen")
plt.plot(MCT_QS[0][0:8032], MCT_QS[1][0:8032],
         label="Exp. 23°C Cyc", lw=2.5, ls="dashdot", color="DarkGreen")

plt.plot(strain_Case_10_4[0], stress_Case_10_4[0], alpha=1., lw=1.5, label='Num. 23°C Mono', color="k",
         ls='solid', marker="o", markeredgecolor="b", mew=2, ms=8, markevery=15, markerfacecolor='none')
# plt.plot(strain_Case_10_6[0], stress_Case_10_6[0], alpha = 1.,lw=1.5, label='Num. 23°C Cyc', color="m",
#        ls='solid',marker="x",markeredgecolor="m",mew=2, ms=6,markevery=25,markerfacecolor='none')
plt.plot(strain_Case_10_7[0], stress_Case_10_7[0], alpha=1., lw=1.5, label='Num. 23°C Cyc', color="m",
         ls='solid', marker="x", markeredgecolor="m", mew=2, ms=6, markevery=25, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.001, 0.05])
# plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="lower right")
plt.savefig("Images_Paper1/PP_Cyclic_Disp_23_Numerical.pdf", bbox_inches="tight")


## Plot 8) Case 10_7
plt.figure(num=8)
plt.plot(UTT_QS[12], UTT_QS[13], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[14], UTT_QS[15], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(UTT_QS[16], UTT_QS[17], label="Exp. -10°C Mono",
         alpha=1.0, lw=1.5, ls="dashed", color="DarkBlue")
plt.plot(MCT_QS[8][0:5350], MCT_QS[9][0:5350],
         label="Exp. -10°C Cyc", lw=2.5, ls="dashdot", color="Blue")

plt.plot(strain_Case_10_4[-1], stress_Case_10_4[-1], alpha=1., lw=1.5, label='Num. -10°C Mono', color="k",
         ls='solid', marker="o", markeredgecolor="k", mew=2, ms=8, markevery=15, markerfacecolor='none')
# plt.plot(strain_Case_10_6[-1], stress_Case_10_6[-1], alpha = 1.,lw=1.75, label='Num. 23°C Cyc', color="m",
#        ls='solid',marker="x",markeredgecolor="m",mew=1, ms=6,markevery=40,markerfacecolor='none')
plt.plot(strain_Case_10_7[-1], stress_Case_10_7[-1], alpha=1., lw=1.75, label='Num. -10°C Cyc', color="m",
         ls='solid', marker="x", markeredgecolor="m", mew=1, ms=6, markevery=40, markerfacecolor='none')

plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.001, 0.05])
# plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="lower right")
plt.savefig("Images_Paper1/PP_Cyclic_Disp_m10_Numerical.pdf",bbox_inches="tight")


## Plot 9) Case 10_7
plt.figure(num=9)
plt.plot(UTT_QS[6], UTT_QS[7], label="_no_label",
         alpha=0.6, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[8], UTT_QS[9], label="_no_label",
         alpha=0.8, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(UTT_QS[10], UTT_QS[11], label="Exp. 70°C Mono",
         alpha=1.0, lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(MCT_QS[6], MCT_QS[7], label="Exp. 70°C",
         lw=1.5, ls="dashdot", color="DarkRed")
plt.plot(strain_Case_10_4[1], stress_Case_10_4[1], alpha=1., lw=1.5, label='Num. 70°C Mono', color="k",
         ls='solid', marker="o", markeredgecolor="k", mew=2, ms=8, markevery=20, markerfacecolor='none')
# plt.plot(strain_Case_10_6[1], stress_Case_10_6[1], alpha = 1.,lw=1.5, label='Num. 70°C Cyc', color="m",
#        ls='solid',marker="x",markeredgecolor="m",mew=1, ms=6,markevery=25,markerfacecolor='none')
plt.plot(strain_Case_10_7[1], stress_Case_10_7[1], alpha=1., lw=1.5, label='Num. 70°C Cyc', color="m",
         ls='solid', marker="x", markeredgecolor="m", mew=1, ms=6, markevery=25, markerfacecolor='none')
plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.25])
# plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize=12, loc="lower right")
plt.savefig("Images_Paper1/PP_Cyclic_Disp_70_Numerical.pdf",bbox_inches="tight")


#### Case 10_8 - Baseline TVP response - Compression monotonic, yes thermSrc, yes mechSrc, regularisation constant test
####
with open('P_Case_10_8.dat', 'rb') as data:
    stress_Case_10_8 = pickle.load(data)
with open('F_Case_10_8.dat', 'rb') as data:
    strain_Case_10_8 = pickle.load(data)
with open('M_Case_10_8.dat', 'rb') as data:
    MatPara_Case_10_8 = pickle.load(data)
with open('L_Case_10_8.dat', 'rb') as data:
    LoadCase_Case_10_8 = pickle.load(data)
# with open('T_Case_10_8.dat','rb') as data:
#    temperature_Case_10_8=pickle.load(data)

## Plot 10) Case 10_8
plt.figure(num=10)

plt.plot(strain_Case_10_8[0], stress_Case_10_8[0], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_8[0][10][0]), color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=0.2, markerfacecolor='none')

plt.plot(strain_Case_10_8[1], stress_Case_10_8[1], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_8[1][10][0]), color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=0.2, markerfacecolor='none')

plt.plot(strain_Case_10_8[2], stress_Case_10_8[2], alpha=1., lw=2.5, label='Num. xi=%g'
         % (MatPara_Case_10_8[2][10][0]), color="k",
         ls='solid', marker="p", markeredgecolor="k", mew=2, ms=10, markevery=0.15, markerfacecolor='none')

plt.plot(strain_Case_10_8[3][0:-1:1], stress_Case_10_8[3][0:-1:1], alpha=1., lw=2.5, label='Num. xi=%g'
         % (MatPara_Case_10_8[3][10][0]), color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=10, markevery=0.1, markerfacecolor='none')

plt.plot(strain_Case_10_8[4][0:-1:1], stress_Case_10_8[4][0:-1:1], alpha=1., lw=2.5, label='Num. xi=%g'
         % (MatPara_Case_10_8[4][10][0]), color="m",
         ls='solid', marker="+", markeredgecolor="m", mew=3, ms=12, markevery=0.1, markerfacecolor='none')

plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("PP ", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.055])
# plt.ylim([-1., 40.])
plt.legend(fontsize=12, loc="lower right")  # bbox_to_anchor=(1.05, 0.5))
plt.grid('on')
plt.savefig("Images_Paper1/PP_Compression_Regularisation_Numerical.pdf",bbox_inches="tight")


#### Case 10_9 - Baseline TVP response - Tension monotonic, yes thermSrc, yes mechSrc, regularisation constant test
####

with open('P_Case_10_9.dat', 'rb') as data:
    stress_Case_10_9 = pickle.load(data)
with open('F_Case_10_9.dat', 'rb') as data:
    strain_Case_10_9 = pickle.load(data)
with open('M_Case_10_9.dat', 'rb') as data:
    MatPara_Case_10_9 = pickle.load(data)
with open('L_Case_10_9.dat', 'rb') as data:
    LoadCase_Case_10_9 = pickle.load(data)
# with open('T_Case_10_9.dat','rb') as data:
#    temperature_Case_10_9=pickle.load(data)

## Plot 3) Case 10_9
plt.figure(num=11)
plt.plot(strain_Case_10_9[0], stress_Case_10_9[0], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_9[0][10][0]), color="b",
         ls='solid', marker="s", markeredgecolor="b", mew=2, ms=8, markevery=0.2, markerfacecolor='none')

plt.plot(strain_Case_10_9[1], stress_Case_10_9[1], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_9[1][10][0]), color="r",
         ls='solid', marker="x", markeredgecolor="r", mew=2, ms=10, markevery=0.2, markerfacecolor='none')

plt.plot(strain_Case_10_9[2], stress_Case_10_9[2], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_9[2][10][0]), color="k",
         ls='solid', marker="p", markeredgecolor="k", mew=2, ms=10, markevery=0.15, markerfacecolor='none')

plt.plot(strain_Case_10_9[3][0:-1:1], stress_Case_10_9[3][0:-1:1], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_9[3][10][0]), color="g",
         ls='solid', marker="o", markeredgecolor="g", mew=2, ms=10, markevery=0.1, markerfacecolor='none')

plt.plot(strain_Case_10_9[4][0:-1:1], stress_Case_10_9[4][0:-1:1], alpha=1., lw=1.5, label='Num. xi=%g'
         % (MatPara_Case_10_9[4][10][0]), color="m",
         ls='solid', marker="+", markeredgecolor="m", mew=3, ms=12, markevery=0.2, markerfacecolor='none')

plt.xlabel("Eng. strain [-]", fontdict={'fontsize': 12.5})
plt.ylabel("Eng. stress [MPa]", fontdict={'fontsize': 12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
# plt.title("PP ", fontdict={'fontsize':10.5, 'weight': 'bold'})
# plt.xlim([-0.001, 0.055])
# plt.ylim([-1., 40.]
plt.legend(fontsize=12, loc="lower right")  # bbox_to_anchor=(1.05, 0.5))
plt.grid('on')
plt.savefig("Images_Paper1/PP_Tension_Regularisation_Numerical.pdf",bbox_inches="tight")
