import numpy as np
import scipy as sp
from scipy import interpolate
import os, random, pickle, csv
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from matplotlib_inline.backend_inline import set_matplotlib_formats
set_matplotlib_formats('pdf')
# import matplotlib_inline
# matplotlib_inline.backend_inline.set_matplotlib_formats('pdf')

# Prepare the experimental curves -> Tension and Compression

## Load Uniaxial Tension Experimental Test Results - Strain Rate (Experiments 3)
    # UTT -> Uniaxial Tension Tests - 1st Sheet -> QS
        # Data -> strain % elongation vs stress MPa
UTT_SR_QS_df = pd.read_excel('../../Experiments/PP/Kepa_3/ZT1075_Carbo Brake_Tensile_20220607.xlsx', \
                       sheet_name = "Tensile uniaxial QS ", \
                       usecols = [2,3,5,6,8,9,19,20,22,23,25,26,36,37,39,40,42,43], \
                       index_col = None, skiprows = range(0, 20))

UTT_SR_QS_df.rename({'e_1 [%]': 'e1_23', 's_1 [MPa]': 's1_23', \
                  'e_1 [%].1': 'e2_23', 's_1 [MPa].1': 's2_23', \
                  'e_1 [%].2': 'e3_23', 's_1 [MPa].2': 's3_23', \
                  'e_1 [%].5': 'e1_70', 's_1 [MPa].5': 's1_70', \
                  'e_1 [%].6': 'e2_70', 's_1 [MPa].6': 's2_70', \
                  'e_1 [%].7': 'e3_70', 's_1 [MPa].7': 's3_70', \
                  'e_1 [%].10': 'e1_m10', 's_1 [MPa].10': 's1_m10', \
                  'e_1 [%].11': 'e2_m10', 's_1 [MPa].11': 's2_m10', \
                  'e_1 [%].12': 'e3_m10', 's_1 [MPa].12': 's3_m10', \
              }, axis=1, inplace=True)
# UTT_SR_QS_df.dropna()

## Load Uniaxial Compression Experimental Test Results - QS (Experiments 3)
    # UCT -> Uniaxial Compression Tests
        # Data -> strain % elongation vs stress MPa
UCT_SR_QS_df = pd.read_excel('../../Experiments/PP/Kepa_3/ZT1075_Carbo Brake_Compression_20220607.xlsx', \
                       sheet_name = "Compression", \
                       usecols = [1,2,3,4,5,6,7,8,9,18,19,20,21,22,23,24,25,26,35,36,37,38,39,40,41,42,43], \
                       index_col = None, skiprows = range(0, 20))

UCT_SR_QS_df.rename({'t [s]': 't1_23', 'e_1 [-]': 'e1_23', 's_1 [MPa]': 's1_23', \
               't [s].1': 't2_23', 'e_1 [-].1': 'e2_23', 's_1 [MPa].1': 's2_23', \
               't [s].2': 't3_23', 'e_1 [-].2': 'e3_23', 's_1 [MPa].2': 's3_23', \
               't [s].5': 't1_70', 'e_1 [-].5': 'e1_70', 's_1 [MPa].5': 's1_70', \
               't [s].6': 't2_70', 'e_1 [-].6': 'e2_70', 's_1 [MPa].6': 's2_70', \
               't [s].7': 't3_70', 'e_1 [-].7': 'e3_70', 's_1 [MPa].7': 's3_70', \
               't [s].10': 't1_m10', 'e_1 [-].10': 'e1_m10', 's_1 [MPa].10': 's1_m10', \
               't [s].11': 't2_m10', 'e_1 [-].11': 'e2_m10', 's_1 [MPa].11': 's2_m10', \
               't [s].12': 't3_m10', 'e_1 [-].12': 'e3_m10', 's_1 [MPa].12': 's3_m10', \
              }, axis=1, inplace=True)

UTT_SR_QS_df.loc[:,'e1_23'] *=1/100.
UTT_SR_QS_df.loc[:,'e2_23'] *=1/100.
UTT_SR_QS_df.loc[:,'e3_23'] *=1/100.
UTT_SR_QS_df.loc[:,'e1_70'] *=1/100.
UTT_SR_QS_df.loc[:,'e2_70'] *=1/100.
UTT_SR_QS_df.loc[:,'e3_70'] *=1/100.
UTT_SR_QS_df.loc[:,'e1_m10'] *=1/100.
UTT_SR_QS_df.loc[:,'e2_m10'] *=1/100.
UTT_SR_QS_df.loc[:,'e3_m10'] *=1/100.

UCT_SR_QS_df.loc[:,'e1_23'] *=1/100.
UCT_SR_QS_df.loc[:,'e2_23'] *=1/100.
UCT_SR_QS_df.loc[:,'e3_23'] *=1/100.
UCT_SR_QS_df.loc[:,'e1_70'] *=1/100.
UCT_SR_QS_df.loc[:,'e2_70'] *=1/100.
UCT_SR_QS_df.loc[:,'e3_70'] *=1/100.
UCT_SR_QS_df.loc[:,'e1_m10'] *=1/100.
UCT_SR_QS_df.loc[:,'e2_m10'] *=1/100.
UCT_SR_QS_df.loc[:,'e3_m10'] *=1/100.

## Load Uniaxial Tension Experimental Test Results - Strain Rate (Experiments 3)
    # UTT -> Uniaxial Tension Tests - 2nd Sheet -> 0.5s-1
        # Data -> strain % elongation vs stress MPa

UTT_SR_0_5_df = pd.read_excel('../../Experiments/PP/Kepa_3/ZT1075_Carbo Brake_Tensile_20220607.xlsx', \
                       sheet_name = "Tensile uniaxial 0.5s-1", \
                       usecols = [1,2,3,4,5,6,7,8,9], \
                       index_col = None, skiprows = range(0, 20))

UTT_SR_0_5_df.rename({'t [s]': 't1_23', 'e_1 [%]': 'e1_23', 's_1 [MPa]': 's1_23', \
                   't [s].1': 't2_23', 'e_1 [%].1': 'e2_23', 's_1 [MPa].1': 's2_23', \
                   't [s].2': 't3_23', 'e_1 [%].2': 'e3_23', 's_1 [MPa].2': 's3_23', \
              }, axis=1, inplace=True)

# UTT_SR_0_5_df.head()


## Load Uniaxial Tension Experimental Test Results - Strain Rate (Experiments 3)
    # UTT -> Uniaxial Tension Tests - 3rd Sheet -> 5s-1
        # Data -> strain % elongation vs stress MPa

UTT_SR_5_0_df = pd.read_excel('../../Experiments/PP/Kepa_3/ZT1075_Carbo Brake_Tensile_20220607.xlsx', \
                       sheet_name = "Tensile uniaxial 5s-1", \
                       usecols = [1,2,3,4,5,6,7,8,9], \
                       index_col = None, skiprows = range(0, 20))

UTT_SR_5_0_df.rename({'t [s]': 't1_23', 'e_1 [%]': 'e1_23', 's_1 [MPa]': 's1_23', \
                   't [s].1': 't2_23', 'e_1 [%].1': 'e2_23', 's_1 [MPa].1': 's2_23', \
                   't [s].2': 't3_23', 'e_1 [%].2': 'e3_23', 's_1 [MPa].2': 's3_23', \
              }, axis=1, inplace=True)

UTT_SR_0_5_df.loc[:,'e1_23'] *=1/100.
UTT_SR_0_5_df.loc[:,'e2_23'] *=1/100.
UTT_SR_0_5_df.loc[:,'e3_23'] *=1/100.
UTT_SR_5_0_df.loc[:,'e1_23'] *=1/100.
UTT_SR_5_0_df.loc[:,'e2_23'] *=1/100.
UTT_SR_5_0_df.loc[:,'e3_23'] *=1/100.

## Load Cyclic Test Results (Experiments 4 Part 1 - cyclic tests at -10, 23, 70°C)
    # MCT -> (Mechanical) Cyclic Tests (mech only isothermal)
        # Data -> strain (%elongation) vs stress (MPa)

MCT_QS_23_1_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part1_23_70_cyclic/23C_1-summary.xlsx', \
                       sheet_name = "23C_1-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_23_2_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part1_23_70_cyclic/23C_2-summary.xlsx', \
                       sheet_name = "23C_2-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_70_1_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part1_23_70_cyclic/70C_1-summary.xlsx', \
                       sheet_name = "70C_1-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_70_2_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part2_m10_70_cyclic/70_2-summary.xlsx', \
                       sheet_name = "70_2-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_m10_1_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part2_m10_70_cyclic/-10C_1-summary.xlsx', \
                       sheet_name = "-10C_1-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_m10_2_df = pd.read_excel('../../Experiments/PP/Kepa_4/Part2_m10_70_cyclic/-10C_2-summary.xlsx', \
                       sheet_name = "-10C_2-summary", \
                       usecols = [4,6], \
                       index_col = None, skiprows = range(0, 7))

MCT_QS_23_1_df.rename({'mm/mm': 'e1_23', 'N/mm²': 's1_23'}, axis=1, inplace=True)
MCT_QS_23_2_df.rename({'mm/mm': 'e2_23', 'N/mm²': 's2_23'}, axis=1, inplace=True)
MCT_QS_70_1_df.rename({'mm/mm': 'e1_70', 'N/mm²': 's1_70'}, axis=1, inplace=True)
MCT_QS_70_2_df.rename({'mm/mm': 'e2_70', 'N/mm²': 's2_70'}, axis=1, inplace=True)
MCT_QS_m10_1_df.rename({'mm/mm': 'e1_m10', 'N/mm²': 's1_m10'}, axis=1, inplace=True)
MCT_QS_m10_2_df.rename({'mm/mm': 'e2_m10', 'N/mm²': 's2_m10'}, axis=1, inplace=True)

### Convert to lists
UTT_QS = [UTT_SR_QS_df.loc[:,'e1_23'].tolist(),UTT_SR_QS_df.loc[:,'s1_23'].tolist(),
          UTT_SR_QS_df.loc[:,'e2_23'].tolist(),UTT_SR_QS_df.loc[:,'s2_23'].tolist(),
          UTT_SR_QS_df.loc[:,'e3_23'].tolist(),UTT_SR_QS_df.loc[:,'s3_23'].tolist(),
          UTT_SR_QS_df.loc[:,'e1_70'].tolist(),UTT_SR_QS_df.loc[:,'s1_70'].tolist(),
          UTT_SR_QS_df.loc[:,'e2_70'].tolist(),UTT_SR_QS_df.loc[:,'s2_70'].tolist(),
          UTT_SR_QS_df.loc[:,'e3_70'].tolist(),UTT_SR_QS_df.loc[:,'s3_70'].tolist(),
          UTT_SR_QS_df.loc[:,'e1_m10'].tolist(),UTT_SR_QS_df.loc[:,'s1_m10'].tolist(),
          UTT_SR_QS_df.loc[:,'e2_m10'].tolist(),UTT_SR_QS_df.loc[:,'s2_m10'].tolist(),
          UTT_SR_QS_df.loc[:,'e3_m10'].tolist(),UTT_SR_QS_df.loc[:,'s3_m10'].tolist()]

UCT_QS = [UCT_SR_QS_df.loc[:,'e1_23'].tolist(),UCT_SR_QS_df.loc[:,'s1_23'].tolist(),
          UCT_SR_QS_df.loc[:,'e2_23'].tolist(),UCT_SR_QS_df.loc[:,'s2_23'].tolist(),
          UCT_SR_QS_df.loc[:,'e3_23'].tolist(),UCT_SR_QS_df.loc[:,'s3_23'].tolist(),
          UCT_SR_QS_df.loc[:,'e1_70'].tolist(),UCT_SR_QS_df.loc[:,'s1_70'].tolist(),
          UCT_SR_QS_df.loc[:,'e2_70'].tolist(),UCT_SR_QS_df.loc[:,'s2_70'].tolist(),
          UCT_SR_QS_df.loc[:,'e3_70'].tolist(),UCT_SR_QS_df.loc[:,'s3_70'].tolist(),
          UCT_SR_QS_df.loc[:,'e1_m10'].tolist(),UCT_SR_QS_df.loc[:,'s1_m10'].tolist(),
          UCT_SR_QS_df.loc[:,'e2_m10'].tolist(),UCT_SR_QS_df.loc[:,'s2_m10'].tolist(),
          UCT_SR_QS_df.loc[:,'e3_m10'].tolist(),UCT_SR_QS_df.loc[:,'s3_m10'].tolist()]

UTT_SR_0_5 = [UTT_SR_0_5_df.loc[:,'e1_23'].tolist(),UTT_SR_0_5_df.loc[:,'s1_23'].tolist(),
          UTT_SR_0_5_df.loc[:,'e2_23'].tolist(),UTT_SR_0_5_df.loc[:,'s2_23'].tolist(),
          UTT_SR_0_5_df.loc[:,'e3_23'].tolist(),UTT_SR_0_5_df.loc[:,'s3_23'].tolist()]

UTT_SR_5_0 = [UTT_SR_5_0_df.loc[:,'e1_23'].tolist(),UTT_SR_5_0_df.loc[:,'s1_23'].tolist(),
          UTT_SR_5_0_df.loc[:,'e2_23'].tolist(),UTT_SR_5_0_df.loc[:,'s2_23'].tolist(),
          UTT_SR_5_0_df.loc[:,'e3_23'].tolist(),UTT_SR_5_0_df.loc[:,'s3_23'].tolist()]

MCT_QS = [MCT_QS_23_1_df.loc[:,'e1_23'].tolist(),MCT_QS_23_1_df.loc[:,'s1_23'].tolist(),
          MCT_QS_23_2_df.loc[:,'e2_23'].tolist(),MCT_QS_23_2_df.loc[:,'s2_23'].tolist(),
          MCT_QS_70_1_df.loc[:,'e1_70'].tolist(),MCT_QS_70_1_df.loc[:,'s1_70'].tolist(),
          MCT_QS_70_2_df.loc[:,'e2_70'].tolist(),MCT_QS_70_2_df.loc[:,'s2_70'].tolist(),
          MCT_QS_m10_1_df.loc[:,'e1_m10'].tolist(),MCT_QS_m10_1_df.loc[:,'s1_m10'].tolist(),
          MCT_QS_m10_2_df.loc[:,'e2_m10'].tolist(),MCT_QS_m10_2_df.loc[:,'s2_m10'].tolist()]


with open('PP_UTT_QS.dat','wb') as data:
    pickle.dump(UTT_QS,data)
with open('PP_UCT_QS.dat','wb') as data:
    pickle.dump(UCT_QS,data)
with open('PP_UTT_SR_0_5.dat','wb') as data:
    pickle.dump(UTT_SR_0_5,data)
with open('PP_UTT_SR_5_0.dat','wb') as data:
    pickle.dump(UTT_SR_5_0,data)
with open('PP_MCT_QS.dat','wb') as data:
    pickle.dump(MCT_QS,data)

#########
### PLOTS
#########

## PLOT UTT Polypropylene
plt.figure(num=1)
plt.plot(UTT_QS[12], UTT_QS[13],label="_no_label", alpha =0.6, lw = 1.5,ls ="dashed",color="DarkBlue")
plt.plot(UTT_QS[14], UTT_QS[15],label="_no_label", alpha =0.8,lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(UTT_QS[16], UTT_QS[17],label="Exp. -10°C", alpha =1.0, lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(UTT_QS[0], UTT_QS[1],label="_no_label", alpha =0.6, lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3],label="_no_label", alpha =0.8, lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5],label="Exp. 23°C", alpha =1.0, lw = 1.5,ls ="solid", color="DarkGreen")
plt.plot(UTT_QS[6], UTT_QS[7],label="_no_label", alpha =0.6,lw = 1.5, ls ="dashdot",color="DarkRed")
plt.plot(UTT_QS[8], UTT_QS[9],label="_no_label", alpha =0.8, lw = 1.5,ls ="dashdot",color="DarkRed")
plt.plot(UTT_QS[10], UTT_QS[11],label="Exp. 70°C", alpha =1.0, lw = 1.5, ls ="dashdot", color="DarkRed")
plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.10])
plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("PP Uniaxial Tension QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.savefig('Images_Paper1/PP_tension_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## PLOT UTT Polypropylene
plt.figure(num=2)
plt.plot(UTT_QS[12], UTT_QS[13],label="_no_label", alpha =0.6, lw = 1.5,ls ="dashed",color="DarkBlue")
plt.plot(UTT_QS[14], UTT_QS[15],label="_no_label", alpha =0.8,lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(UTT_QS[16], UTT_QS[17],label="Exp. -10°C", alpha =1.0, lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(UTT_QS[0], UTT_QS[1],label="_no_label", alpha =0.6, lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3],label="_no_label", alpha =0.8, lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5],label="Exp. 23°C", alpha =1.0, lw = 1.5,ls ="solid", color="DarkGreen")
plt.plot(UTT_QS[6][0:-1:100], UTT_QS[7][0:-1:100],label="_no_label", alpha =0.6,lw = 1.5, ls ="dashdot",color="DarkRed")
plt.plot(UTT_QS[8][0:-1:100], UTT_QS[9][0:-1:100],label="_no_label", alpha =0.8, lw = 1.5,ls ="dashdot",color="DarkRed")
plt.plot(UTT_QS[10][0:-1:100], UTT_QS[11][0:-1:100],label="Exp. 70°C", alpha =1.0, lw = 1.5, ls ="dashdot", color="DarkRed")
plt.grid(color='k', linewidth=0.25)
# plt.xlim([-0.005, 0.10])
plt.ylim([-1., 35.])
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("PP Uniaxial Tension QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.legend(fontsize = 12, loc = "upper right")
plt.savefig('Images_Paper1/PP_tension70_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## PLOT UCT Polypropylene
plt.figure(num=3)
plt.plot(UCT_QS[12], UCT_QS[13],label="_no_label", lw = 1.5,ls ="dashed",alpha =0.6, color="DarkBlue")
plt.plot(UCT_QS[14], UCT_QS[15],label="_no_label", lw = 1.5,ls ="dashed", alpha =0.8, color="DarkBlue")
plt.plot(UCT_QS[16], UCT_QS[17],label="Exp. -10°C", lw = 1.5,ls ="dashed", alpha =1., color="DarkBlue")
plt.plot(UCT_QS[0], UCT_QS[1],label="_no_label", lw = 1.5,ls ="solid", alpha =0.6, color="DarkGreen")
plt.plot(UCT_QS[2], UCT_QS[3],label="_no_label", lw = 1.5,ls ="solid", alpha =0.8, color="DarkGreen")
plt.plot(UCT_QS[4], UCT_QS[5],label="Exp. 23°C", lw = 1.5,ls ="solid", alpha =1.0, color="DarkGreen")
plt.plot(UCT_QS[6], UCT_QS[7],label="_no_label", lw = 1.5,ls ="dashdot", alpha =0.6, color="DarkRed")
plt.plot(UCT_QS[8], UCT_QS[9],label="_no_label", lw = 1.5,ls ="dashdot", alpha =0.8, color="DarkRed")
plt.plot(UCT_QS[10], UCT_QS[11],label="Exp. 70°C",lw = 1.5,ls ="dashdot", alpha =1.0, color="DarkRed")
plt.grid(color='k', linewidth=0.25)
# plt.xlim([-0.005, 0.10])
plt.ylim([-1., 100.])
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("PP Uniaxial Compression QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.savefig('Images_Paper1/PP_compression_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## PLOT UCT Polypropylene
plt.figure(num=4)
plt.plot(UCT_QS[12], UCT_QS[13],label="_no_label", lw = 1.5,ls ="dashed",alpha =0.6, color="DarkBlue")
plt.plot(UCT_QS[14], UCT_QS[15],label="_no_label", lw = 1.5,ls ="dashed", alpha =0.8, color="DarkBlue")
plt.plot(UCT_QS[16], UCT_QS[17],label="Exp. -10°C", lw = 1.5,ls ="dashed", alpha =1., color="DarkBlue")
plt.plot(UCT_QS[0], UCT_QS[1],label="_no_label", lw = 1.5,ls ="solid", alpha =0.6, color="DarkGreen")
plt.plot(UCT_QS[2], UCT_QS[3],label="_no_label", lw = 1.5,ls ="solid", alpha =0.8, color="DarkGreen")
plt.plot(UCT_QS[4], UCT_QS[5],label="Exp. 23°C", lw = 1.5,ls ="solid", alpha =1.0, color="DarkGreen")
plt.plot(UCT_QS[6], UCT_QS[7],label="_no_label", lw = 1.5,ls ="dashdot", alpha =0.6, color="DarkRed")
plt.plot(UCT_QS[8], UCT_QS[9],label="_no_label", lw = 1.5,ls ="dashdot", alpha =0.8, color="DarkRed")
plt.plot(UCT_QS[10], UCT_QS[11],label="Exp. 70°C",lw = 1.5,ls ="dashdot", alpha =1.0, color="DarkRed")
plt.grid(color='k', linewidth=0.25)
plt.xlim([-0.005, 0.12])
plt.ylim([-1., 100.])
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("PP Uniaxial Compression QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.savefig('Images_Paper1/PP_compression_Zoomed_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## PLOT UTT_SR Polypropylene
plt.figure(num=5)
plt.plot(UTT_QS[0], UTT_QS[1],label="_no_label", lw = 1.5,ls ="solid",alpha =0.6, color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3],label="_no_label", lw = 1.5,ls ="solid",alpha =0.8, color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5],label="Exp. 23°C, QS", lw = 1.5,ls ="solid",alpha =1.0,color="DarkGreen")
plt.plot(UTT_SR_0_5[0][0:-1:10], UTT_SR_0_5[1][0:-1:10],label="_no_label",lw = 1.5,ls ="dashed", alpha =0.8,color="DarkBlue")
plt.plot(UTT_SR_0_5[2][0:-1:10], UTT_SR_0_5[3][0:-1:10],label="_no_label",lw = 1.5,ls ="dashed", alpha =0.9,color="DarkBlue")
plt.plot(UTT_SR_0_5[4][0:-1:10], UTT_SR_0_5[5][0:-1:10],label="Exp. 23°C, 0.5/s",lw = 1.5,ls ="dashed", alpha =0.6,color="DarkBlue" )
plt.plot(UTT_SR_5_0[0][0:-1:10], UTT_SR_5_0[1][0:-1:10],label="_no_label",lw = 1.5,ls ="dashdot", alpha =0.8, color="DarkRed")
plt.plot(UTT_SR_5_0[2][0:-1:10], UTT_SR_5_0[3][0:-1:10],label="_no_label",lw = 1.5,ls ="dashdot", alpha =0.9, color="DarkRed")
plt.plot(UTT_SR_5_0[4][0:-1:10], UTT_SR_5_0[5][0:-1:10],label="Exp. 23°C, 5.0/s",lw = 1.5, ls ="dashdot", alpha =1.0, color="DarkRed")
plt.grid(color='k', linewidth=0.1)
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "lower right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("Tension Strain rate Stress vs. Strain", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.savefig('Images_Paper1/PP_SR_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## PLOT UTT_SR Polypropylene
plt.figure(num=6)
plt.plot(UTT_QS[0], UTT_QS[1],label="_no_label", lw = 1.5,ls ="solid",alpha =0.6, color="DarkGreen")
plt.plot(UTT_QS[2], UTT_QS[3],label="_no_label", lw = 1.5,ls ="solid",alpha =0.8, color="DarkGreen")
plt.plot(UTT_QS[4], UTT_QS[5],label="Exp. 23°C, QS", lw = 1.5,ls ="solid",alpha =1.0,color="DarkGreen")
plt.plot(UTT_SR_0_5[0][0:-1:10], UTT_SR_0_5[1][0:-1:10],label="_no_label",lw = 1.5,ls ="dashed", alpha =0.8,color="DarkBlue")
plt.plot(UTT_SR_0_5[2][0:-1:10], UTT_SR_0_5[3][0:-1:10],label="_no_label",lw = 1.5,ls ="dashed", alpha =0.9,color="DarkBlue")
plt.plot(UTT_SR_0_5[4][0:-1:10], UTT_SR_0_5[5][0:-1:10],label="Exp. 23°C, 0.5/s",lw = 1.5,ls ="dashed", alpha =0.6,color="DarkBlue" )
plt.plot(UTT_SR_5_0[0][0:-1:10], UTT_SR_5_0[1][0:-1:10],label="_no_label",lw = 1.5,ls ="dashdot", alpha =0.8, color="DarkRed")
plt.plot(UTT_SR_5_0[2][0:-1:10], UTT_SR_5_0[3][0:-1:10],label="_no_label",lw = 1.5,ls ="dashdot", alpha =0.9, color="DarkRed")
plt.plot(UTT_SR_5_0[4][0:-1:10], UTT_SR_5_0[5][0:-1:10],label="Exp. 23°C, 5.0/s",lw = 1.5, ls ="dashdot", alpha =1.0, color="DarkRed")
plt.grid(color='k', linewidth=0.1)
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
plt.xlim([-0.005, 0.1])
# plt.ylim([-1., 100.])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "lower right") # ,bbox_to_anchor=(1.1, 0.5)
# plt.title("Tension Strain rate Stress vs. Strain", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.savefig('Images_Paper1/PP_SR_Zoomed_Experiments.pdf', bbox_inches='tight') #, dpi = 300)

## Tension Cyclic Loading Comparison
plt.figure(num=7)
plt.plot(MCT_QS[8][0:5350], MCT_QS[9][0:5350],label="Exp. -10°C",lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(MCT_QS[0][0:8032], MCT_QS[1][0:8032],label="Exp. 23°C",lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(MCT_QS[6], MCT_QS[7],label="Exp. 70°C",lw = 1.5,ls ="dashdot",color="DarkRed")
plt.grid(color='k', linewidth=0.1)
plt.xlim([-0.001, 0.205])
plt.ylim([-5., 31.])
# plt.title("Experimental Cyclic Tension QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
# plt.xlim([-0.005, 0.1])
# plt.ylim([-1., 100.])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5))
plt.locator_params(axis='x', nbins=6)
plt.savefig('Images_Paper1/PP_CyclicLoading_experiment.pdf', bbox_inches='tight') #, dpi = 300)

## Tension Cyclic Loading Comparison

plt.figure(num=9)
plt.plot(MCT_QS[8][0:5350], MCT_QS[9][0:5350],label="Exp. -10°C",lw = 1.5,ls ="dashed", color="DarkBlue")
plt.plot(MCT_QS[0][0:8032], MCT_QS[1][0:8032],label="Exp. 23°C",lw = 1.5,ls ="solid",color="DarkGreen")
plt.plot(MCT_QS[6], MCT_QS[7],label="Exp. 70°C",lw = 1.5,ls ="dashdot",color="DarkRed")
plt.grid(color='k', linewidth=0.1)
plt.xlim([-0.001, 0.075])
plt.ylim([-5., 31.])
# plt.title("Experimental Cyclic Tension QS", fontdict={'fontsize':11.5, 'weight': 'bold'})
plt.xlabel("Eng. strain [-]",fontdict={'fontsize':12.5})
plt.ylabel("Eng. stress [MPa]",fontdict={'fontsize':12.5})
# plt.xlim([-0.005, 0.1])
# plt.ylim([-1., 100.])
plt.xticks(fontsize=12.)
plt.yticks(fontsize=12.)
plt.legend(fontsize = 12, loc = "upper right") # ,bbox_to_anchor=(1.1, 0.5))
plt.locator_params(axis='x', nbins=6)
plt.savefig('Images_Paper1/PP_CyclicLoading_Zoomed_experiment.pdf', bbox_inches='tight') #, dpi = 300)
