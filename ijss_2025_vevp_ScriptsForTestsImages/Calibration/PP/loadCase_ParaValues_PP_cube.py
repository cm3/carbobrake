######## list MatPara ########
#### MatPara[0][...] -> ExtraBranch -> (4 + 1 parameters)
#### MatPara[1][...] -> IsoHard -> (0-boolean,1-yieldStrenght,2-isoPara1,3-isoPara2,4-isoPara3,5-m,6-tempBoolean,7-isoTempFunc1,8-isoTempFunc2,9-isoTempFunc3,10-TYPE)
#### MatPara[2][...] -> KinHard -> (0-boolean,1-kinPara1,2-kinPara2,3-kinPara3,4-tempBoolean,5-kinTempFunc1,6-kinTempFunc2,7-kinTempFunc3)
#### MatPara[3][...] -> YieldFunctionParas -> (0-YieldPowerFactor,1-FlowRuleFactor)
#### MatPara[4][...] -> Viscosity -> (0-boolean,1-viscPara1,2-viscPara2,3-tempBoolean,4-viscoTempFunc1,5-viscoTempFunc2,6-viscoTempFunc3)
#### MatPara[5][...] -> TVE -> (0-TVE_Boolean, 1-ExtraBranch_Boolean)
#### MatPara[6][...] -> Mullins -> (0-boolean,1-mullinsPara1,2-tempBoolean,3-mullinsTempFunc1,4-mullinsTempFunc2,5-mullinsTempFunc3)
#### MatPara[7][...] -> ExtraTVE - material dependent
#### MatPara[8][...] -> heatSrc -> (0-boolean ThermSrc and tempBC, 1-boolean MechSrc)
#### MatPara[9][...] -> rotationCorrection -> (0-boolean)
#### MatPara[10][...] -> regularisation -> (0-regularisation constant)

def FillMatPara(extraBranch,IsoHard,KinHard,YieldFunctionParas,Viscosity,TVE,mullins,extraTVE,heatSrc,rotationCorrection,regularisation):
  MatPara = []
  MatPara.append(extraBranch)
  MatPara.append(IsoHard)
  MatPara.append(KinHard)
  MatPara.append(YieldFunctionParas)
  MatPara.append(Viscosity)
  MatPara.append(TVE)
  MatPara.append(mullins)
  MatPara.append(extraTVE)
  MatPara.append(heatSrc)
  MatPara.append(rotationCorrection)
  MatPara.append(regularisation)
  return MatPara

####### list extraBranch
extraBranchBoolean = [False,True]

# Case 1.0
extraBranch = [[0.0,1000,1.5,0.0,1000.,1.5]]
extraBranch_Comp1 = [1.8]
extraBranch_additional = [[0.,2000,1.35,0.,2000.,1.35]]

# extraTVE
import pandas as pd
import numpy as np
relSpec = pd.read_csv('relaxationSpectrum_Et_N27_19_04_24_Tref20.txt',header=None).to_numpy()
N = int(0.5*(relSpec.size-1))
extraTVE = np.ones((int(0.5*(relSpec.size-1)),13)) # 3 vol + 3 dev + 1 comp

# tension
extraTVE[:,0] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # V0 first at -10
extraTVE[:,1] = np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # V1 - flipped, last at -10
extraTVE[:,2] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # V2
extraTVE[:,3] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # D0
extraTVE[:,4] =  np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # D1 - flipped, last at -10
extraTVE[:,5] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # D2
extraTVE[:,6] = np.flip(np.concatenate((np.linspace(1.8,1.8,13),np.linspace(1.8,5.,14)))) # Ci

# compression
extraTVE[:,7] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,0]  # V3
extraTVE[:,8] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,1]
extraTVE[:,9] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,2]
extraTVE[:,10] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,3]
extraTVE[:,11] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,4]
extraTVE[:,12] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,5]
extraTVE.tolist()

####### list IsoHard
isoBoolean = [True]
yieldStrenght = [4.5]
isoPara1 = [5.]
isoPara2 = [5.]
isoPara3 = [5.]
m = [1.]
isoTempBoolean = [False]
isoTempFunc1 = [0.]
isoTempFunc2 = [0.]
isoTempFunc3 = [0.]
isoPara4 = [4.5]
isoPara5 = [5]
isoPara6 = [7.5]
isoPara7 = [12.5]

####### list kinHard
kinBoolean = [False,True]
kinPara1 = [20]
kinPara2 = [25]
kinPara3 = [0]
kinTempBoolean = [False]
kinTempFunc1 = [0.]
kinTempFunc2 = [0.]
kinTempFunc3 = [0.]
kinPara4 = [0.]

###### list YieldFunctionParas
YieldPowerFactor = [3.]
FlowRuleFactor = [0.64]

###### list Viscosity
viscBoolean = [False,True]
viscPara1 = [1000.]
viscPara2 = [0.21]
visctempBoolean = [False]
viscTempFunc1 = [0.]
viscTempFunc2 = [0.]
viscTempFunc3 = [0.]

##### list TVE
TVE_Boolean = [True, False]
ExtraBranch_Boolean = [True, False]

##### list Mullins
mullinsBoolean = [True, False]
mullinsPara1 = [0.99]
mullinstempBoolean = [False]
mullinsTempFunc1 = [0.]
mullinsTempFunc2 = [0.]
mullinsTempFunc3 = [0.]

##### list heatSrc
thermSrc_Boolean = [True, False]
mechSrc_Boolean = [True, False]

##### list rotationCorrection
rotationCorrection_Boolean = [True, False]

##### list regularisation
regularisation_constant = [10,100,500,1000,2000] # ,5000]
