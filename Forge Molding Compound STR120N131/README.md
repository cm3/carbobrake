# Forge Molding Compound STR120N131

This folder includes all experimental data for the Forged Molding Compound material of Mitsubishi, which is available under the name STR120N131.
Hereby, the material is a sheet molding compound (SMC) consisting out of 25.4 mm x 8 mm x 0.115 mm carbon fiber bundles (Toray TR50S), embedded in a vinyl ester thermoset matrix.

## Directories

For a full characterization of the material flow behavior the following tests were performed at 80°C and 100°C.
 * Squeeze Flow Rheometry
 * Pressure-specific Volume-Temperature Dilatometry

The structural properties of the cured SMC material were analysed for different plate thicknesses (2mm, 4mm, 6mm and 8mm) by 
 * Tensile tests
 * Three point bending tests
 * Dynamic mechanical analysis including temperature and frequency sweeps

Further information can be found in the following publications:
 * Zulueta et al., Advancing on viscosity characterization and modelling of SMC for compression molding simulation, DOI: 10.1002/pc.27510
 * Doppelbauer et al., A macroscopic model of the compaction process during compression molding of carbon fiber sheet molding compounds, DOI: https://doi.org/10.1016/j.compositesa.2023.107535
 * Fiedler et al., CF reinforced SMC in brakes: Design and simulation of a lightweight motorcycle brake calliper, DOI: 10.46720/eb2023-cmt-016
 * Fiedler et al., CF reinforced SMC in brakes: Manufacturing and testing of a lightweight motorcycle brake calliper, DOI: https://doi.org/10.46720/EB2024-CMT-023

## License
Creative Commons Attribution 4.0 International (CC BY 4.0) licence

